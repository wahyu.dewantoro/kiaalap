<?php 

$string = "
@extends('layouts.master')
@section('judul')
<h3>".ucfirst($table_name)."
</h3>
@endsection
@section('content')
<div class=\"row\">
    <div class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12\">
        <div class=\"sparkline9-list responsive-mg-b-30\">
            <div class=\"sparkline9-graph\">
                <div class=\"basic-login-form-ad\">
                    
                            <div class=\"basic-login-inner\">
                                <form action=\"{{ \$action }}\" method=\"post\">
                                    <?= csrfToken() ?>
                                    ";
                                    foreach ($non_pk as $row) {
                                        if ($row["data_type"] == 'text')
                                        {
                                        $string .= "\n\t    <div class=\"form-group\">
                                                <label for=\"".$row["column_name"]."\">".label($row["column_name"])." <?php echo form_error('".$row["column_name"]."') ?></label>
                                                <textarea class=\"form-control\" rows=\"3\" name=\"".$row["column_name"]."\" id=\"".$row["column_name"]."\" placeholder=\"".label($row["column_name"])."\"><?php echo $".$row["column_name"]."; ?></textarea>
                                            </div>";
                                        } else
                                        {
                                        $string .= "\n\t    <div class=\"form-group\">
                                                <label for=\"".$row["data_type"]."\">".label($row["column_name"])." <?php echo form_error('".$row["column_name"]."') ?></label>
                                                <input type=\"text\" class=\"form-control\" name=\"".$row["column_name"]."\" id=\"".$row["column_name"]."\" placeholder=\"".label($row["column_name"])."\" value=\"<?php echo $".$row["column_name"]."; ?>\" />
                                            </div>";
                                        }
                                    }
                                    $string .= "\n\t    <input type=\"hidden\" name=\"".$pk."\" value=\"<?php echo $".$pk."; ?>\" />
                                    <div class=\"login-btn-inner\">
                                    <div class=\"login-horizental\">
                                    <button class=\"btn btn-sm btn-primary login-submit-cs\" type=\"submit\"><i class=\"fa fa-save\"></i> Simpan</button>
                                    {!! anchor('".$c_url."','<i class=\"fa fa-close\"></i> Batal','class=\"btn btn-custon-four btn-warning\"') !!}
                                    </div>
                                    </div>
                                </form>
                            </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection";

$hasil_view_form = createFile($string, $target."views/" . $c_url . "/" . $v_form_file);

?>