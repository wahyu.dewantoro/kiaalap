<?php

$string = "<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class " . $m . " extends CI_Model
{

    public \$table = '$table_name';
    public \$id = '$pk';
    public \$order = 'DESC';";

if ($jenis_tabel <> 'reguler_table') {

    $column_all = array();
    foreach ($all as $row) {
        $column_all[] = $row['column_name'];
    }
    $columnall = implode(',', $column_all);

    $string .= "\n\n    // datatables
    function json() {
        \$this->datatables->select('" . $columnall . "');
        \$this->datatables->from('" . $table_name . "');
        //add this line for join
        //\$this->datatables->join('table2', '" . $table_name . ".field = table2.field');
        \$this->datatables->add_column('action', anchor(site_url('" . $c_url . "/read/\$1'),'Read').\" | \".anchor(site_url('" . $c_url . "/update/\$1'),'Update').\" | \".anchor(site_url('" . $c_url . "/delete/\$1'),'Delete','onclick=\"javasciprt: return confirm(\\'Are You Sure ?\\')\"'), '$pk');
        return \$this->datatables->generate();
    }";
}

$string .= "\n\n    // get all
    function get()
    {
        \$this->db->order_by(\$this->id, \$this->order);
        return \$this->db->get(\$this->table);
    }

    // get data by
    function first(\$colom,\$var){
        \$this->db->where(\$colom,\$var);
        return \$this->get()->row();
    }
    

     function queryDasar(\$q=null)
    {
        \$this->db->like('$pk', \$q);";
foreach ($non_pk as $row) {
    $string .= "\n\t\$this->db->or_like('" . $row['column_name'] . "', \$q);";
}
$string .= "\n\t return \$this->db->from(\$this->table);

     } 
    // get total rows
    function totalRows(\$q=null){
        \$this->queryDasar(\$q);
        return \$this->db->count_all_results();
    }

    //lmiit data
    function getLimitData(\$limit, \$start = 0, \$q = NULL){
        \$this->queryDasar(\$q);
        \$this->db->limit(\$limit, \$start);
        return \$this->db->get()->result();
     }

    // insert data
    function insertData(\$data)
    {
        \$this->db->insert(\$this->table, \$data);
    }
    

    // update data
    function updateData( \$data,\$id)
    {
        \$this->db->where(\$this->id, \$id);
        \$this->db->update(\$this->table, \$data);
    }

    // delete data
    function deleteData(\$id)
    {
        \$this->db->where(\$this->id, \$id);
        \$this->db->delete(\$this->table);
    }

}

/* End of file $m_file */
/* Location: ./application/models/$m_file */";

$hasil_model = createFile($string, $target . "models/" . $m_file);
