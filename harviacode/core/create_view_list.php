<?php 

$string = "
@extends('layouts.master')
@section('judul')
<h3>".ucfirst($table_name)." <div class=\"pull-right\"> {!! anchor('".$c_url."/create','<i class=\"fa fa-plus\"></i> Tambah','class=\"btn btn-custon-four btn-primary\"') !!} </div>
</h3>
@endsection
@section('content')

<div class=\"row\">
    <div class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12\">
        <div class=\"sparkline13-list\">
            <div class=\"sparkline13-graph\">
                <div class=\"datatable-dashv1-list custom-datatable-overright\">
                <form action=\"<?php echo site_url('$c_url/index'); ?>\" class=\"form-inline\" method=\"get\">
                <div class=\"input-group\">
                    <input type=\"text\" class=\"form-control\" name=\"q\" value=\"<?php echo \$q; ?>\">
                    <span class=\"input-group-btn\">
                        <?php 
                            if (\$q <> '')
                            {
                                ?>
                                <a href=\"<?php echo site_url('$c_url'); ?>\" class=\"btn btn-default\">Reset</a>
                                <?php
                            }
                        ?>
                      <button class=\"btn btn-xs btn-primary\" type=\"submit\">Search</button>
                    </span>
                </div>
            </form>
            <table class=\"table table-striped\" style=\"margin-bottom: 10px\">
            <tr>
                <th >No</th>";
foreach ($non_pk as $row) {
    $string .= "\n\t\t<th>" . label($row['column_name']) . "</th>";
}
$string .= "\n\t\t<th></th>
            </tr>";
$string .= "<?php
            foreach ($" . $c_url . "_data as \$$c_url)
            {
                ?>
                <tr>";

$string .= "\n\t\t\t<td    width=\"80px\"><?php echo ++\$start ?></td>";
foreach ($non_pk as $row) {
    $string .= "\n\t\t\t<td><?php echo $" . $c_url ."->". $row['column_name'] . " ?></td>";
}


$string .= "\n\t\t\t<td style=\"text-align:center\" width=\"100px\">"
        . "\n\t\t\t\t<?php "
        . "\n\t\t\t\techo anchor(site_url('".$c_url."/update/'.$".$c_url."->".$pk."),'<i class=\"fa fa-edit\"></i>','class=\"btn btn-xs btn-success\"'); "
        . "\n\t\t\t\techo anchor(site_url('".$c_url."/delete/'.$".$c_url."->".$pk."),'<i class=\"fa fa-trash\"></i>','class=\"btn btn-xs btn-danger\" onclick=\"javasciprt: return confirm(\\'Apakah anda yakin ?\\')\"'); "
        . "\n\t\t\t\t?>"
        . "\n\t\t\t</td>";

$string .=  "\n\t\t</tr>
                <?php
            }
            ?>
        </table>
        <div class=\"row\">
        <div class=\"col-md-6\">
            <a href=\"#\" class=\"btn btn-primary\">Total Record : <?php echo \$total_rows ?></a>";
if ($export_excel == '1') {
$string .= "\n\t\t<?php echo anchor(site_url('".$c_url."/excel'), 'Excel', 'class=\"btn btn-primary\"'); ?>";
}
if ($export_word == '1') {
$string .= "\n\t\t<?php echo anchor(site_url('".$c_url."/word'), 'Word', 'class=\"btn btn-primary\"'); ?>";
}
if ($export_pdf == '1') {
$string .= "\n\t\t<?php echo anchor(site_url('".$c_url."/pdf'), 'PDF', 'class=\"btn btn-primary\"'); ?>";
}
$string .= "\n\t    </div>
        <div class=\"col-md-6 text-right\">
            <?php echo \$pagination ?>
        </div>
    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection";


$hasil_view_list = createFile($string, $target."views/" . $c_url . "/" . $v_list_file);

?>