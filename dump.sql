/*
SQLyog Ultimate v12.5.1 (64 bit)
MySQL - 10.1.28-MariaDB : Database - newkoishow
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `cartdetail` */

DROP TABLE IF EXISTS `cartdetail`;

CREATE TABLE `cartdetail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cartheader_id` int(11) DEFAULT NULL,
  `variety_id` int(11) DEFAULT NULL,
  `ukuran` int(3) DEFAULT NULL,
  `gender` int(1) DEFAULT NULL,
  `breeder` int(1) DEFAULT NULL,
  `gambar_ikan` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cart_fk` (`cartheader_id`),
  CONSTRAINT `cart_fko` FOREIGN KEY (`cartheader_id`) REFERENCES `cartheader` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `cartdetail` */

/*Table structure for table `cartheader` */

DROP TABLE IF EXISTS `cartheader`;

CREATE TABLE `cartheader` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uniq_code` varchar(100) DEFAULT NULL,
  `namahandling` varchar(100) DEFAULT NULL,
  `kotahandling` varchar(100) DEFAULT NULL,
  `telphandling` varchar(20) DEFAULT NULL,
  `namaowner` varchar(100) DEFAULT NULL,
  `kotaowner` varchar(100) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `checkout_by` int(11) DEFAULT NULL,
  `checkout_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `cartheader` */

/*Table structure for table `groups` */

DROP TABLE IF EXISTS `groups`;

CREATE TABLE `groups` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/*Data for the table `groups` */

insert  into `groups`(`id`,`name`,`description`) values 
(1,'admin','Administrator'),
(2,'members','General User'),
(5,'Handling','-');

/*Table structure for table `ikandetail` */

DROP TABLE IF EXISTS `ikandetail`;

CREATE TABLE `ikandetail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uniq_code` varchar(50) DEFAULT NULL,
  `namahandling` varchar(100) DEFAULT NULL,
  `kotahandling` varchar(100) DEFAULT NULL,
  `namaowner` varchar(100) DEFAULT NULL,
  `kotaowner` varchar(100) DEFAULT NULL,
  `variety_id` int(11) DEFAULT NULL,
  `ukuran` int(11) DEFAULT NULL,
  `gender` int(11) DEFAULT NULL,
  `breeder` int(11) DEFAULT NULL,
  `gambar_ikan` varchar(100) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=134 DEFAULT CHARSET=latin1;

/*Data for the table `ikandetail` */

insert  into `ikandetail`(`id`,`uniq_code`,`namahandling`,`kotahandling`,`namaowner`,`kotaowner`,`variety_id`,`ukuran`,`gender`,`breeder`,`gambar_ikan`,`created_at`,`created_by`,`updated_at`,`updated_by`,`deleted_at`,`deleted_by`) values 
(1,'A0001','Dua D Team','Jakarta','Mustofa','Kediri',53,10,1,1,'ikan/02cb2206b95058e63a895cbf60d7b36d.jpg','2019-09-14 08:27:39',1,NULL,NULL,NULL,NULL),
(2,'A0001','Dua D Team','Jakarta','Mustofa','Kediri',45,38,1,1,'ikan/307326c7d75d56711d1264e42e1342aa.jpg','2019-09-14 08:27:39',1,NULL,NULL,NULL,NULL),
(4,'A0002','Dua D Team','Jakarta','Dede Darmawan','Jakarta',41,56,1,1,'ikan/0fd356bbb018340f13ef6e4e0204e2fc.jpg','2019-09-14 08:27:51',1,NULL,NULL,NULL,NULL),
(5,'A0002','Dua D Team','Jakarta','Dede Darmawan','Jakarta',32,20,2,1,'ikan/802360383bd67dd586dbbd7dc0cfb722.jpg','2019-09-14 08:27:51',1,NULL,NULL,NULL,NULL),
(7,'A0003','Wik Koi','Blitar','Sunyoto','Denpasar',34,45,2,1,'ikan/3626b88058f9230e3fa6bffb5620d1b8.jpg','2019-09-14 08:27:56',1,NULL,NULL,NULL,NULL),
(8,'A0003','Wik Koi','Blitar','Sunyoto','Denpasar',45,10,1,1,'ikan/20e0494bc0ca67fa0c746e7148c6d391.jpg','2019-09-14 08:27:56',1,NULL,NULL,NULL,NULL),
(9,'A0003','Wik Koi','Blitar','Sunyoto','Denpasar',32,35,1,1,'ikan/801975801d16827cd723cbc0e5745370.jpg','2019-09-14 08:27:56',1,NULL,NULL,NULL,NULL),
(10,'A0003','Wik Koi','Blitar','Sunyoto','Denpasar',49,30,2,1,'ikan/b9a2ea34cfdfe817774750fa2acf9eb6.jpg','2019-09-14 08:27:56',1,NULL,NULL,NULL,NULL),
(11,'A0003','Wik Koi','Blitar','Sunyoto','Denpasar',53,60,2,1,'ikan/da40c3342c6f13812a162756052ee049.jpg','2019-09-14 08:27:56',1,NULL,NULL,NULL,NULL),
(12,'A0003','Wik Koi','Blitar','Sunyoto','Denpasar',34,65,2,2,'ikan/3766b44860ee5462ef1fd2e26483c4ce.jpg','2019-09-14 08:27:56',1,NULL,NULL,NULL,NULL),
(13,'A0003','Wik Koi','Blitar','Sunyoto','Denpasar',45,25,1,1,'ikan/24402ccc8c76f2aaf3305d9c54bfec19.jpg','2019-09-14 08:27:56',1,NULL,NULL,NULL,NULL),
(14,'A0003','Wik Koi','Blitar','Sunyoto','Denpasar',40,15,2,1,'ikan/af14f8998dd2564fab0a2c948e92e4fc.jpg','2019-09-14 08:27:56',1,NULL,NULL,NULL,NULL),
(15,'A0003','Wik Koi','Blitar','Sunyoto','Denpasar',41,22,2,1,'ikan/01754e66bef27a49db07fb3c4f8b5b81.jpg','2019-09-14 08:27:56',1,NULL,NULL,NULL,NULL),
(16,'A0003','Wik Koi','Blitar','Sunyoto','Denpasar',44,17,2,1,'ikan/da380e194e3f90041dd131dce193e616.png','2019-09-14 08:27:56',1,NULL,NULL,NULL,NULL),
(22,'A0005','Ariya Koi','Blitar','Zaenuri','Probolinggo',34,9,2,1,'ikan/49a4ed7fe9224413959ae14cd70a758d.jpg','2019-09-14 08:28:05',1,NULL,NULL,NULL,NULL),
(23,'A0005','Ariya Koi','Blitar','Zaenuri','Probolinggo',47,14,1,1,'ikan/589968e36ecb81bcf15aaca5eb3a5dc1.jpg','2019-09-14 08:28:05',1,NULL,NULL,NULL,NULL),
(24,'A0005','Ariya Koi','Blitar','Zaenuri','Probolinggo',33,25,2,1,'ikan/04bc4495d5bdb373858f5e73d46e7326.jpg','2019-09-14 08:28:05',1,NULL,NULL,NULL,NULL),
(25,'A0005','Ariya Koi','Blitar','Zaenuri','Probolinggo',44,14,1,1,'ikan/752eaeb222f57e5dc126ac4b75fdb71d.jpg','2019-09-14 08:28:05',1,NULL,NULL,NULL,NULL),
(29,'A0004','Bang Ardi','Tulungagung','Muljadi','Tulungagung',36,12,1,1,'ikan/ad093895d75a64bb691786b8b83e47b3.jpg','2019-09-14 08:28:09',1,NULL,NULL,NULL,NULL),
(30,'A0004','Bang Ardi','Tulungagung','Muljadi','Tulungagung',32,53,1,2,'ikan/5f352f0e59e74cca5db74fa99ea9d85b.JPG','2019-09-14 08:28:09',1,NULL,NULL,NULL,NULL),
(31,'A0004','Bang Ardi','Tulungagung','Muljadi','Tulungagung',32,10,1,1,'ikan/a82e5ff35ce6241949c1bbdea9b1a578.jpg','2019-09-14 08:28:09',1,NULL,NULL,NULL,NULL),
(32,'A0004','Bang Ardi','Tulungagung','Muljadi','Tulungagung',49,17,2,1,'ikan/57496d6198734ae5c50779915a536935.jpg','2019-09-14 08:28:09',1,NULL,NULL,NULL,NULL),
(36,'A0006','Kimi koi','Magetan','Agus Hartopo','Semarang',32,10,1,1,'ikan/d25da60c6de2e0cbaecfc40c839a1a96.jpg','2019-09-14 08:28:13',1,NULL,NULL,NULL,NULL),
(37,'A0006','Kimi koi','Magetan','Agus Hartopo','Semarang',33,10,1,1,'ikan/ed502a659e3ab97db22b01349397f42f.jpg','2019-09-14 08:28:13',1,NULL,NULL,NULL,NULL),
(38,'A0006','Kimi koi','Magetan','Agus Hartopo','Semarang',34,10,1,1,'ikan/860b1c9c0818ed181d4b54193bac451f.jpg','2019-09-14 08:28:13',1,NULL,NULL,NULL,NULL),
(39,'A0007','Kura Besi','Semarang','Hendro Kartiko','Jakarta',35,10,1,1,'ikan/80b0bd655749600cf62d02a00242e304.jpg','2019-09-14 08:28:17',1,NULL,NULL,NULL,NULL),
(40,'A0007','Kura Besi','Semarang','Hendro Kartiko','Jakarta',36,10,1,1,'ikan/9bab7dca8ef8f0b69dca27018f911bdb.jpg','2019-09-14 08:28:17',1,NULL,NULL,NULL,NULL),
(41,'A0007','Kura Besi','Semarang','Hendro Kartiko','Jakarta',37,10,2,1,'ikan/dc561cb9d5d3b2ca8d2c243cebf95680.jpg','2019-09-14 08:28:17',1,NULL,NULL,NULL,NULL),
(42,'A0007','Kura Besi','Semarang','Hendro Kartiko','Jakarta',38,9,1,1,'ikan/4da33841074d720b28f8fa9e8778812b.jpg','2019-09-14 08:28:17',1,NULL,NULL,NULL,NULL),
(43,'A0007','Kura Besi','Semarang','Hendro Kartiko','Jakarta',39,10,1,1,'ikan/6a5835d85652b5e887c4df848ec1c98e.jpg','2019-09-14 08:28:17',1,NULL,NULL,NULL,NULL),
(44,'A0007','Kura Besi','Semarang','Hendro Kartiko','Jakarta',40,10,2,1,'ikan/60a95fe98f39434cd35fee5a78297227.jpg','2019-09-14 08:28:17',1,NULL,NULL,NULL,NULL),
(45,'A0007','Kura Besi','Semarang','Hendro Kartiko','Jakarta',41,10,1,1,'ikan/64c2385080917d176839d22458d740d3.jpg','2019-09-14 08:28:17',1,NULL,NULL,NULL,NULL),
(46,'A0007','Kura Besi','Semarang','Hendro Kartiko','Jakarta',42,10,1,1,'ikan/a4af53e8993a31a41d3da0e3dd67e024.jpg','2019-09-14 08:28:17',1,NULL,NULL,NULL,NULL),
(47,'A0007','Kura Besi','Semarang','Hendro Kartiko','Jakarta',43,10,1,1,'ikan/0dfb651319508c7cf5d25ba6f7a3cdf0.jpg','2019-09-14 08:28:17',1,NULL,NULL,NULL,NULL),
(48,'A0007','Kura Besi','Semarang','Hendro Kartiko','Jakarta',44,10,1,1,'ikan/e0472f51f6045cfa08aa603d086def90.jpg','2019-09-14 08:28:17',1,NULL,NULL,NULL,NULL),
(54,'A0008','Saleho Koi','Nganjuk','Sarintul','Kediri',32,9,1,1,'ikan/1c86c73122c92f17c9d9b15dce50e9b9.jpg','2019-09-14 08:29:01',1,NULL,NULL,NULL,NULL),
(55,'A0008','Saleho Koi','Nganjuk','Sarintul','Kediri',32,8,1,1,'ikan/bc8a51db5bd8aa3eddd6a1a8feb51396.jpg','2019-09-14 08:29:01',1,NULL,NULL,NULL,NULL),
(56,'A0008','Saleho Koi','Nganjuk','Sarintul','Kediri',32,10,1,1,'ikan/1b4b9787bc51b05df98f9e24effffe63.jpg','2019-09-14 08:29:01',1,NULL,NULL,NULL,NULL),
(57,'A0008','Saleho Koi','Nganjuk','Sarintul','Kediri',32,9,1,1,'ikan/5b9509001593d32470bce0efe6cab19c.jpg','2019-09-14 08:29:01',1,NULL,NULL,NULL,NULL),
(58,'A0008','Saleho Koi','Nganjuk','Sarintul','Kediri',32,10,1,1,'ikan/d37a02c0cf21cdd4be93f4a3226c5f4a.jpg','2019-09-14 08:29:01',1,NULL,NULL,NULL,NULL),
(59,'A0008','Saleho Koi','Nganjuk','Sarintul','Kediri',32,8,1,1,'ikan/0885a94c9ef50100446b63e16f85c993.jpg','2019-09-14 08:29:01',1,NULL,NULL,NULL,NULL),
(60,'A0008','Saleho Koi','Nganjuk','Sarintul','Kediri',32,21,1,1,'ikan/88f29408d27b3e70bf06c88bdefd72ed.jpg','2019-09-14 08:29:01',1,NULL,NULL,NULL,NULL),
(61,'A0008','Saleho Koi','Nganjuk','Sarintul','Kediri',32,22,1,1,'ikan/d0c0edf14dc0daaab3fca273fd760ae2.jpg','2019-09-14 08:29:01',1,NULL,NULL,NULL,NULL),
(62,'A0008','Saleho Koi','Nganjuk','Sarintul','Kediri',32,23,1,1,'ikan/3a9845eeb172db731762506e946afc29.jpg','2019-09-14 08:29:01',1,NULL,NULL,NULL,NULL),
(63,'A0008','Saleho Koi','Nganjuk','Sarintul','Kediri',32,22,1,1,'ikan/ee7ae173a71a669f3229d03be84bb3b1.jpg','2019-09-14 08:29:01',1,NULL,NULL,NULL,NULL),
(64,'A0008','Saleho Koi','Nganjuk','Sarintul','Kediri',32,25,1,1,'ikan/60c70a691f3b9f14009014b15a32bcf7.jpg','2019-09-14 08:29:01',1,NULL,NULL,NULL,NULL),
(69,'A0009','Istimiwir Koi','Kediri','Soni','Balikpapan',32,11,1,1,'ikan/c24b8e509c2911e0f3d7aed97ef39ab2.jpg','2019-09-14 08:29:05',1,NULL,NULL,NULL,NULL),
(70,'A0009','Istimiwir Koi','Kediri','Soni','Balikpapan',32,11,1,1,'ikan/2646cc0c2695bf6e08f4e398da1108de.jpg','2019-09-14 08:29:05',1,NULL,NULL,NULL,NULL),
(71,'A0009','Istimiwir Koi','Kediri','Soni','Balikpapan',32,12,1,1,'ikan/beba0bec04cb3feb1e9b9dae56d71cfb.jpg','2019-09-14 08:29:05',1,NULL,NULL,NULL,NULL),
(72,'A0009','Istimiwir Koi','Kediri','Soni','Balikpapan',32,13,1,1,'ikan/d3be654223a89e2c85f74060ef4ddf14.jpg','2019-09-14 08:29:05',1,NULL,NULL,NULL,NULL),
(73,'A0009','Istimiwir Koi','Kediri','Soni','Balikpapan',32,26,1,1,'ikan/7e266d9053221d7f1cc4383cbd141b3c.jpg','2019-09-14 08:29:05',1,NULL,NULL,NULL,NULL),
(74,'A0009','Istimiwir Koi','Kediri','Soni','Balikpapan',32,28,1,2,'ikan/d59477e1fdd3a85a01b1b91372cc03f3.jpg','2019-09-14 08:29:05',1,NULL,NULL,NULL,NULL),
(75,'A0009','Istimiwir Koi','Kediri','Soni','Balikpapan',32,28,1,1,'ikan/76efc87bfda4f465e487ace762ca89a1.jpg','2019-09-14 08:29:05',1,NULL,NULL,NULL,NULL),
(76,'A0009','Istimiwir Koi','Kediri','Soni','Balikpapan',32,28,1,1,'ikan/d30be6e026dae01d77551f195d6b3246.jpg','2019-09-14 08:29:05',1,NULL,NULL,NULL,NULL),
(77,'A0009','Istimiwir Koi','Kediri','Soni','Balikpapan',32,26,1,1,'ikan/8f1c71e6d792b3758d612d8cd11a8610.jpg','2019-09-14 08:29:05',1,NULL,NULL,NULL,NULL),
(84,'A0010','Isitimiwir Koi','Kediri','Setiawan','Samarinda',32,17,1,1,'ikan/55900399c9ceffd401249a0b345fa233.jpg','2019-09-14 08:29:08',1,NULL,NULL,NULL,NULL),
(85,'A0010','Isitimiwir Koi','Kediri','Setiawan','Samarinda',32,18,1,1,'ikan/8a38cf45dbcfe3c913a7b4357d7997ce.jpg','2019-09-14 08:29:08',1,NULL,NULL,NULL,NULL),
(86,'A0010','Isitimiwir Koi','Kediri','Setiawan','Samarinda',32,18,1,1,'ikan/5987c4576f16bc404f604bdc308a0913.jpg','2019-09-14 08:29:08',1,NULL,NULL,NULL,NULL),
(87,'A0010','Isitimiwir Koi','Kediri','Setiawan','Samarinda',32,18,1,1,'ikan/61cddf367052537dfcb639ee4b16b4a3.jpg','2019-09-14 08:29:08',1,NULL,NULL,NULL,NULL),
(88,'A0010','Isitimiwir Koi','Kediri','Setiawan','Samarinda',32,19,1,1,'ikan/f21189e6114a87b6f8e3256e015c29c4.jpg','2019-09-14 08:29:08',1,NULL,NULL,NULL,NULL),
(89,'A0010','Isitimiwir Koi','Kediri','Setiawan','Samarinda',32,33,1,1,'ikan/fb65a154a431a002f4b89f17df89a111.jpg','2019-09-14 08:29:08',1,NULL,NULL,NULL,NULL),
(90,'A0010','Isitimiwir Koi','Kediri','Setiawan','Samarinda',32,33,1,1,'ikan/a504a37c7aa5737640a5c6f0f708fcc3.jpg','2019-09-14 08:29:08',1,NULL,NULL,NULL,NULL),
(91,'A0010','Isitimiwir Koi','Kediri','Setiawan','Samarinda',32,33,1,1,'ikan/77eb524bb6b552ef18c058c8c497d81c.jpg','2019-09-14 08:29:08',1,NULL,NULL,NULL,NULL),
(92,'A0010','Isitimiwir Koi','Kediri','Setiawan','Samarinda',32,33,1,1,'ikan/0c83d2c40c3c5d5733fa588afd5d4577.jpg','2019-09-14 08:29:08',1,NULL,NULL,NULL,NULL),
(93,'A0010','Isitimiwir Koi','Kediri','Setiawan','Samarinda',32,32,1,1,'ikan/586c5e7ba15a9b4db88db6f200674700.jpg','2019-09-14 08:29:08',1,NULL,NULL,NULL,NULL),
(99,'A0013','Almaira Koi','Mojokerto','Nanda','Malang',32,43,1,1,'ikan/a421a2316d3b544508079ae4edb9875c.jpg','2019-09-14 08:29:12',1,NULL,NULL,NULL,NULL),
(100,'A0013','Almaira Koi','Mojokerto','Nanda','Malang',32,43,1,1,'ikan/ebd9058ac7e18de46dfe96c4c6557d32.jpg','2019-09-14 08:29:12',1,NULL,NULL,NULL,NULL),
(101,'A0013','Almaira Koi','Mojokerto','Nanda','Malang',32,43,1,1,'ikan/e9d74da7e1a779e3eef57c31edf0cb48.jpg','2019-09-14 08:29:12',1,NULL,NULL,NULL,NULL),
(102,'A0013','Almaira Koi','Mojokerto','Nanda','Malang',32,43,1,1,'ikan/e2c251c599106da48674e2cc3f5002b2.jpg','2019-09-14 08:29:12',1,NULL,NULL,NULL,NULL),
(106,'A0015','Code Koi','Kediri','Koirul','Nganjuk',32,38,1,1,'ikan/6c813a5cd2f08a1bb1ff8fc5b8582829.jpg','2019-09-14 08:29:15',1,NULL,NULL,NULL,NULL),
(107,'A0015','Code Koi','Kediri','Koirul','Nganjuk',32,38,1,1,'ikan/b508b6ca05efe82fee50313bf29f4e1c.jpg','2019-09-14 08:29:15',1,NULL,NULL,NULL,NULL),
(108,'A0015','Code Koi','Kediri','Koirul','Nganjuk',32,38,2,1,'ikan/a18900b20d24155810d0170a9d689da3.jpg','2019-09-14 08:29:15',1,NULL,NULL,NULL,NULL),
(109,'A0015','Code Koi','Kediri','Koirul','Nganjuk',32,38,2,1,'ikan/290582d3702cd8d8c5412868fa132d81.jpg','2019-09-14 08:29:15',1,NULL,NULL,NULL,NULL),
(110,'A0015','Code Koi','Kediri','Koirul','Nganjuk',32,38,2,1,'ikan/c9e5ed32c5a8ef15395cd048bbbfb661.jpg','2019-09-14 08:29:15',1,NULL,NULL,NULL,NULL),
(111,'A0015','Code Koi','Kediri','Koirul','Nganjuk',32,38,1,1,'ikan/e85b9b39fc457121ac03f5616e4a989c.jpg','2019-09-14 08:29:15',1,NULL,NULL,NULL,NULL),
(113,'A0016','Dul Koi','Malang','Koirul','Pasuruan',34,44,1,1,'ikan/7b7e7c60846118e50801285f659530c6.jpeg','2019-09-14 08:29:19',1,NULL,NULL,NULL,NULL),
(114,'A0016','Dul Koi','Malang','Koirul','Pasuruan',45,25,1,1,'ikan/42ff8ed6d38c87e3f4e2520b92f4751d.jpg','2019-09-14 08:29:19',1,NULL,NULL,NULL,NULL),
(116,'A0017','Code Koi','Kediri','Supri','Malang',48,25,1,1,'ikan/a4a6e279ddf9df144b5f051cbd36ae15.jpg','2019-09-14 08:46:20',1,NULL,NULL,NULL,NULL),
(117,'A0017','Code Koi','Kediri','Supri','Malang',45,25,2,1,'ikan/d442bba56e67b6c3bbb51eeee140f6c5.jpg','2019-09-14 08:46:20',1,NULL,NULL,NULL,NULL),
(119,'A0018','Code Koi','Kediri','Nanag Armando','Blitar',37,10,1,1,'ikan/7ac6ed2316672b7eb2a42a5754426dc3.jpg','2019-09-14 08:46:23',1,NULL,NULL,NULL,NULL),
(120,'A0018','Code Koi','Kediri','Nanag Armando','Blitar',33,10,1,1,'ikan/565ff180281049a385357c935668a495.jpg','2019-09-14 08:46:23',1,NULL,NULL,NULL,NULL),
(121,'A0018','Code Koi','Kediri','Nanag Armando','Blitar',49,20,1,1,'ikan/b175b73b06f694fafcec66ab89bd4453.jpg','2019-09-14 08:46:23',1,NULL,NULL,NULL,NULL),
(122,'A0019','Code Koi','Kediri','Jono','Ngawi',45,25,2,1,'ikan/7db45c47ce7b504606487d9acb2b27e6.jpg','2019-09-14 08:46:27',1,'2019-09-14 04:31:48',1,'2019-09-14 04:36:46',1),
(123,'A0019','Code Koi','Kediri','Jono','Ngawi',33,50,1,1,'ikan/bf0f1a7c4e40b42117e0eda0c3ba3a81.jpg','2019-09-14 08:46:27',1,NULL,NULL,NULL,NULL),
(125,'A0020','Deenha Koi','Lumajang','Nardi','Lumajang',33,10,1,1,'ikan/afdd52baf18beb5cf0f043b5eec60bde.jpg','2019-09-14 08:46:30',1,NULL,NULL,NULL,NULL),
(126,'A0020','Deenha Koi','Lumajang','Nardi','Lumajang',50,14,1,1,'ikan/07e443cfe6f9384ddbbdcb09c66a7af6.jpg','2019-09-14 08:46:30',1,NULL,NULL,NULL,NULL),
(128,'A0021','Deenha Koi','Lumajang','Muljadi','Tulungagung',49,15,1,1,'ikan/8225e90f6d03581449a539a2f7e80020.jpg','2019-09-14 08:46:34',1,NULL,NULL,NULL,NULL),
(129,'A0021','Deenha Koi','Lumajang','Muljadi','Tulungagung',34,23,2,1,'ikan/715ce0c3c3f34880ea0691120edca8b8.jpg','2019-09-14 08:46:34',1,NULL,NULL,NULL,NULL),
(130,'A0021','Deenha Koi','Lumajang','Muljadi','Tulungagung',38,18,1,1,'ikan/e411292c8dd60b4f4e348812d8316ea9.jpg','2019-09-14 08:46:34',1,NULL,NULL,NULL,NULL),
(131,'A0021','Deenha Koi','Lumajang','Muljadi','Tulungagung',37,45,2,1,'ikan/36ce3bef5286c48c4803bfa9ba549810.jpeg','2019-09-14 08:46:34',1,NULL,NULL,NULL,NULL),
(132,'A0021','Deenha Koi','Lumajang','Muljadi','Tulungagung',32,20,1,1,'ikan/7d1a16d4b9b69d9d00f8b028d2506356.jpg','2019-09-14 08:46:34',1,NULL,NULL,NULL,NULL),
(133,'A0021','Deenha Koi','Lumajang','Muljadi','Tulungagung',45,25,1,1,'ikan/a0a80fd1cdb985c12d46fb782c377f28.jpg','2019-09-14 08:46:34',1,NULL,NULL,NULL,NULL);

/*Table structure for table `ikanheader` */

DROP TABLE IF EXISTS `ikanheader`;

CREATE TABLE `ikanheader` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uniq_code` varchar(50) DEFAULT NULL,
  `namahandling` varchar(100) DEFAULT NULL,
  `kotahandling` varchar(100) DEFAULT NULL,
  `namaowner` varchar(100) DEFAULT NULL,
  `kotaowner` varchar(100) DEFAULT NULL,
  `no_kwitansi` int(5) DEFAULT NULL,
  `lunas` int(1) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=latin1;

/*Data for the table `ikanheader` */

insert  into `ikanheader`(`id`,`uniq_code`,`namahandling`,`kotahandling`,`namaowner`,`kotaowner`,`no_kwitansi`,`lunas`,`created_at`,`created_by`,`updated_at`,`updated_by`,`deleted_at`,`deleted_by`) values 
(35,'A0001','Dua D Team','Jakarta','Mustofa','Kediri',18,1,'2019-09-14 08:27:39',1,NULL,NULL,NULL,NULL),
(36,'A0002','Dua D Team','Jakarta','Dede Darmawan','Jakarta',18,1,'2019-09-14 08:27:51',1,NULL,NULL,NULL,NULL),
(37,'A0003','Wik Koi','Blitar','Sunyoto','Denpasar',NULL,NULL,'2019-09-14 08:27:56',1,NULL,NULL,NULL,NULL),
(38,'A0005','Ariya Koi','Blitar','Zaenuri','Probolinggo',NULL,NULL,'2019-09-14 08:28:05',1,NULL,NULL,NULL,NULL),
(39,'A0004','Bang Ardi','Tulungagung','Muljadi','Tulungagung',NULL,NULL,'2019-09-14 08:28:09',1,NULL,NULL,NULL,NULL),
(40,'A0006','Kimi koi','Magetan','Agus Hartopo','Semarang',NULL,NULL,'2019-09-14 08:28:13',1,NULL,NULL,NULL,NULL),
(41,'A0007','Kura Besi','Semarang','Hendro Kartiko','Jakarta',NULL,NULL,'2019-09-14 08:28:17',1,NULL,NULL,NULL,NULL),
(42,'A0008','Saleho Koi','Nganjuk','Sarintul','Kediri',NULL,NULL,'2019-09-14 08:29:01',1,NULL,NULL,NULL,NULL),
(43,'A0009','Istimiwir Koi','Kediri','Soni','Balikpapan',NULL,NULL,'2019-09-14 08:29:05',1,NULL,NULL,NULL,NULL),
(44,'A0010','Isitimiwir Koi','Kediri','Setiawan','Samarinda',NULL,NULL,'2019-09-14 08:29:08',1,NULL,NULL,NULL,NULL),
(45,'A0013','Almaira Koi','Mojokerto','Nanda','Malang',NULL,NULL,'2019-09-14 08:29:12',1,NULL,NULL,NULL,NULL),
(46,'A0015','Code Koi','Kediri','Koirul','Nganjuk',19,1,'2019-09-14 08:29:15',1,NULL,NULL,NULL,NULL),
(47,'A0016','Dul Koi','Malang','Koirul','Pasuruan',NULL,NULL,'2019-09-14 08:29:19',1,NULL,NULL,NULL,NULL),
(48,'A0017','Code Koi','Kediri','Supri','Malang',19,1,'2019-09-14 08:46:20',1,NULL,NULL,NULL,NULL),
(49,'A0018','Code Koi','Kediri','Nanag Armando','Blitar',19,1,'2019-09-14 08:46:23',1,NULL,NULL,NULL,NULL),
(50,'A0019','Code Koi','Kediri','Jono','Ngawi',19,1,'2019-09-14 08:46:27',1,NULL,NULL,NULL,NULL),
(51,'A0020','Deenha Koi','Lumajang','Nardi','Lumajang',NULL,NULL,'2019-09-14 08:46:30',1,NULL,NULL,NULL,NULL),
(52,'A0021','Deenha Koi','Lumajang','Muljadi','Tulungagung',NULL,NULL,'2019-09-14 08:46:34',1,NULL,NULL,NULL,NULL);

/*Table structure for table `juara` */

DROP TABLE IF EXISTS `juara`;

CREATE TABLE `juara` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `namajuara` varchar(100) DEFAULT NULL,
  `aliasjuara` varchar(100) DEFAULT NULL,
  `id_up` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `juara` */

insert  into `juara`(`id`,`namajuara`,`aliasjuara`,`id_up`) values 
(1,'Juara 1','1st',NULL),
(2,'Juara 2','2nd',1),
(3,'Juara 3','3rd',2),
(4,'Juara 4','4th',3),
(5,'Juara 5','5th',4),
(6,'Best In Size','BIS',NULL),
(7,'Champion','Champ',NULL);

/*Table structure for table `juara_kontes` */

DROP TABLE IF EXISTS `juara_kontes`;

CREATE TABLE `juara_kontes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `juara_id` int(11) DEFAULT NULL,
  `ikandetail_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `juara_kontes` */

/*Table structure for table `kontes` */

DROP TABLE IF EXISTS `kontes`;

CREATE TABLE `kontes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `namakontes` varchar(100) DEFAULT NULL,
  `tempat` varchar(100) DEFAULT NULL,
  `kota` varchar(100) DEFAULT NULL,
  `rekening` varchar(100) DEFAULT NULL,
  `daftar` int(1) DEFAULT NULL,
  `poin` int(11) DEFAULT NULL,
  `handling` int(1) DEFAULT NULL,
  `entri` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `kontes` */

insert  into `kontes`(`id`,`namakontes`,`tempat`,`kota`,`rekening`,`daftar`,`poin`,`handling`,`entri`) values 
(1,'Koi Festival 2019','GOR Patria','Blitar','BANK XXX - 12345678890 A.N. KOI FESTIVAL',0,0,0,0);

/*Table structure for table `kwitansidetail` */

DROP TABLE IF EXISTS `kwitansidetail`;

CREATE TABLE `kwitansidetail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kwitansi_id` int(11) DEFAULT NULL,
  `ikanheader_id` int(11) DEFAULT NULL,
  `uniq_code` varchar(50) DEFAULT NULL,
  `namahandling` varchar(100) DEFAULT NULL,
  `kotahandling` varchar(100) DEFAULT NULL,
  `namaowner` varchar(100) DEFAULT NULL,
  `kotaowner` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;

/*Data for the table `kwitansidetail` */

insert  into `kwitansidetail`(`id`,`kwitansi_id`,`ikanheader_id`,`uniq_code`,`namahandling`,`kotahandling`,`namaowner`,`kotaowner`) values 
(20,18,35,'A0001','Dua D Team','Jakarta','Mustofa','Kediri'),
(21,18,36,'A0002','Dua D Team','Jakarta','Dede Darmawan','Jakarta'),
(23,19,46,'A0015','Code Koi','Kediri','Koirul','Nganjuk'),
(24,19,48,'A0017','Code Koi','Kediri','Supri','Malang'),
(25,19,49,'A0018','Code Koi','Kediri','Nanag Armando','Blitar'),
(26,19,50,'A0019','Code Koi','Kediri','Jono','Ngawi');

/*Table structure for table `kwitansiheader` */

DROP TABLE IF EXISTS `kwitansiheader`;

CREATE TABLE `kwitansiheader` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tgl_kwitansi` date DEFAULT NULL,
  `jumlah` int(11) DEFAULT NULL,
  `lunas` int(1) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

/*Data for the table `kwitansiheader` */

insert  into `kwitansiheader`(`id`,`tgl_kwitansi`,`jumlah`,`lunas`,`created_at`,`created_by`,`updated_at`,`updated_by`) values 
(18,'2019-09-14',1175000,1,'2019-09-14 03:47:55',1,NULL,NULL),
(19,'2019-09-14',500000,1,'2019-09-14 03:52:03',1,NULL,NULL);

/*Table structure for table `login_attempts` */

DROP TABLE IF EXISTS `login_attempts`;

CREATE TABLE `login_attempts` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(45) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `login_attempts` */

/*Table structure for table `ms_menu` */

DROP TABLE IF EXISTS `ms_menu`;

CREATE TABLE `ms_menu` (
  `id_inc` int(11) NOT NULL AUTO_INCREMENT,
  `nama_menu` varchar(100) DEFAULT NULL,
  `link_menu` varchar(100) DEFAULT NULL,
  `parent` int(11) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `icon` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_inc`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=latin1;

/*Data for the table `ms_menu` */

insert  into `ms_menu`(`id_inc`,`nama_menu`,`link_menu`,`parent`,`sort`,`icon`) values 
(18,'Sistem','#',0,99,'fa fa-bandcamp'),
(19,'Group','group',18,1,'fa-user-secret'),
(20,'Pengguna','pengguna',18,2,'fa-users'),
(21,'Menu','menu',18,3,'fa-newspaper-o'),
(23,'Master Data','#',0,9,'fa fa-fort-awesome'),
(24,'Kontes','kontes',23,1,'-'),
(25,'Size Kontes','size',23,2,'-'),
(27,'Variety','variety',23,3,'-'),
(28,'Juara','juara',23,4,'-'),
(29,'List poin','poin',23,5,'-'),
(30,'Administrasi','#',0,2,'fa fa-drivers-license'),
(31,'Fish Entry','regisikan',30,1,'-'),
(32,'pembayaran','pembayaran',30,2,'-'),
(33,'peserta','peserta',30,3,'-'),
(34,'Penjurian','#',0,3,'fa fa-cubes'),
(35,'Kategori','penjurian/kategori',34,1,'-'),
(36,'BIS & Champion','penjurian/bischampion',34,2,'-'),
(37,'Dashboard','welcome',0,1,'fa fa-home');

/*Table structure for table `ms_privilege` */

DROP TABLE IF EXISTS `ms_privilege`;

CREATE TABLE `ms_privilege` (
  `id_inc` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) DEFAULT NULL,
  `ms_menu_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `is_created` int(11) DEFAULT NULL,
  `is_updated` int(11) DEFAULT NULL,
  `is_deleted` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_inc`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `ms_privilege` */

insert  into `ms_privilege`(`id_inc`,`group_id`,`ms_menu_id`,`status`,`is_created`,`is_updated`,`is_deleted`) values 
(1,1,18,1,1,1,1),
(2,1,19,1,1,1,1),
(3,1,20,1,1,1,1),
(4,1,21,1,1,1,1);

/*Table structure for table `penomoran` */

DROP TABLE IF EXISTS `penomoran`;

CREATE TABLE `penomoran` (
  `jenis` varchar(100) DEFAULT NULL,
  `nomor` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `penomoran` */

insert  into `penomoran`(`jenis`,`nomor`) values 
('no_ikan',7),
('kwitansi',1);

/*Table structure for table `plastik` */

DROP TABLE IF EXISTS `plastik`;

CREATE TABLE `plastik` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `namaplastik` varchar(20) DEFAULT NULL,
  `ukuranmin` int(11) DEFAULT NULL,
  `ukuranmax` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `plastik` */

insert  into `plastik`(`id`,`namaplastik`,`ukuranmin`,`ukuranmax`) values 
(1,'SS',1,10),
(2,'S',11,25),
(3,'M',26,40),
(4,'L',41,65);

/*Table structure for table `poinmaster` */

DROP TABLE IF EXISTS `poinmaster`;

CREATE TABLE `poinmaster` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ukuranmin` int(11) DEFAULT NULL,
  `ukuranmax` int(11) DEFAULT NULL,
  `juara_id` int(11) DEFAULT NULL,
  `poin` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `poinmaster` */

/*Table structure for table `ukuran` */

DROP TABLE IF EXISTS `ukuran`;

CREATE TABLE `ukuran` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ukuranMin` int(3) DEFAULT NULL,
  `ukuranMax` int(3) DEFAULT NULL,
  `namaukuran` varchar(50) DEFAULT NULL,
  `biaya` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

/*Data for the table `ukuran` */

insert  into `ukuran`(`id`,`ukuranMin`,`ukuranMax`,`namaukuran`,`biaya`) values 
(1,0,10,'Up to 10 cm',75000),
(2,11,15,'11 - 15 cm',100000),
(3,16,20,'16 - 20 cm',150000),
(4,21,25,'21 - 25 cm',200000),
(5,26,30,'26 - 30 cm',200000),
(6,31,35,'31 - 35 cm',250000),
(7,36,40,'36 - 40 cm',300000),
(8,41,45,'41 - 45 cm',400000),
(9,46,50,'46 - 50 cm',500000),
(10,51,55,'51 - 55 cm',600000),
(11,56,60,'56 - 60 cm',650000),
(12,61,65,'61 - 65 cm',750000);

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(45) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(254) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) unsigned DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) unsigned NOT NULL,
  `last_login` int(11) unsigned DEFAULT NULL,
  `active` tinyint(1) unsigned DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `users` */

insert  into `users`(`id`,`ip_address`,`username`,`password`,`salt`,`email`,`activation_code`,`forgotten_password_code`,`forgotten_password_time`,`remember_code`,`created_on`,`last_login`,`active`,`first_name`,`last_name`,`company`,`phone`) values 
(1,'127.0.0.1','administrator','$2a$07$SeBknntpZror9uyftVopmu61qg0ms8Qv1yV6FG.kQOSM.9QhmTo36','','admin@admin.com','',NULL,NULL,NULL,1268889823,1568424375,1,'Wahyu','Dewantoro','Infinity System Code','082330319913'),
(2,'::1','admin@mail.com','$2y$08$kZrBRnZxaWsFISwTpUXY1Oz4EE73l5vbJMxWMUBLh3SEvc5ODf7Gy',NULL,'admin@mail.com',NULL,NULL,NULL,NULL,1566546575,NULL,1,'qweqwe','dfsdf','sdffh','12312'),
(3,'127.0.0.1','mia@mail.com','$2y$08$NXXH2yCYYTP0FTXNIN1s9Oi5dF8NNOvwCTwoWV4pflnpo/b.hx//W',NULL,'mia@mail.com',NULL,NULL,NULL,NULL,1566621028,NULL,1,'Mia','Amalia','qwerty','0192237');

/*Table structure for table `users_groups` */

DROP TABLE IF EXISTS `users_groups`;

CREATE TABLE `users_groups` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  KEY `fk_users_groups_users1_idx` (`user_id`),
  KEY `fk_users_groups_groups1_idx` (`group_id`),
  CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

/*Data for the table `users_groups` */

insert  into `users_groups`(`id`,`user_id`,`group_id`) values 
(9,1,1),
(10,1,2),
(3,2,2),
(6,3,2);

/*Table structure for table `variety` */

DROP TABLE IF EXISTS `variety`;

CREATE TABLE `variety` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `namavariety` varchar(100) DEFAULT NULL,
  `kelas` varchar(50) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=latin1;

/*Data for the table `variety` */

insert  into `variety`(`id`,`namavariety`,`kelas`,`sort`) values 
(32,'Kohaku','A',1),
(33,'Taisho Sanshoku','A',2),
(34,'Showa Sanshoku','A',3),
(35,'Ghosiki','B',4),
(36,'Kujaku','B',5),
(37,'Kawarimono A','B',6),
(38,'Kinginrin A','B',7),
(39,'Shiro Utsuri','B',8),
(40,'Hikari Moyomono','C',9),
(41,'Doitsu','C',10),
(42,'Goromo','C',11),
(43,'Koromo','C',12),
(44,'Kinginrin B','C',13),
(45,'Tancho','C',14),
(46,'Hikari Utsurimono','D',15),
(47,'Asagi','D',16),
(48,'Sushui','D',17),
(49,'Bekko','D',18),
(50,'Hi Ki Utsurimono','D',19),
(51,'Kumunryu','D',20),
(52,'Hikari Mujimono','E',21),
(53,'Kawarimono B','E',22);

/*Table structure for table `xcartdetail` */

DROP TABLE IF EXISTS `xcartdetail`;

CREATE TABLE `xcartdetail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cartheader_id` int(11) DEFAULT NULL,
  `variety_id` int(11) DEFAULT NULL,
  `ukuran` int(3) DEFAULT NULL,
  `gender` int(1) DEFAULT NULL,
  `breeder` int(1) DEFAULT NULL,
  `gambar_ikan` varchar(100) DEFAULT NULL,
  `uniq_code` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cart_fk` (`cartheader_id`),
  CONSTRAINT `cart_fk` FOREIGN KEY (`cartheader_id`) REFERENCES `xcartheader` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=119 DEFAULT CHARSET=latin1;

/*Data for the table `xcartdetail` */

insert  into `xcartdetail`(`id`,`cartheader_id`,`variety_id`,`ukuran`,`gender`,`breeder`,`gambar_ikan`,`uniq_code`) values 
(42,NULL,53,10,1,1,'ikan/02cb2206b95058e63a895cbf60d7b36d.jpg','ZUME855'),
(43,NULL,45,38,1,1,'ikan/307326c7d75d56711d1264e42e1342aa.jpg','ZUME855'),
(44,NULL,41,56,1,1,'ikan/0fd356bbb018340f13ef6e4e0204e2fc.jpg','JDCBPGCC'),
(45,NULL,32,20,2,1,'ikan/802360383bd67dd586dbbd7dc0cfb722.jpg','JDCBPGCC'),
(46,NULL,34,45,2,1,'ikan/3626b88058f9230e3fa6bffb5620d1b8.jpg','SRZBB033'),
(47,NULL,45,10,1,1,'ikan/20e0494bc0ca67fa0c746e7148c6d391.jpg','SRZBB033'),
(48,NULL,32,35,1,1,'ikan/801975801d16827cd723cbc0e5745370.jpg','SRZBB033'),
(49,NULL,49,30,2,1,'ikan/b9a2ea34cfdfe817774750fa2acf9eb6.jpg','SRZBB033'),
(50,NULL,53,60,2,1,'ikan/da40c3342c6f13812a162756052ee049.jpg','SRZBB033'),
(51,NULL,34,65,2,2,'ikan/3766b44860ee5462ef1fd2e26483c4ce.jpg','SRZBB033'),
(52,NULL,45,25,1,1,'ikan/24402ccc8c76f2aaf3305d9c54bfec19.jpg','SRZBB033'),
(53,NULL,40,15,2,1,'ikan/af14f8998dd2564fab0a2c948e92e4fc.jpg','SRZBB033'),
(54,NULL,41,22,2,1,'ikan/01754e66bef27a49db07fb3c4f8b5b81.jpg','SRZBB033'),
(55,NULL,44,17,2,1,'ikan/da380e194e3f90041dd131dce193e616.png','SRZBB033'),
(56,NULL,36,12,1,1,'ikan/ad093895d75a64bb691786b8b83e47b3.jpg','1577CGPP'),
(57,NULL,32,53,1,2,'ikan/5f352f0e59e74cca5db74fa99ea9d85b.JPG','1577CGPP'),
(58,NULL,32,10,1,1,'ikan/a82e5ff35ce6241949c1bbdea9b1a578.jpg','1577CGPP'),
(59,NULL,49,17,2,1,'ikan/57496d6198734ae5c50779915a536935.jpg','1577CGPP'),
(60,NULL,34,9,2,1,'ikan/49a4ed7fe9224413959ae14cd70a758d.jpg','BKX68N'),
(61,NULL,47,14,1,1,'ikan/589968e36ecb81bcf15aaca5eb3a5dc1.jpg','BKX68N'),
(62,NULL,33,25,2,1,'ikan/04bc4495d5bdb373858f5e73d46e7326.jpg','BKX68N'),
(63,NULL,44,14,1,1,'ikan/752eaeb222f57e5dc126ac4b75fdb71d.jpg','BKX68N'),
(64,NULL,32,10,1,1,'ikan/d25da60c6de2e0cbaecfc40c839a1a96.jpg','KYLXE266'),
(65,NULL,33,10,1,1,'ikan/ed502a659e3ab97db22b01349397f42f.jpg','KYLXE266'),
(66,NULL,34,10,1,1,'ikan/860b1c9c0818ed181d4b54193bac451f.jpg','KYLXE266'),
(67,NULL,35,10,1,1,'ikan/80b0bd655749600cf62d02a00242e304.jpg','TC2TDBHH'),
(68,NULL,36,10,1,1,'ikan/9bab7dca8ef8f0b69dca27018f911bdb.jpg','TC2TDBHH'),
(69,NULL,37,10,2,1,'ikan/dc561cb9d5d3b2ca8d2c243cebf95680.jpg','TC2TDBHH'),
(70,NULL,38,9,1,1,'ikan/4da33841074d720b28f8fa9e8778812b.jpg','TC2TDBHH'),
(71,NULL,39,10,1,1,'ikan/6a5835d85652b5e887c4df848ec1c98e.jpg','TC2TDBHH'),
(72,NULL,40,10,2,1,'ikan/60a95fe98f39434cd35fee5a78297227.jpg','TC2TDBHH'),
(73,NULL,41,10,1,1,'ikan/64c2385080917d176839d22458d740d3.jpg','TC2TDBHH'),
(74,NULL,42,10,1,1,'ikan/a4af53e8993a31a41d3da0e3dd67e024.jpg','TC2TDBHH'),
(75,NULL,43,10,1,1,'ikan/0dfb651319508c7cf5d25ba6f7a3cdf0.jpg','TC2TDBHH'),
(76,NULL,44,10,1,1,'ikan/e0472f51f6045cfa08aa603d086def90.jpg','TC2TDBHH'),
(77,NULL,32,9,1,1,'ikan/1c86c73122c92f17c9d9b15dce50e9b9.jpg','QLNV1BDD'),
(78,NULL,32,8,1,1,'ikan/bc8a51db5bd8aa3eddd6a1a8feb51396.jpg','QLNV1BDD'),
(79,NULL,32,10,1,1,'ikan/1b4b9787bc51b05df98f9e24effffe63.jpg','QLNV1BDD'),
(80,NULL,32,9,1,1,'ikan/5b9509001593d32470bce0efe6cab19c.jpg','QLNV1BDD'),
(81,NULL,32,10,1,1,'ikan/d37a02c0cf21cdd4be93f4a3226c5f4a.jpg','QLNV1BDD'),
(82,NULL,32,8,1,1,'ikan/0885a94c9ef50100446b63e16f85c993.jpg','QLNV1BDD'),
(83,NULL,32,11,1,1,'ikan/c24b8e509c2911e0f3d7aed97ef39ab2.jpg','0Z1VHFTT'),
(84,NULL,32,11,1,1,'ikan/2646cc0c2695bf6e08f4e398da1108de.jpg','0Z1VHFTT'),
(85,NULL,32,12,1,1,'ikan/beba0bec04cb3feb1e9b9dae56d71cfb.jpg','0Z1VHFTT'),
(86,NULL,32,13,1,1,'ikan/d3be654223a89e2c85f74060ef4ddf14.jpg','0Z1VHFTT'),
(87,NULL,32,17,1,1,'ikan/55900399c9ceffd401249a0b345fa233.jpg','9EIUYTBB'),
(88,NULL,32,18,1,1,'ikan/8a38cf45dbcfe3c913a7b4357d7997ce.jpg','9EIUYTBB'),
(89,NULL,32,18,1,1,'ikan/5987c4576f16bc404f604bdc308a0913.jpg','9EIUYTBB'),
(90,NULL,32,18,1,1,'ikan/61cddf367052537dfcb639ee4b16b4a3.jpg','9EIUYTBB'),
(91,NULL,32,19,1,1,'ikan/f21189e6114a87b6f8e3256e015c29c4.jpg','9EIUYTBB'),
(92,NULL,32,21,1,1,'ikan/88f29408d27b3e70bf06c88bdefd72ed.jpg','QLNV1BDD'),
(93,NULL,32,22,1,1,'ikan/d0c0edf14dc0daaab3fca273fd760ae2.jpg','QLNV1BDD'),
(94,NULL,32,23,1,1,'ikan/3a9845eeb172db731762506e946afc29.jpg','QLNV1BDD'),
(95,NULL,32,22,1,1,'ikan/ee7ae173a71a669f3229d03be84bb3b1.jpg','QLNV1BDD'),
(96,NULL,32,25,1,1,'ikan/60c70a691f3b9f14009014b15a32bcf7.jpg','QLNV1BDD'),
(97,NULL,32,26,1,1,'ikan/7e266d9053221d7f1cc4383cbd141b3c.jpg','0Z1VHFTT'),
(98,NULL,32,28,1,2,'ikan/d59477e1fdd3a85a01b1b91372cc03f3.jpg','0Z1VHFTT'),
(99,NULL,32,28,1,1,'ikan/76efc87bfda4f465e487ace762ca89a1.jpg','0Z1VHFTT'),
(100,NULL,32,28,1,1,'ikan/d30be6e026dae01d77551f195d6b3246.jpg','0Z1VHFTT'),
(101,NULL,32,26,1,1,'ikan/8f1c71e6d792b3758d612d8cd11a8610.jpg','0Z1VHFTT'),
(102,NULL,32,43,1,1,'ikan/a421a2316d3b544508079ae4edb9875c.jpg','R6WHQ6KK'),
(103,NULL,32,43,1,1,'ikan/ebd9058ac7e18de46dfe96c4c6557d32.jpg','R6WHQ6KK'),
(104,NULL,32,43,1,1,'ikan/e9d74da7e1a779e3eef57c31edf0cb48.jpg','R6WHQ6KK'),
(105,NULL,32,43,1,1,'ikan/e2c251c599106da48674e2cc3f5002b2.jpg','R6WHQ6KK'),
(106,NULL,32,33,1,1,'ikan/fb65a154a431a002f4b89f17df89a111.jpg','9EIUYTBB'),
(107,NULL,32,33,1,1,'ikan/a504a37c7aa5737640a5c6f0f708fcc3.jpg','9EIUYTBB'),
(108,NULL,32,33,1,1,'ikan/77eb524bb6b552ef18c058c8c497d81c.jpg','9EIUYTBB'),
(109,NULL,32,33,1,1,'ikan/0c83d2c40c3c5d5733fa588afd5d4577.jpg','9EIUYTBB'),
(110,NULL,32,32,1,1,'ikan/586c5e7ba15a9b4db88db6f200674700.jpg','9EIUYTBB'),
(111,NULL,32,38,1,1,'ikan/6c813a5cd2f08a1bb1ff8fc5b8582829.jpg','IS9QDRXX'),
(112,NULL,32,38,1,1,'ikan/b508b6ca05efe82fee50313bf29f4e1c.jpg','IS9QDRXX'),
(113,NULL,32,38,2,1,'ikan/a18900b20d24155810d0170a9d689da3.jpg','IS9QDRXX'),
(114,NULL,32,38,2,1,'ikan/290582d3702cd8d8c5412868fa132d81.jpg','IS9QDRXX'),
(115,NULL,32,38,2,1,'ikan/c9e5ed32c5a8ef15395cd048bbbfb661.jpg','IS9QDRXX'),
(116,NULL,32,38,1,1,'ikan/e85b9b39fc457121ac03f5616e4a989c.jpg','IS9QDRXX'),
(117,NULL,34,44,1,1,'ikan/7b7e7c60846118e50801285f659530c6.jpeg','1K4G1U44'),
(118,NULL,45,25,1,1,'ikan/42ff8ed6d38c87e3f4e2520b92f4751d.jpg','1K4G1U44');

/*Table structure for table `xcartheader` */

DROP TABLE IF EXISTS `xcartheader`;

CREATE TABLE `xcartheader` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uniq_code` varchar(100) DEFAULT NULL,
  `namahandling` varchar(100) DEFAULT NULL,
  `kotahandling` varchar(100) DEFAULT NULL,
  `telphandling` varchar(20) DEFAULT NULL,
  `namaowner` varchar(100) DEFAULT NULL,
  `kotaowner` varchar(100) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `checkout_by` int(11) DEFAULT NULL,
  `checkout_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

/*Data for the table `xcartheader` */

insert  into `xcartheader`(`id`,`uniq_code`,`namahandling`,`kotahandling`,`telphandling`,`namaowner`,`kotaowner`,`created_by`,`created_at`,`updated_by`,`updated_at`,`checkout_by`,`checkout_at`) values 
(1,'ZUME855','Dua D Team','Jakarta',NULL,'Mustofa','Kediri',NULL,NULL,NULL,NULL,NULL,NULL),
(2,'JDCBPGCC','Dua D Team','Jakarta',NULL,'Dede Darmawan','Jakarta',NULL,NULL,NULL,NULL,NULL,NULL),
(3,'SRZBB033','Wik Koi','Blitar',NULL,'Sunyoto','Denpasar',NULL,NULL,NULL,NULL,NULL,NULL),
(4,'1577CGPP','Bang Ardi','Tulungagung',NULL,'Muljadi','Tulungagung',NULL,NULL,NULL,NULL,NULL,NULL),
(5,'BKX68N','Ariya Koi','Blitar',NULL,'Zaenuri','Probolinggo',NULL,NULL,NULL,NULL,NULL,NULL),
(6,'KYLXE266','Kimi koi','Magetan',NULL,'Agus Hartopo','Semarang',NULL,NULL,NULL,NULL,NULL,NULL),
(7,'TC2TDBHH','Kura Besi','Semarang',NULL,'Hendro Kartiko','Jakarta',NULL,NULL,NULL,NULL,NULL,NULL),
(8,'QLNV1BDD','Saleho Koi','Nganjuk',NULL,'Sarintul','Kediri',NULL,NULL,NULL,NULL,NULL,NULL),
(9,'0Z1VHFTT','Istimiwir Koi','Kediri',NULL,'Soni','Balikpapan',NULL,NULL,NULL,NULL,NULL,NULL),
(10,'9EIUYTBB','Isitimiwir Koi','Kediri',NULL,'Setiawan','Samarinda',NULL,NULL,NULL,NULL,NULL,NULL),
(11,'QLNV1BDD','Code Koi','Kediri',NULL,'Wahyu ','Kediri',NULL,NULL,NULL,NULL,NULL,NULL),
(12,'0Z1VHFTT','Code Koi','Kediri',NULL,'Mia Amalia','Kediri',NULL,NULL,NULL,NULL,NULL,NULL),
(13,'R6WHQ6KK','Almaira Koi','Mojokerto',NULL,'Nanda','Malang',NULL,NULL,NULL,NULL,NULL,NULL),
(14,'9EIUYTBB','Code Koi','Kediri',NULL,'Burhan','Madiun',NULL,NULL,NULL,NULL,NULL,NULL),
(15,'IS9QDRXX','Code Koi','Kediri',NULL,'Koirul','Nganjuk',NULL,NULL,NULL,NULL,NULL,NULL),
(16,'1K4G1U44','Dul Koi','Malang',NULL,'Koirul','Pasuruan',NULL,NULL,NULL,NULL,NULL,NULL);

/* Function  structure for function  `ambilnoikan` */

/*!50003 DROP FUNCTION IF EXISTS `ambilnoikan` */;
DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` FUNCTION `ambilnoikan`() RETURNS int(11)
BEGIN
	DECLARE nomer_ INT;
	SELECT nomor  INTO nomer_ FROM penomoran WHERE jenis='no_ikan';
	UPDATE penomoran SET nomor=nomor+1 WHERE jenis='no_ikan';
	return nomer_;
	-- RETURN CONCAT(DATE_FORMAT(NOW(), '%h%s%i'),LPAD(nomer_,5,'0'));
    END */$$
DELIMITER ;

/* Function  structure for function  `ambilnokwitansi` */

/*!50003 DROP FUNCTION IF EXISTS `ambilnokwitansi` */;
DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` FUNCTION `ambilnokwitansi`() RETURNS int(11)
BEGIN
	DECLARE nomer_ INT;
	SELECT nomor  INTO nomer_ FROM penomoran WHERE jenis='kwitansi';
	UPDATE penomoran SET nomor=nomor+1 WHERE jenis='kwitansi';
	return nomer_;
	-- RETURN CONCAT(DATE_FORMAT(NOW(), '%h%s%i'),LPAD(nomer_,5,'0'));
    END */$$
DELIMITER ;

/* Function  structure for function  `biaya` */

/*!50003 DROP FUNCTION IF EXISTS `biaya` */;
DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` FUNCTION `biaya`(vu int) RETURNS int(11)
BEGIN
  DECLARE res INT;
  IF vu > 65
  THEN
  SELECT
    biaya into res
  FROM
    ukuran
  WHERE ukuranMin <= 65
    AND ukuranMax >= 65;
  ELSE
  SELECT
    biaya INTO res
  FROM
    ukuran
  WHERE ukuranMin <= vu
    AND ukuranMax >= vu;
  END IF;
  RETURN res;
END */$$
DELIMITER ;

/* Function  structure for function  `digits` */

/*!50003 DROP FUNCTION IF EXISTS `digits` */;
DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` FUNCTION `digits`( str CHAR(32) ) RETURNS char(32) CHARSET latin1
BEGIN
  DECLARE i, len SMALLINT DEFAULT 1;
  DECLARE ret CHAR(32) DEFAULT '';
  DECLARE c CHAR(1);

  IF str IS NULL
  THEN 
    RETURN "";
  END IF;

  SET len = CHAR_LENGTH( str );
  REPEAT
    BEGIN
      SET c = MID( str, i, 1 );
      IF c BETWEEN '0' AND '9' THEN 
        SET ret=CONCAT(ret,c);
      END IF;
      SET i = i + 1;
    END;
  UNTIL i > len END REPEAT;
  RETURN ret;
END */$$
DELIMITER ;

/* Function  structure for function  `getplastik` */

/*!50003 DROP FUNCTION IF EXISTS `getplastik` */;
DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` FUNCTION `getplastik`(`uk` INT) RETURNS varchar(11) CHARSET latin1
BEGIN
DECLARE res VARCHAR(11);
		SELECT namaplastik INTO res FROM plastik WHERE ukuranmin<=uk AND ukuranmax>=uk;
	RETURN res;
    END */$$
DELIMITER ;

/* Function  structure for function  `ukuranalias` */

/*!50003 DROP FUNCTION IF EXISTS `ukuranalias` */;
DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` FUNCTION `ukuranalias`(vu int) RETURNS varchar(100) CHARSET latin1
BEGIN
    declare res varchar(100);
	SELECt namaukuran INTO res
	FROM ukuran WHERE ukuranMin <= vu  AND ukuranMax >= vu;
    return res;

    END */$$
DELIMITER ;

/* Function  structure for function  `getkode` */

/*!50003 DROP FUNCTION IF EXISTS `getkode` */;
DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` FUNCTION `getkode`(vid int) RETURNS varchar(100) CHARSET latin1
BEGIN
	declare res varchar(100);
		
SELECT CONCAT(
              SUBSTRING('ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789', RAND(@seed:=ROUND(RAND(vid)*4294967296))*36+1, 1),
              SUBSTRING('ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789', RAND(@seed:=ROUND(RAND(@seed)*4294967296))*36+1, 1),
              SUBSTRING('ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789', RAND(@seed:=ROUND(RAND(@seed)*4294967296))*36+1, 1),
              SUBSTRING('ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789', RAND(@seed:=ROUND(RAND(@seed)*4294967296))*36+1, 1),
              SUBSTRING('ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789', RAND(@seed:=ROUND(RAND(@seed)*4294967296))*36+1, 1),
              SUBSTRING('ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789', RAND(@seed:=ROUND(RAND(@seed)*4294967296))*36+1, 1),
              SUBSTRING('ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789', RAND(@seed:=ROUND(RAND(@seed)*4294967296))*36+1, 1),
              SUBSTRING('ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789', RAND(@seed)*36+1, 1)
             ) into res;
	return res;
    END */$$
DELIMITER ;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
