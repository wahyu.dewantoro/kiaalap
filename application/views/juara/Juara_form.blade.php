@extends('layouts.master')
@section('judul')
<h3>Juara
</h3>
@endsection
@section('content')
<div class="row">
    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
        <div class="sparkline9-list responsive-mg-b-30">
            <div class="sparkline9-graph">
                <div class="basic-login-form-ad">

                    <div class="basic-login-inner">
                        <form action="{{ $action }}" method="post">
                            <?= csrfToken() ?>

                            <div class="form-group">
                                <label for="varchar">Juara <?php echo form_error('namajuara') ?></label>
                                <input type="text" class="form-control" name="namajuara" id="namajuara" placeholder="Nama juara" value="<?php echo $namajuara; ?>" />
                            </div>
                            <div class="form-group">
                                <label for="varchar">Alias <?php echo form_error('aliasjuara') ?></label>
                                <input type="text" class="form-control" name="aliasjuara" id="aliasjuara" placeholder="Alias juara" value="<?php echo $aliasjuara; ?>" />
                            </div>
                            <div class="form-group">
                                <label for="int">Juara Up </label>
                                <!-- <input type="text"  placeholder="Id Up" value="<?php echo $id_up; ?>" /> -->
                                <select class="form-control" name="id_up" id="id_up">
                                    <option value="">null</option>
                                    @foreach($lup as $lup)
                                    <option value="{{ $lup->id }}">{{ $lup->namajuara }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <input type="hidden" name="id" value="<?php echo $id; ?>" />
                            <div class="login-btn-inner">
                                <div class="login-horizental">
                                    <button class="btn btn-sm btn-primary login-submit-cs" type="submit"><i class="fa fa-save"></i> Simpan</button>
                                    {!! anchor('juara','<i class="fa fa-close"></i> Batal','class="btn btn-custon-four btn-warning"') !!}
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection