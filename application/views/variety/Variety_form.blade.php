
@extends('layouts.master')
@section('judul')
<h3>Variety
</h3>
@endsection
@section('content')
<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="sparkline9-list responsive-mg-b-30">
            <div class="sparkline9-graph">
                <div class="basic-login-form-ad">
                    
                            <div class="basic-login-inner">
                                <form action="{{ $action }}" method="post">
                                    <?= csrfToken() ?>
                                    
	    <div class="form-group">
                                                <label for="varchar">Namavariety <?php echo form_error('namavariety') ?></label>
                                                <input type="text" class="form-control" name="namavariety" id="namavariety" placeholder="Namavariety" value="<?php echo $namavariety; ?>" />
                                            </div>
	    <div class="form-group">
                                                <label for="varchar">Kelas <?php echo form_error('kelas') ?></label>
                                                <input type="text" class="form-control" name="kelas" id="kelas" placeholder="Kelas" value="<?php echo $kelas; ?>" />
                                            </div>
	    <div class="form-group">
                                                <label for="int">Sort <?php echo form_error('sort') ?></label>
                                                <input type="text" class="form-control" name="sort" id="sort" placeholder="Sort" value="<?php echo $sort; ?>" />
                                            </div>
	    <input type="hidden" name="id" value="<?php echo $id; ?>" />
                                    <div class="login-btn-inner">
                                    <div class="login-horizental">
                                    <button class="btn btn-sm btn-primary login-submit-cs" type="submit"><i class="fa fa-save"></i> Simpan</button>
                                    {!! anchor('variety','<i class="fa fa-close"></i> Batal','class="btn btn-custon-four btn-warning"') !!}
                                    </div>
                                    </div>
                                </form>
                            </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection