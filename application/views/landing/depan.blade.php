<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="https://getbootstrap.com/favicon.ico">
    <title>Decode</title>
    <link href="<?= base_url() ?>assets/cssdepan/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <link href="<?= base_url() ?>assets/cssdepan/offcanvas.css" rel="stylesheet">
    @yield('css')
</head>

<body class="bg-light">
    <nav class="navbar navbar-expand-md fixed-top navbar-dark bg-dark">
        <a class="navbar-brand" href="#">Garum Koi Show</a>
        <button class="navbar-toggler p-0 border-0" type="button" data-toggle="offcanvas">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="navbar-collapse offcanvas-collapse" id="navbarsExampleDefault">
            <?php 
                $k=getKontes();
            ?>

            <ul class="navbar-nav mr-auto">
                @if($k->daftar==1)
                <li class="nav-item active">
                    <a class="nav-link" href="{{ base_url().'auth/registrasi' }}">Registrasi</a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="{{ base_url().'auth/login' }}">Login </a>
                </li>
                @endif
            </ul>
        </div>
    </nav>
    <main role="main" class="container">
        @yield('content')
    </main>
    <script src="<?= base_url() ?>assets/cssdepan/jquery/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script>
        window.jQuery || document.write('<script src="<?= base_url() ?>assets/cssdepan/assets/js/vendor/jquery-slim.min.js"><\/script>')
    </script>
    <script src="<?= base_url() ?>assets/cssdepan/assets/js/vendor/popper.min.js"></script>
    <script src="<?= base_url() ?>assets/cssdepan/dist/js/bootstrap.min.js"></script>
    <script src="<?= base_url() ?>assets/cssdepan/assets/js/vendor/holder.min.js"></script>
    <script src="<?= base_url() ?>assets/cssdepan/offcanvas.js"></script>
    @yield('script')
</body>

</html>