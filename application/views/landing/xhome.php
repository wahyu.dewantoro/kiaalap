<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="https://getbootstrap.com/favicon.ico">

    <title>Decode</title>
    <link href="<?= base_url() ?>assets/cssdepan/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <link href="<?= base_url() ?>assets/cssdepan/offcanvas.css" rel="stylesheet">
</head>

<body class="bg-light">
    <nav class="navbar navbar-expand-md fixed-top navbar-dark bg-dark">
        <a class="navbar-brand" href="#">Garum Koi Show</a>
        <button class="navbar-toggler p-0 border-0" type="button" data-toggle="offcanvas">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="navbar-collapse offcanvas-collapse" id="navbarsExampleDefault">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="#">Setting <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="#">Logout <span class="sr-only">(current)</span></a>
                </li>
            </ul>
        </div>
    </nav>
    <main role="main" class="container">
        <div class="my-3 p-3 bg-white rounded box-shadow">
            <h6 class="border-bottom border-gray pb-2 mb-0">List Owner
            </h6>
            <div class="pull-right">
                <div class="btn-group text-r" role="group" aria-label="Basic example">
                    <a href="index.html" class="btn btn-sm btn-success"><i class="fa fa-home"></i> Home</a>
                    <a href="#" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> Tambah</a>

                </div>
            </div>
            <div class="table-responsive-sm">
                <table class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th width="3%">No</th>
                            <th>Nama</th>
                            <th>Kota</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1</td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </main>
    <script src="<?= base_url() ?>assets/cssdepan/jquery/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script>
        window.jQuery || document.write('<script src="<?= base_url() ?>assets/cssdepan/assets/js/vendor/jquery-slim.min.js"><\/script>')
    </script>
    <script src="<?= base_url() ?>assets/cssdepan/assets/js/vendor/popper.min.js"></script>
    <script src="<?= base_url() ?>assets/cssdepan/dist/js/bootstrap.min.js"></script>
    <script src="<?= base_url() ?>assets/cssdepan/assets/js/vendor/holder.min.js"></script>
    <script src="<?= base_url() ?>assets/cssdepan/offcanvas.js"></script>
</body>

</html>