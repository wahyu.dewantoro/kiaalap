@extends('layouts.master')
@section('judul')
<h3>Master Poin
</h3>
@endsection
@section('content')
<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="sparkline9-list responsive-mg-b-30">
            <div class="sparkline9-graph">
                <div class="basic-login-form-ad">

                    <div class="basic-login-inner">
                        <form action="{{ $action }}" method="post">
                            <?= csrfToken() ?>
                            <div class="form-group">
                                <label for="int">Ukuran </label>
                                <div class="row">
                                    <div class="col-md-2">
                                        <input type="number" class="form-control" name="ukuranmin" id="ukuranmin" placeholder="Min" value="<?php echo $ukuranmin; ?>" />
                                        <?php echo form_error('ukuranmin') ?>
                                    </div>
                                    <div class="col-md-2">
                                        <input type="number" class="form-control" name="ukuranmax" id="ukuranmax" placeholder="Max" value="<?php echo $ukuranmax; ?>" />
                                        <?php echo form_error('ukuranmax') ?>
                                    </div>
                                </div>

                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-2">
                                        <label for="int">Juara <?php echo form_error('juara_id') ?></label>
                                        <select class="form-control select2" name="juara_id" id="juara_id">
                                            <option value="">Pilih</option>
                                            @foreach($juara as $rj)
                                            <option @if($rj->id==$juara_id) selected @endif value="{{ $rj->id }}">{{ $rj->namajuara }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-2">
                                        <label for="int">Poin <?php echo form_error('poin') ?></label>
                                        <input type="number" class="form-control" name="poin" id="poin" placeholder="Poin" value="<?php echo $poin; ?>" />
                                    </div>
                                </div>
                            </div>
                           
                            <input type="hidden" name="id" value="<?php echo $id; ?>" />
                            <div class="login-btn-inner">
                                <div class="login-horizental">
                                    <button class="btn btn-sm btn-primary login-submit-cs" type="submit"><i class="fa fa-save"></i> Simpan</button>
                                    {!! anchor('poin','<i class="fa fa-close"></i> Batal','class="btn btn-custon-four btn-warning"') !!}
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection