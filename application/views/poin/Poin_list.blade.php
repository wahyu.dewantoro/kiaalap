@extends('layouts.master')
@section('judul')
<h3>Master Poin <div class="pull-right"> {!! anchor('poin/create','<i class="fa fa-plus"></i> Tambah','class="btn btn-custon-four btn-primary"') !!} </div>
</h3>
@endsection
@section('content')

<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="sparkline13-list">
            <div class="sparkline13-graph">
                <div class="datatable-dashv1-list custom-datatable-overright">
                    <form action="<?php echo site_url('poin/index'); ?>" class="form-inline" method="get">
                        <div class="input-group">
                            <input type="text" class="form-control" name="q" value="<?php echo $q; ?>">
                            <span class="input-group-btn">
                                <?php
                                if ($q <> '') {
                                    ?>
                                    <a href="<?php echo site_url('poin'); ?>" class="btn btn-default">Reset</a>
                                <?php
                                }
                                ?>
                                <button class="btn btn-xs btn-primary" type="submit">Search</button>
                            </span>
                        </div>
                    </form>
                    <table class="table table-striped" style="margin-bottom: 10px">
                        <tr>
                            <th>No</th>
                            <th>Ukuran</th>
                            <th>Juara</th>
                            <th>Poin</th>
                            <th></th>
                        </tr>@foreach ($poin_data as $poin)
                        <tr>
                            <td width="80px">{{  ++$start }}</td>
                            <td>{{  $poin->ukuranmin }} - {{  $poin->ukuranmax }} cm</td>
                            <td>{{  $poin->namajuara }}</td>
                            <td>{{  $poin->poin }}</td>
                            <td style="text-align:center" width="100px">
                                
                                {!! anchor(site_url('poin/update/' . $poin->id), '<i class="fa fa-edit"></i>', 'class="btn btn-xs btn-success"') !!}
                                {!! anchor(site_url('poin/delete/' . $poin->id), '<i class="fa fa-trash"></i>', 'class="btn btn-xs btn-danger" onclick="javasciprt: return confirm(\'Apakah anda yakin ?\')"') !!}
                                
                            </td>
                            @endforeach
                    </table>
                    <div class="row">
                        <div class="col-md-6">
                            <a href="#" class="btn btn-primary">Total Record : {!!  $total_rows !!}</a>
                        </div>
                        <div class="col-md-6 text-right">
                            {!!  $pagination !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection