@extends('layouts.master')
@section('judul')
<h3>Championref <div class="pull-right"> {!! anchor('refchamp/create','<i class="fa fa-plus"></i> Tambah','class="btn btn-custon-four btn-primary"') !!} </div>
</h3>
@endsection
@section('content')

<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="sparkline13-list">
            <div class="sparkline13-graph">
                <div class="datatable-dashv1-list custom-datatable-overright">
                    <form action="<?php echo site_url('refchamp/index'); ?>" class="form-inline" method="get">
                        <div class="input-group">
                            <input type="text" class="form-control" name="q" value="<?php echo $q; ?>">
                            <span class="input-group-btn">
                                <?php
                                if ($q <> '') {
                                    ?>
                                    <a href="<?php echo site_url('refchamp'); ?>" class="btn btn-default">Reset</a>
                                <?php
                                }
                                ?>
                                <button class="btn btn-xs btn-primary" type="submit">Search</button>
                            </span>
                        </div>
                    </form>
                    <table class="table table-striped" style="margin-bottom: 10px">
                        <tr>
                            <th>No</th>
                            <th>Ukuran</th>
                            <th>Champion</th>
                            <th>Urut</th>
                            <th></th>
                        </tr><?php
                                foreach ($refchamp_data as $refchamp) {
                                    ?>
                            <tr>
                                <td width="80px"><?php echo ++$start ?></td>
                                <td>{{ $refchamp->vmin }} - {{ $refchamp->vmax }} cm</td>
                                <td>{{ $refchamp->alias }}</td>
                                <td>{{ $refchamp->urut }}</td>
                                <td style="text-align:center" width="100px">
                                    <?php
                                        echo anchor(site_url('refchamp/update/' . $refchamp->id), '<i class="fa fa-edit"></i>', 'class="btn btn-xs btn-success"');
                                        echo anchor(site_url('refchamp/delete/' . $refchamp->id), '<i class="fa fa-trash"></i>', 'class="btn btn-xs btn-danger" onclick="javasciprt: return confirm(\'Apakah anda yakin ?\')"');
                                        ?>
                                </td>
                            </tr>
                        <?php
                        }
                        ?>
                    </table>
                    <div class="row">
                        <div class="col-md-6">
                            <a href="#" class="btn btn-primary">Total Record : <?php echo $total_rows ?></a>
                        </div>
                        <div class="col-md-6 text-right">
                            <?php echo $pagination ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection