@extends('layouts.master')
@section('judul')
<h3>Ukuran
</h3>
@endsection
@section('content')
<div class="row">
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
        <div class="sparkline9-list responsive-mg-b-30">
            <div class="sparkline9-graph">
                <div class="basic-login-form-ad">

                    <div class="basic-login-inner">
                        <form action="{{ $action }}" method="post">
                            <?= csrfToken() ?>
                            <div class="form-group">
                                <label for="int">Range Ukuran</label>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="input-group">
                                            <input type="text" class="form-control" name="ukuranMin" id="ukuranMin" placeholder="Min" value="<?php echo $ukuranMin; ?>" />
                                            <span class="input-group-addon">cm</span>
                                        </div>
                                        <?php echo form_error('ukuranMin') ?>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="input-group">
                                            <input type="text" class="form-control" name="ukuranMax" id="ukuranMax" placeholder="Max" value="<?php echo $ukuranMax; ?>" />
                                            <span class="input-group-addon">cm</span>
                                        </div>
                                        <?php echo form_error('ukuranMax') ?>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="varchar">Nama Ukuran <?php echo form_error('namaukuran') ?></label>
                                <input type="text" class="form-control" name="namaukuran" id="namaukuran" placeholder="Namaukuran" value="<?php echo $namaukuran; ?>" />
                            </div>
                            <div class="form-group">
                                <label for="int">Biaya Pendaftaran<?php echo form_error('biaya') ?></label>
                                <input type="text" class="form-control" name="biaya" id="biaya" placeholder="Biaya" value="<?php echo $biaya; ?>" />
                            </div>
                            <input type="hidden" name="id" value="<?php echo $id; ?>" />
                            <div class="login-btn-inner">
                                <div class="login-horizental">
                                    <button class="btn btn-sm btn-primary login-submit-cs" type="submit"><i class="fa fa-save"></i> Simpan</button>
                                    {!! anchor('size','<i class="fa fa-close"></i> Batal','class="btn btn-custon-four btn-warning"') !!}
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection