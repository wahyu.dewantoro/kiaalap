@extends('layouts.master')
@section('judul')
<h3>Groups <div class="pull-right"> {!! anchor('group/tambah','<i class="fa fa-plus"></i> Tambah','class="btn btn-custon-four btn-primary"') !!} </div>
</h3>
@endsection
@section('content')

<div class="row">
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
        <div class="sparkline13-list">

            <div class="sparkline13-graph">
                <div class="datatable-dashv1-list custom-datatable-overright">
                    <table class="table table-striped table-border">
                        <thead>
                            <tr>
                                <th width='10px'>No</th>
                                <th>Group</th>
                                <th>Deskripsi</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($data as $data)
                            <tr>
                                <td>{{ ++$start }}</td>
                                <td>{{ $data->name }}</td>
                                <td>{{ $data->description }}</td>
                                <td align="center">
                                    {!! anchor('group/setting/'.$data->id,'<i class="fa fa-lock"></i>','class="btn btn-xs btn-primary"') !!}
                                    {!! anchor('group/edit/'.$data->id,'<i class="fa fa-edit"></i>','class="btn btn-xs btn-success"') !!}
                                    {!! anchor('group/hapus/'.$data->id,'<i class="fa fa-trash"></i>','class="btn btn-xs btn-danger" onclick="return confirm(\'Apakah anda yakin?\')"') !!}
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                            <button class="btn  btn-space btn-secondary" disabled>Total Record : {{ $total_rows }}</button>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                            <div class="pull-right"><?= $pagination ?></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection