<?php $__env->startSection('judul'); ?>
<h3>First Entry <div class="pull-right"> <?php echo anchor('regisikan/create','<i class="fa fa-plus"></i> Tambah','class="btn btn-custon-four btn-primary"'); ?> </div>
</h3>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>

<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="sparkline13-list">
            <div class="sparkline13-graph">
                <div class="datatable-dashv1-list ">
                    <div class="table-responsive">
                        <table data-toolbar="#toolbarb" data-checkbox-header="false" data-id-field="id" data-select-item-name="id" id="tableabc" data-toggle="table" data-pagination="true" data-search="true" data-resizable="true">
                            <thead>
                                <tr>
                                    <th class="text-center">No</th>
                                    <th class="text-center" data-field="id">Kode</th>
                                    <th class="text-center">Handling</th>
                                    <th class="text-center">owner</th>
                                    <th class="text-center">Ikan</th>
                                    <th class="text-center">Entry By</th>
                                    <th class="text-center"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                foreach ($regisikan_data as $regisikan) {
                                    ?>
                                    <tr>
                                        <td class="text-center" width="10px"><?php echo e(++$start); ?></td>
                                        <td><?php echo anchor('regisikan/detail/'.$regisikan->id,$regisikan->uniq_code); ?></td>
                                        <td>
                                            <a href="<?php echo e(base_url().'regisikan/create?namahandling='.urlencode($regisikan->namahandling).'&kotahandling='.urlencode($regisikan->kotahandling).'&telphandling='.urlencode($regisikan->telphandling)); ?>"><?php echo e($regisikan->namahandling); ?><br>
                                                <small class="text-muted"><?php echo e($regisikan->kotahandling); ?> / <?php echo e($regisikan->telphandling); ?></small></a>
                                        </td>
                                        <td><?php echo e($regisikan->namaowner); ?><br>
                                            <small class="text-muted"><?php echo e($regisikan->kotaowner); ?></small>
                                        </td>
                                        <td><?php echo e($regisikan->ikan); ?> <br><small class="text-muted">Ekor</small></td>
                                        </td>
                                        <td><?php echo e($regisikan->entri); ?><br>

                                            <small class="text-muted"><?php echo $regisikan->created_at; ?></small>
                                        </td>
                                        <td class="text-center" width="100px">
                                            <?php
                                                echo anchor(site_url('regisikan/checkikan/' . $regisikan->id), '<i class="fa fa-sign-out"></i> Checkout', 'class="btn btn-xs btn-warning" onclick="javasciprt: return confirm(\'Apakah anda yakin ?\')"');
                                                echo anchor(site_url('regisikan/update/' . $regisikan->id), '<i class="fa fa-edit"></i>', 'class="btn btn-xs btn-success"');
                                                echo anchor(site_url('regisikan/delete/' . $regisikan->id), '<i class="fa fa-trash"></i>', 'class="btn btn-xs btn-danger" onclick="javasciprt: return confirm(\'Apakah anda yakin ?\')"');
                                                ?>
                                        </td>
                                    </tr>
                                <?php
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('css'); ?>
<link rel="stylesheet" href="<?php echo e(base_url('assets/')); ?>css/data-table/bootstrap-table.css">
<link rel="stylesheet" href="<?php echo e(base_url('assets/')); ?>css/data-table/bootstrap-editable.css">
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
<!-- data table JS
		============================================ -->
<script src="<?php echo e(base_url('assets/')); ?>js/data-table/bootstrap-table.js"></script>
<script src="<?php echo e(base_url('assets/')); ?>js/data-table/tableExport.js"></script>
<script src="<?php echo e(base_url('assets/')); ?>js/data-table/data-table-active.js"></script>
<script src="<?php echo e(base_url('assets/')); ?>js/data-table/bootstrap-table-editable.js"></script>
<script src="<?php echo e(base_url('assets/')); ?>js/data-table/bootstrap-editable.js"></script>
<script src="<?php echo e(base_url('assets/')); ?>js/data-table/bootstrap-table-resizable.js"></script>
<script src="<?php echo e(base_url('assets/')); ?>js/data-table/colResizable-1.5.source.js"></script>
<script src="<?php echo e(base_url('assets/')); ?>js/data-table/bootstrap-table-export.js"></script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH E:\xampp\htdocs\newkoishow\application\views/regisikan/Regisikan_list.blade.php ENDPATH**/ ?>