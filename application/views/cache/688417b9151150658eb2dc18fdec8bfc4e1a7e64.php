<?php $__env->startSection('judul'); ?>
<h3>Juara
</h3>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="row">
    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
        <div class="sparkline9-list responsive-mg-b-30">
            <div class="sparkline9-graph">
                <div class="basic-login-form-ad">

                    <div class="basic-login-inner">
                        <form action="<?php echo e($action); ?>" method="post">
                            <?= csrfToken() ?>

                            <div class="form-group">
                                <label for="varchar">Juara <?php echo form_error('namajuara') ?></label>
                                <input type="text" class="form-control" name="namajuara" id="namajuara" placeholder="Nama juara" value="<?php echo $namajuara; ?>" />
                            </div>
                            <div class="form-group">
                                <label for="varchar">Alias <?php echo form_error('aliasjuara') ?></label>
                                <input type="text" class="form-control" name="aliasjuara" id="aliasjuara" placeholder="Alias juara" value="<?php echo $aliasjuara; ?>" />
                            </div>
                            <div class="form-group">
                                <label for="int">Juara Up </label>
                                <!-- <input type="text"  placeholder="Id Up" value="<?php echo $id_up; ?>" /> -->
                                <select class="form-control" name="id_up" id="id_up">
                                    <option value="">null</option>
                                    <?php $__currentLoopData = $lup; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $lup): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option value="<?php echo e($lup->id); ?>"><?php echo e($lup->namajuara); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                            </div>
                            <input type="hidden" name="id" value="<?php echo $id; ?>" />
                            <div class="login-btn-inner">
                                <div class="login-horizental">
                                    <button class="btn btn-sm btn-primary login-submit-cs" type="submit"><i class="fa fa-save"></i> Simpan</button>
                                    <?php echo anchor('juara','<i class="fa fa-close"></i> Batal','class="btn btn-custon-four btn-warning"'); ?>

                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH E:\xampp\htdocs\newkoishow\application\views/juara/Juara_form.blade.php ENDPATH**/ ?>