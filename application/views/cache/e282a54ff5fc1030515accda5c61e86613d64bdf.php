<?php $__env->startSection('judul'); ?>
<h3>Dashboard <div class="pull-right"><button type="button" class="btn btn-custon-four btn-default">Default</button></div>
</h3>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="row">
    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
        <div class="analytics-sparkle-line reso-mg-b-30">
            <div class="analytics-content">
                <h5>Computer Technologies</h5>
                <h2>$<span class="counter">5000</span> <span class="tuition-fees">Tuition Fees</span></h2>
                <span class="text-success">20%</span>
                <div class="progress m-b-0">
                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:20%;"> <span class="sr-only">20% Complete</span> </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
        <div class="analytics-sparkle-line reso-mg-b-30">
            <div class="analytics-content">
                <h5>Accounting Technologies</h5>
                <h2>$<span class="counter">3000</span> <span class="tuition-fees">Tuition Fees</span></h2>
                <span class="text-danger">30%</span>
                <div class="progress m-b-0">
                    <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:30%;"> <span class="sr-only">230% Complete</span> </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\kiaalap\application\views/index.blade.php ENDPATH**/ ?>