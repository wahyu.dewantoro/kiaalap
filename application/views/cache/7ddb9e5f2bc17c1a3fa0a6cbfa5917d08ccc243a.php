<?php $__env->startSection('judul'); ?>
<h3>Groups <div class="pull-right"> <?php echo anchor('pengguna/tambah','<i class="fa fa-plus"></i> Tambah','class="btn btn-custon-four btn-primary"'); ?> </div>
</h3>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>

<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="sparkline13-list">

            <div class="sparkline13-graph">
                <div class="datatable-dashv1-list custom-datatable-overright">
                    <table class="table table-striped table-border">
                        <thead>
                            <tr>
                                <th width='10px'>No</th>
                                <th>Nama</th>
                                <th>Email</th>
                                <th>Company</th>
                                <th>Phone</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr>
                                <td><?php echo e(++$start); ?></td>
                                <td><?php echo e($data->first_name); ?> <?php echo e($data->last_name); ?></td>
                                <td><?php echo e($data->email); ?></td>
                                <td><?php echo e($data->company); ?></td>
                                <td><?php echo e($data->phone); ?></td>

                                <td align="center">
                                    <?= anchor('pengguna/edit/' . $data->id, '<i class="fa fa-edit"></i>', 'class="btn btn-xs btn-success"') ?>
                                    <?= anchor('pengguna/hapus/' . $data->id, '<i class="fa fa-trash"></i>', 'class="btn btn-xs btn-danger" onclick="return confirm(\'Apakah anda yakin?\')"') ?>
                                </td>
                            </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tbody>
                    </table>
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                            <button class="btn  btn-space btn-secondary" disabled>Total Record : <?php echo e($total_rows); ?></button>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                            <div class="pull-right"><?= $pagination ?></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH E:\xampp\htdocs\newkoishow\application\views/pengguna/index.blade.php ENDPATH**/ ?>