<?php $__env->startSection('judul'); ?>
<h3>Peserta <div class="pull-right"> </div>
</h3>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>

<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="sparkline13-list">
            <div class="sparkline13-graph">
                <div class="datatable-dashv1-list custom-datatable-overright">
                    <form action="<?php echo site_url('peserta/index'); ?>" class="form-inline" method="get">
                        <div class="input-group">
                            <input type="text" class="form-control" name="q" value="<?php echo $q; ?>">
                            <span class="input-group-btn">
                                <?php
                                if ($q <> '') {
                                    ?>
                                    <a href="<?php echo site_url('peserta'); ?>" class="btn btn-default">Reset</a>
                                <?php
                                }
                                ?>
                                <button class="btn btn-xs btn-primary" type="submit">Search</button>
                            </span>
                        </div>
                    </form>
                    <table class="table table-striped" style="margin-bottom: 10px">
                        <tr>
                            <th>No</th>
                            <th>Kode</th>
                            <th>Handling</th>
                            <th>Owner</th>
                            <th>Ikan</th>
                            <th>Created By</th>
                            <th>Updated By</th>
                            <th></th>
                        </tr>
                        <?php $__currentLoopData = $peserta_data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $peserta): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                            <td width="80px"><?php echo e(++$start); ?></td>
                            <td><a href="<?php echo e(base_url('peserta/read/'.$peserta->uniq_code)); ?>"><?php echo e($peserta->uniq_code); ?></a>
                                <?php if($peserta->no_kwitansi<>''): ?> <br> <small class="text-muted">No Kwitansi : <?php echo e($peserta->no_kwitansi); ?> </small> <?php else: ?> <br> <small class="text-muted" style="color:red;">Belum bayar ! </small> <?php endif; ?>
                            </td>
                            <td>
                                <?php echo e($peserta->namahandling); ?><br>
                                <small class="text-muted"><?php echo e($peserta->kotahandling); ?></small>
                            </td>
                            <td>
                                <?php echo e($peserta->namaowner); ?><br>
                                <small class="text-muted"><?php echo e($peserta->kotaowner); ?></small>
                            </td>
                            <td><?php echo e($peserta->ikan); ?> ekor</td>
                            <td><?php echo e(getNamaUser($peserta->created_by)); ?><br> <small class="text-muted"> <?php echo e($peserta->created_at); ?></small> </td>
                            <td><?php echo e(getNamaUser($peserta->updated_by)); ?><br> <small class="text-muted"> <?php echo e($peserta->updated_at); ?></small> </td>
                            <td style="text-align:center" width="100px">
                                <?php
                                echo anchor(site_url('peserta/read/' . $peserta->uniq_code), '<i class="fa fa-eye"></i>', 'class="btn btn-xs btn-primary"');
                                echo anchor(site_url('peserta/update/' . $peserta->id), '<i class="fa fa-edit"></i>', 'class="btn btn-xs btn-success"');
                                echo anchor(site_url('peserta/delete/' . $peserta->id), '<i class="fa fa-trash"></i>', 'class="btn btn-xs btn-danger" onclick="javasciprt: return confirm(\'Apakah anda yakin ?\')"');
                                ?>
                            </td>
                        </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </table>
                    <div class="row">
                        <div class="col-md-6">
                            <a href="#" class="btn btn-primary">Total Record : <?php echo e($total_rows); ?></a>
                        </div>
                        <div class="col-md-6 text-right">
                            <?php echo $pagination ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH E:\xampp\htdocs\newkoishow\application\views/peserta/Peserta_list.blade.php ENDPATH**/ ?>