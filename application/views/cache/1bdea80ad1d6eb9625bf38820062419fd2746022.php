<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Decode</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- favicon
		============================================ -->
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo e(base_url()); ?>img/favicon.ico">
    <!-- Google Fonts
		============================================ -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,700,900" rel="stylesheet">
    <!-- Bootstrap CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo e(base_url()); ?>assets/css/bootstrap.min.css">
    <!-- Bootstrap CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo e(base_url()); ?>assets/css/font-awesome.min.css">
    <!-- owl.carousel CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo e(base_url()); ?>assets/css/owl.carousel.css">
    <link rel="stylesheet" href="<?php echo e(base_url()); ?>assets/css/owl.theme.css">
    <link rel="stylesheet" href="<?php echo e(base_url()); ?>assets/css/owl.transitions.css">
    <!-- animate CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo e(base_url()); ?>assets/css/animate.css">
    <!-- normalize CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo e(base_url()); ?>assets/css/normalize.css">
    <!-- meanmenu icon CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo e(base_url()); ?>assets/css/meanmenu.min.css">
    <!-- main CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo e(base_url()); ?>assets/css/main.css">
    <!-- educate icon CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo e(base_url()); ?>assets/css/educate-custon-icon.css">
    <!-- morrisjs CSS
		============================================ -->
    <!-- <link rel="stylesheet" href="<?php echo e(base_url()); ?>assets/css/morrisjs/morris.css"> -->
    <!-- mCustomScrollbar CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo e(base_url()); ?>assets/css/scrollbar/jquery.mCustomScrollbar.min.css">
    <!-- metisMenu CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo e(base_url()); ?>assets/css/metisMenu/metisMenu.min.css">
    <link rel="stylesheet" href="<?php echo e(base_url()); ?>assets/css/metisMenu/metisMenu-vertical.css">
    <!-- calendar CSS
		============================================ -->
    <!-- <link rel="stylesheet" href="<?php echo e(base_url()); ?>assets/css/calendar/fullcalendar.min.css">
    <link rel="stylesheet" href="<?php echo e(base_url()); ?>assets/css/calendar/fullcalendar.print.min.css"> -->
    <!-- buttons CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo e(base_url()); ?>assets/css/buttons.css">
    <!-- style CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo e(base_url()); ?>assets/style.css">
    <!-- responsive CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo e(base_url()); ?>assets/css/responsive.css">
    <?php echo $__env->yieldContent('css'); ?>
</head>

<body>
    <!--[if lt IE 8]>
		<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="<?php echo e(base_url()); ?>http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
	<![endif]-->
    <!-- Start Left menu area -->
    <?php echo $__env->make('layouts.sidebar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <!-- End Left menu area -->
    <!-- Start Welcome area -->
    <div class="all-content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="logo-pro">
                        <a href="<?= base_url() ?>"><img class="main-logo" src="<?= base_url() ?>assets/img/logo/logo.png" alt="" /></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="header-advance-area">
            
            <?php echo $__env->make('layouts.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            <!-- Mobile Menu start -->
            
            <?php echo $__env->make('layouts.mobilemenu', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

            <!-- Mobile Menu end -->
            <div class="breadcome-area">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="single-page-breadcome" style="padding-bottom:10px; padding-top:10px;">
                                <?php echo $__env->yieldContent('judul'); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Button start-->
        <div class="button-edu-area mg-b-15">
            <div class="container-fluid">
                <?php echo $__env->yieldContent('content'); ?>
            </div>
        </div>
        <!-- Button End-->
        <!-- <div class="footer-copyright-area"> -->
        <!-- <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="footer-copy-right">
                            <p>Copyright © 2018. All rights reserved. Template by <a href="<?php echo e(base_url()); ?>https://colorlib.com/wp/templates/">Colorlib</a></p>
                        </div>
                    </div>
                </div>
            </div> -->
        <!-- </div> -->
    </div>

    <!-- jquery
		============================================ -->
    <script src="<?php echo e(base_url()); ?>assets/js/vendor/jquery-1.12.4.min.js"></script>
    <!-- bootstrap JS
		============================================ -->
    <script src="<?php echo e(base_url()); ?>assets/js/bootstrap.min.js"></script>
    <!-- wow JS
		============================================ -->
    <script src="<?php echo e(base_url()); ?>assets/js/wow.min.js"></script>
    <!-- price-slider JS
		============================================ -->
    <!-- <script src="<?php echo e(base_url()); ?>assets/js/jquery-price-slider.js"></script> -->
    <!-- meanmenu JS
		============================================ -->
    <script src="<?php echo e(base_url()); ?>assets/js/jquery.meanmenu.js"></script>
    <!-- owl.carousel JS
		============================================ -->
    <script src="<?php echo e(base_url()); ?>assets/js/owl.carousel.min.js"></script>
    <!-- sticky JS
		============================================ -->
    <script src="<?php echo e(base_url()); ?>assets/js/jquery.sticky.js"></script>
    <!-- scrollUp JS
		============================================ -->
    <script src="<?php echo e(base_url()); ?>assets/js/jquery.scrollUp.min.js"></script>
    <!-- mCustomScrollbar JS
		============================================ -->
    <script src="<?php echo e(base_url()); ?>assets/js/scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="<?php echo e(base_url()); ?>assets/js/scrollbar/mCustomScrollbar-active.js"></script>
    <!-- metisMenu JS
		============================================ -->
    <script src="<?php echo e(base_url()); ?>assets/js/metisMenu/metisMenu.min.js"></script>
    <script src="<?php echo e(base_url()); ?>assets/js/metisMenu/metisMenu-active.js"></script>
    <!-- tab JS
		============================================ -->
    <!-- <script src="<?php echo e(base_url()); ?>assets/js/tab.js"></script> -->
    <!-- plugins JS
		============================================ -->
    <script src="<?php echo e(base_url()); ?>assets/js/plugins.js"></script>
    <!-- main JS
		============================================ -->
    <script src="<?php echo e(base_url()); ?>assets/js/main.js"></script>
    <?php echo $__env->yieldContent('script'); ?>
</body>

</html><?php /**PATH C:\xampp\htdocs\kiaalap\application\views/layouts/master.blade.php ENDPATH**/ ?>