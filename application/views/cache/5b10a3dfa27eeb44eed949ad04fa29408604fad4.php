<?php $__env->startSection('judul'); ?>
<h3>Form Pengguna
</h3>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="sparkline9-list responsive-mg-b-30">
            <div class="sparkline9-graph">
                <div class="basic-login-form-ad">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="basic-login-inner">
                                <div id="infoMessage"><?php echo $message; ?></div>
                                <?php echo form_open("pengguna/tambah"); ?>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Nama Pengguna </label>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <?php echo form_input($first_name); ?>
                                                </div>
                                                <div class="col-md-6">
                                                    <?php echo form_input($last_name); ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <?php echo lang('create_user_company_label', 'company'); ?>
                                            <?php echo form_input($company); ?>
                                        </div>
                                        <div class="form-group">
                                            <?php echo lang('create_user_phone_label', 'phone'); ?>
                                            <?php echo form_input($phone); ?>
                                        </div>

                                    </div>
                                    <div class="col-md-6">
                                        <?php
                                        if ($identity_column !== 'email') {
                                            echo '<div class="form-group">';
                                            echo lang('create_user_identity_label', 'identity');
                                            // echo '';
                                            echo form_error('identity');
                                            echo form_input($identity);
                                            echo '</div>';
                                        }
                                        ?>

                                        <div class="form-group">
                                            <?php echo lang('create_user_email_label', 'email'); ?>
                                            <?php echo form_input($email); ?>
                                        </div>
                                        <div class="form-group">
                                            <?php echo lang('create_user_password_label', 'password'); ?>
                                            <?php echo form_input($password); ?>
                                        </div>

                                        <div class="form-group">
                                            <?php echo lang('create_user_password_confirm_label', 'password_confirm'); ?>
                                            <?php echo form_input($password_confirm); ?>
                                        </div>
                                        <div class="form-group">
                                            <button class="btn btn-sm btn-primary"><i class="fa fa-save"></i> Simpan </button>
                                        </div>

                                    </div>
                                </div>
                                <?php echo form_close(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH E:\xampp\htdocs\newkoishow\application\views/pengguna/form.blade.php ENDPATH**/ ?>