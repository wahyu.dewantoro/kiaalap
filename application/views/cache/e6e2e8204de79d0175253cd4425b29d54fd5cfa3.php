<?php $__env->startSection('judul'); ?>
<h3>Print Preview <div class="pull-right"> <?php echo anchor($_SERVER['HTTP_REFERER'],'<i class="fa fa-angle-double-left"></i> kembali','class="btn btn-sm btn-success"'); ?></div>
</h3>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>

<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div id="tutorial-pdf-responsive" class="tutorial-pdf-responsive custom2">
            <div class="custom2">
                <iframe src="<?php echo e($url); ?>"></iframe>
            </div>
        </div>
    </div>
     
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('css'); ?>
<style>
    .custom1 {
        margin-left: auto !important;
        margin-right: auto !important;
        margin: 20px;
        padding: 10px;
    }

    .custom2 {
        position: relative;
        height: 0;
        overflow: hidden;
        padding-bottom: 90%;
    }

    .custom2 iframe {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        padding: 0;
        margin: 0;
    }

    .tutorial-pdf-responsive {
        max-width: 100%;
        max-height: auto;
        overflow: hidden;
    }
</style>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH E:\xampp\htdocs\newkoishow\application\views/pembayaran/printpreview.blade.php ENDPATH**/ ?>