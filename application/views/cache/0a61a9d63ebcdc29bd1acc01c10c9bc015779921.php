<?php $__env->startSection('judul'); ?>
<h3>Ukuran <div class="pull-right"> <?php echo anchor('size/create','<i class="fa fa-plus"></i> Tambah','class="btn btn-custon-four btn-primary"'); ?> </div>
</h3>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>

<div class="row">
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
        <div class="sparkline13-list">
            <div class="sparkline13-graph">
                <div class="datatable-dashv1-list custom-datatable-overright">
                    <form action="<?php echo site_url('size/index'); ?>" class="form-inline" method="get">
                        <div class="input-group">
                            <input type="text" class="form-control" name="q" value="<?php echo $q; ?>">
                            <span class="input-group-btn">
                                <?php
                                if ($q <> '') {
                                    ?>
                                <a href="<?php echo site_url('size'); ?>" class="btn btn-default">Reset</a>
                                <?php
                                }
                                ?>
                                <button class="btn btn-xs btn-primary" type="submit">Search</button>
                            </span>
                        </div>
                    </form>
                    <table class="table table-striped" style="margin-bottom: 10px">
                        <tr>
                            <th>No</th>
                            <th>Min</th>
                            <th>Max</th>
                            <th>Alias</th>
                            <th>Biaya</th>
                            <th></th>
                        </tr><?php $__currentLoopData = $size_data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $size): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                            <td width="80px"><?php echo ++$start ?></td>
                            <td><?php echo $size->ukuranMin ?></td>
                            <td><?php echo $size->ukuranMax ?></td>
                            <td><?php echo $size->namaukuran ?></td>
                            <td align="right"><?php echo rupiah($size->biaya) ?></td>
                            <td style="text-align:center" width="100px">
                                <?php
                                    echo anchor(site_url('size/update/' . $size->id), '<i class="fa fa-edit"></i>', 'class="btn btn-xs btn-success"');
                                    echo anchor(site_url('size/delete/' . $size->id), '<i class="fa fa-trash"></i>', 'class="btn btn-xs btn-danger" onclick="javasciprt: return confirm(\'Apakah anda yakin ?\')"');
                                    ?>
                            </td>
                        </tr>
                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </table>
                    <div class="row">
                        <div class="col-md-6">
                            <a href="#" class="btn btn-primary">Total Record : <?php echo $total_rows ?></a>
                        </div>
                        <div class="col-md-6 text-right">
                            <?php echo $pagination ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH E:\xampp\htdocs\newkoishow\application\views/size/Size_list.blade.php ENDPATH**/ ?>