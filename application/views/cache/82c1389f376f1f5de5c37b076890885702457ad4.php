<?php $__env->startSection('judul'); ?>
<h3>Detail Cart <div class="pull-right">

        <?php echo anchor('regisikan/update/'.$header->id,'<i class="fa fa-edit"></i>  edit','class="btn btn-sm btn-success"'); ?>

        <?php echo anchor('regisikan','<i class="fa fa-angle-double-left"></i> kembali','class="btn btn-sm btn-primary"'); ?> </div>
</h3>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="row">
    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
        <div class="profile-info-inner">
            <div class="profile-details-hr">
                <div class="row">
                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-6">
                        <div class="address-hr">
                            <p><b>Handling</b><br /> <?php echo e($header->namahandling); ?></p>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-6">
                        <div class="address-hr tb-sm-res-d-n dps-tb-ntn">
                            <p><b>Kota</b><br /> <?php echo e($header->kotahandling); ?></p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-6">
                        <div class="address-hr">
                            <p><b>Owner</b><br /> <?php echo e($header->namaowner); ?></p>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-6">
                        <div class="address-hr tb-sm-res-d-n dps-tb-ntn">
                            <p><b>Kota</b><br /> <?php echo e($header->kotaowner); ?></p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="address-hr">
                            <p><b>Kode</b><br /> <u><?php echo e($header->uniq_code); ?></u></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
        <div class="row">
            <?php $__currentLoopData = $detail; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $detail): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                <div class="student-inner-std res-mg-b-30">
                    <div class="student-img">
                        <img class="img-responsive" src="<?php echo e(base_url($detail->gambar_ikan)); ?>" alt="" />
                    </div>
                    <div class="student-dtl">
                        <h5><?php echo e($detail->namavariety); ?></h5>
                        <b>Size</b>: <?php echo e($detail->ukuran); ?> cm</p>
                        <p class="dp"><?php echo e($gender[$detail->gender]); ?>, <?php echo e($breeder[$detail->breeder]); ?></p>
                    </div>
                </div>
            </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('css'); ?>
<style>
    .img-responsive {
        width: auto;
        height: 230px;
    }
</style>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH E:\xampp\htdocs\newkoishow\application\views/regisikan/view_detail.blade.php ENDPATH**/ ?>