<div class="row">
    <div class="col-md-4">
        <img class="img-responsive" src="<?php echo e(base_url($gambar_ikan)); ?>" alt="" />
    </div>
    <div class="col-md-8">
        <table class="table">
            <tr>
                <td width="20%">No Ikan</td>
                <td> <?php echo e($no_ikan); ?> 
                    <?php echo anchor('penjurian/editikan/'.$no_ikan,'<i class="fa fa-edit"></i> Edit','class="btn btn-xs btn-success"'); ?>

                </td>
            </tr>
            <tr>
                <td width="20%">Variety</td>
                <td> <?php echo e($namavariety); ?> <br>
                <small class="text-muted"> <?php echo e($gd[$gender].', '.$br[$breeder]); ?> </small>
            </td>
            </tr>
            <tr>
                <td width="20%">Ukuran</td>
                <td> <?php echo e($ukuran); ?> cm</td>
            </tr>
            <tr>
                <td width="20%">Handling</td>
                <td> <?php echo e($namahandling); ?><br> <small class="text-muted"> <?php echo e($kotahandling); ?></small> </td>
            </tr>
            <tr>
                <td width="20%">Owner</td>
                <td> <?php echo e($namaowner); ?><br> <small class="text-muted"> <?php echo e($kotahandling); ?></small> </td>
            </tr>
        </table>
    </div>
</div>
<style>
    .img-responsive {
        width: auto;
        height: 150px;
    }
</style><?php /**PATH E:\xampp\htdocs\newkoishow\application\views/penjurian/detailikan.blade.php ENDPATH**/ ?>