<?php $__env->startSection('judul'); ?>
<h3>Penjurian Bis & Champion<div class="pull-right"> </div>
</h3>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="profile-info-inner">
            <div class="profile-details-hr">
                <input type="hidden" id="vc" name="vc" value="<?php echo e($vc); ?>">
                <input type="hidden" id="vk" name="vk" value="<?php echo e($vk); ?>">

                <div class="row">
                    <div class="col-md-10 col-md-offset-2">
                        <?php $__currentLoopData = $kelas; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $kelas): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <button data-kelas="<?php echo e($kelas->kelas); ?>" <?php if($vk==$kelas->kelas): ?> class="kelas btn btn-primary" <?php else: ?> class="kelas btn btn-primary" <?php endif; ?> >Kelas <br> <?php echo e($kelas->kelas); ?></button>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <?php $__currentLoopData = $champ; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $champ): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <br>
                        <button data-champ='<?php echo e($champ->id); ?>' <?php if($vc==$champ->id): ?> class=" champ btn btn-warning" <?php else: ?> class="champ btn btn-warning" <?php endif; ?> ><?php $dd=explode(' ',$champ->alias); ?> <?php echo e($dd[0]); ?> <br><?php echo e($dd[1]); ?> </button>
                        <br>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                    <div class="col-md-10">
                        <br>
                        <div id="hasildata"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('css'); ?>
<style>
    .img-responsive {
        width: auto;
        height: 150px;
    }
</style>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
<script>
    getData($('#vk').val(), $('#vc').val());

    $(document).on('click', '.kelas,.champ', function(e) {
        e.preventDefault();
        var _kelas = $(this).attr('data-kelas');
        var _champ = $(this).attr('data-champ');

        if (_kelas == null) {
            _kelas = $('#vk').val();
        }

        if (_champ == null) {
            _champ = $('#vc').val();
        }
        $('#vc').val(_champ);
        $('#vk').val(_kelas);
        getData(_kelas, _champ);
    });

    function getData(_kelas, _champ) {
        var _url = '<?= base_url() ?>' + 'penjurian/getdataformbis';
        $.get(_url, {
            'kelas': _kelas,
            'champ': _champ
        }, function(res) {
            $('#hasildata').html(res);
        });
    }
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH E:\xampp\htdocs\newkoishow\application\views/penjurian/bischamp.blade.php ENDPATH**/ ?>