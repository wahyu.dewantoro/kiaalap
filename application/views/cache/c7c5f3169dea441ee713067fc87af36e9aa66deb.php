<?php $__env->startSection('judul'); ?>
<h3>Penjurian Rank 1-5 <div class="pull-right"> </div>
</h3>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="row">
    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
        <div class="profile-info-inner">
            <div class="profile-details-hr">
                <form>
                    <div class="form-group">
                        <select id="noikan" class="form-control select2_demo_3" data-placeholder="Cari ikan">
                            <option value=""></option>
                            <?php $__currentLoopData = $noikan; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ni): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option value="<?php echo e($ni->no_ikan); ?>"><?php echo e($ni->no_ikan.' - '.$ni->namavariety); ?> </option>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </select>
                    </div>
                </form>
                <hr>
                <form method="get" action="<?php echo e(base_url('penjurian/kategori')); ?>">
                    <div class="form-group">
                        <select onchange="this.form.submit()" name="ukuran" id="ukuran" class="select2_demo_3 form-control" required>
                            <?php $__currentLoopData = $ukuran; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ru): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option <?php if($vu==$ru->ukuranMin.'_'.$ru->ukuranMax): ?> selected <?php endif; ?> value="<?php echo e($ru->ukuranMin.'_'.$ru->ukuranMax); ?>"><?php echo e($ru->namaukuran); ?></option>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </select>
                    </div>
                    <div class="form-group">

                        <select onchange="this.form.submit()" name="variety" id="variety" class="form-control select2_demo_3" data-placeholder="Pilih variety" required>
                            <option value=""></option>
                            <?php $__currentLoopData = $variety; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $rv): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option <?php if($vv==$rv->id): ?> selected <?php endif; ?> value="<?php echo e($rv->id); ?>"><?php echo e($rv->namavariety); ?></option>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </select>
                    </div>
                </form>
                <hr>
                <div id="listjuara"></div>
            </div>
        </div>
    </div>
    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
        <div class="row">
            <?php $__currentLoopData = $ikan; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $detail): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-6">
                <div data-id="<?php echo e($detail->id); ?>" class="student-inner-std res-mg-b-30 ikan">
                    <div class="student-img">
                        <img class="img-responsive" src="<?php echo e(base_url($detail->gambar_ikan)); ?>" alt="" />
                    </div>
                    <div class="student-dtl">
                        <p class="dp-ag"><b>ID</b>: <?php echo e($detail->id); ?><br> <b>Size</b>: <?php echo e($detail->ukuran); ?> cm<br>
                            <!-- {!-- $detail->namahandling --!} (<?php echo e($detail->namaowner); ?>) -->
                        </p>
                    </div>
                </div>
            </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">Data Ikan</h4>
            </div>
            <div class="modal-body">
            </div>
             
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('css'); ?>
<!-- select2 CSS
		============================================ -->
<link rel="stylesheet" href="<?php echo e(base_url().'assets/'); ?>css/select2/select2.min.css">
<style>
    .img-responsive {
        width: auto;
        height: 150px;
    }
</style>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
<!-- select2 JS
		============================================ -->
<script src="<?php echo e(base_url().'assets/'); ?>js/select2/select2.full.min.js"></script>
<script src="<?php echo e(base_url().'assets/'); ?>js/select2/select2-active.js"></script>
<script>
    $(document).on('change', '#noikan', function(e) {
        // alert();
        e.preventDefault();
        $(".modal-body").html('');
        $("#myModal").modal('show');
        $.ajax({
            url: "<?php echo base_url('penjurian/cariikan'); ?>",
            type: 'get',
            data: {id:$('#noikan').val()},
            success: function(data) {
                $(".modal-body").html(data);
            }
        });
    });


    $(function() {
        loadjuara();

        $(document).on('click', '.ikan', function(e) {
            e.preventDefault();
            var ikan = $(this).attr('data-id');
            $.get('<?php echo base_url() . "penjurian/choserank" ?>', {
                    id: $(this).attr('data-id'),
                    ukuran: $('#ukuran').val(),
                    variety: $('#variety').val()
                },
                function(html) {
                    // $('#listjuara').html(html);
                    loadjuara();
                }
            );
        });

        function loadjuara() {
            $.get('<?php echo base_url() . "penjurian/listrank" ?>', {
                    ukuran: $('#ukuran').val(),
                    variety: $('#variety').val()
                },
                function(html) {
                    $('#listjuara').html(html);
                }
            );
        }
    });
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH E:\xampp\htdocs\newkoishow\application\views/penjurian/kategori.blade.php ENDPATH**/ ?>