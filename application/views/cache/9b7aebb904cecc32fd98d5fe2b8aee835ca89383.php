<?php $__env->startSection('judul'); ?>
<h3>Peserta <div class="pull-right"> </div>
</h3>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>

<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="product-payment-inner-st res-mg-t-30 analysis-progrebar-ctn">
            <ul id="myTabedu1" class="tab-review-design">
                <li <?php if($tab=='satu' ): ?> class="active" <?php endif; ?>><a href="#description">Tagihan</a></li>
                <li <?php if($tab=='dua' ): ?> class="active" <?php endif; ?>><a href="#reviews">Pembayaran</a></li>
            </ul>
            <div id="myTabContent" class="tab-content custom-product-edit st-prf-pro">
                <div class="product-tab-list tab-pane fade <?php if($tab=='satu'): ?> active in <?php endif; ?>" id="description">
                    <div class="row">
                        <div class="col-md-12">
                            <div id="toolbar">
                                <button class="btn btn-sm btn-success" id="generate_kwitansi"><i class="fa fa-print"></i> kwitansi</button>
                            </div>
                            <table data-toolbar="#toolbar" data-checkbox-header="false" data-id-field="kode" data-select-item-name="kode" id="tableabc" data-toggle="table" data-pagination="true" data-search="true" data-resizable="true">
                                <thead>
                                    <tr>
                                        <th data-checkbox="true"></th>
                                        <th data-field="kode">Kode</th>
                                        <th>Handling</th>
                                        <th>Owner</th>
                                        <th>Ikan</th>
                                        <th>Tagihan</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $noa=1; ?>
                                    <?php $__currentLoopData = $tagihan; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $rn): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr>
                                        <td></td>
                                        <td><?php echo $rn->uniq_code; ?></td>
                                        <td><?php echo e($rn->namahandling); ?><br> <small class='text-muted'><?php echo e($rn->kotahandling); ?></small></td>
                                        <td><?php echo e($rn->namaowner); ?><br> <small class='text-muted'><?php echo e($rn->kotaowner); ?></small></td>
                                        <td class="text-center"><?php echo e($rn->ikan); ?> ekor</td>
                                        <td class="text-right"><?php echo e(rupiah($rn->tagihan)); ?></td>
                                        <td class="text-center"><?php echo anchor('pembayaran/detailTagihan?q='.urlencode($rn->uniq_code),'<i class="fa fa-binoculars"></i> Detail','class="btn btn-xs btn-primary"'); ?></td>
                                    </tr>
                                    <?php $noa++; ?>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="product-tab-list tab-pane fade <?php if($tab=='dua'): ?> active in <?php endif; ?>" id="reviews">
                    <div class="row">
                        <div class="col-md-12">
                            <div id="toolbarb">
                                <button class="btn btn-sm btn-success" id="pelunasan_masal"><i class="fa fa-check"></i> Pelunasan</button>
                            </div>
                            <table data-toolbar="#toolbarb" data-checkbox-header="false" data-id-field="id" data-select-item-name="id" id="tableabc" data-toggle="table" data-pagination="true" data-search="true" data-resizable="true">
                                <thead>
                                    <tr>
                                        <th data-checkbox="true"></th>
                                        <th data-field="id">No Kwitansi</th>
                                        <th>Tanggal</th>
                                        <th>Jumlah</th>
                                        <th>Lunas</th>
                                        <th>Keterangan</th>
                                        <th>Created By</th>
                                        <th>Cetak</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $nob=1; ?>
                                    <?php $__currentLoopData = $pembayaran; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $rm): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr>
                                        <td></td>
                                        <td class="text-center"><?php echo e($rm->id); ?></td>
                                        <td class="text-center"><?php echo e(date_indo($rm->tgl_kwitansi)); ?></td>
                                        <td class="text-right"><?php echo e(rupiah($rm->jumlah)); ?></td>
                                        <td class="text-center"><?php if($rm->lunas=='1'): ?> Lunas <?php else: ?> Belum <?php endif; ?></td>
                                        <td><?php echo e($rm->keterangan); ?></td>
                                        <td><?php echo e(getNamaUser($rm->created_by)); ?><br>
                                            <small class="text-muted"><?php echo e($rm->created_at); ?></small>
                                        </td>
                                        <td class="text-center">
                                            <a href="<?php echo e(base_url('pembayaran/print/'.$rm->id)); ?>" class="btn btn-xs btn-primary"><i class="fa fa-print"></i></a>
                                            <a href="<?php echo e(base_url('pembayaran/printlabel/'.$rm->id)); ?>" class="btn btn-xs btn-success"><i class="fa fa-file-text"></i></a>
                                        </td>
                                    </tr>
                                    <?php $nob++; ?>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('css'); ?>
<link rel="stylesheet" href="<?php echo e(base_url('assets/')); ?>css/data-table/bootstrap-table.css">
<link rel="stylesheet" href="<?php echo e(base_url('assets/')); ?>css/data-table/bootstrap-editable.css">
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
<!-- data table JS
		============================================ -->
<script src="<?php echo e(base_url('assets/')); ?>js/data-table/bootstrap-table.js"></script>
<script src="<?php echo e(base_url('assets/')); ?>js/data-table/tableExport.js"></script>
<script src="<?php echo e(base_url('assets/')); ?>js/data-table/data-table-active.js"></script>
<script src="<?php echo e(base_url('assets/')); ?>js/data-table/bootstrap-table-editable.js"></script>
<script src="<?php echo e(base_url('assets/')); ?>js/data-table/bootstrap-editable.js"></script>
<script src="<?php echo e(base_url('assets/')); ?>js/data-table/bootstrap-table-resizable.js"></script>
<script src="<?php echo e(base_url('assets/')); ?>js/data-table/colResizable-1.5.source.js"></script>
<script src="<?php echo e(base_url('assets/')); ?>js/data-table/bootstrap-table-export.js"></script>
<script>
    // pelunasan
    // 
    $(document).on('click', '#pelunasan_masal', function(e) {
        e.preventDefault();
        var val = [];
        $(':checkbox:checked').each(function(i) {
            val[i] = $(this).val();
        });

        $.get("<?php echo e(base_url('pembayaran/pelunasan')); ?>", {
                kwitansi: val
            },
            function(data) {
                window.location.replace("<?php echo e(base_url('pembayaran?tab=dua')); ?>");
            });
    });

    $(document).on('click', '#generate_kwitansi', function(e) {
        e.preventDefault();
        var val = [];
        $(':checkbox:checked').each(function(i) {
            val[i] = $(this).val();
        });
        valu = val.join();
        $.get("<?php echo e(base_url('pembayaran/generatekwitansi')); ?>", {
                uniq: val
            },
            function(data) {
                // alert(data);
                if (data == 0) {
                    location.reload(true);
                } else {
                    window.location.replace("<?php echo e(base_url('pembayaran/print/')); ?>" + data);
                }
            });
    });
</script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH E:\xampp\htdocs\newkoishow\application\views/pembayaran/index.blade.php ENDPATH**/ ?>