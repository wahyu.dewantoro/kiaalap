<?php $__env->startSection('judul'); ?>
<h3>Fish Entry
</h3>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="sparkline9-list responsive-mg-b-30">
            <div class="sparkline9-graph">
                <div class="basic-login-form-ad">

                    <div class="basic-login-inner">
                        <form action="<?php echo e($action); ?>" method="post" enctype="multipart/form-data">
                            <?= csrfToken() ?>
                            <div class="row">
                                <?php if($is_handling==false): ?>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="varchar">Handling <?php echo form_error('namahandling') ?></label>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <input type="text" class="form-control" name="namahandling" id="namahandling" placeholder="Nama Handling" value="<?php echo $namahandling; ?>" />
                                            </div>
                                            <div class="col-md-6">
                                                <input type="text" class="form-control" name="kotahandling" id="kotahandling" placeholder="Kota Handling" value="<?php echo $kotahandling; ?>" />
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="varchar">Telp <?php echo form_error('telphandling') ?></label>
                                        <input type="text" class="form-control" name="telphandling" id="telphandling" placeholder="Telp handling" value="<?php echo $telphandling; ?>" />
                                    </div>
                                </div>
                                <?php endif; ?>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="varchar">Owner <?php echo form_error('namaowner') ?></label>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <input type="text" class="form-control" name="namaowner" id="namaowner" placeholder="Nama owner" value="<?php echo $namaowner; ?>" />
                                            </div>
                                            <div class="col-md-6">
                                                <input type="text" class="form-control" name="kotaowner" id="kotaowner" placeholder="Kota owner" value="<?php echo $kotaowner; ?>" />
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <?php if(empty($detail)): ?>
                                <div class="col-md-6">
                                    <div class="panel panel-primary">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                                        <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 120px; height: 160px;"> </div>
                                                        <small> <span class="fileinput-filename"></span></small>
                                                        <div>
                                                            <span class="btn-file"> <input type="file" required accept="image/*" name="gambar_ikan[]"> </span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <!-- <input type="text"> -->
                                                        <select name="variety_id[]" class="form-control" required>
                                                            <option value="">Pilih variety</option>
                                                            <?php $__currentLoopData = $variety; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $vv): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <option value="<?php echo e($vv->id); ?>"><?php echo e($vv->namavariety); ?></option>
                                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <input type="number" name="ukuran[]" placeholder="Ukuran" class="form-control">
                                                    </div>
                                                    <div class="form-group">
                                                        <select name="gender[]" class="form-control" required>
                                                            <option value="">Gender</option>
                                                            <?php for($g=1;$g<=2;$g++): ?> <option value="<?php echo e($g); ?>"><?php echo e($gender[$g]); ?></option>
                                                                <?php endfor; ?>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <select name="breeder[]" class="form-control" required>
                                                            <option value="">Breeder</option>
                                                            <?php for($b=1;$b<=2;$b++): ?> <option value="<?php echo e($b); ?>"><?php echo e($breeder[$b]); ?></option>
                                                                <?php endfor; ?>
                                                        </select>
                                                    </div>
                                                    <button type="button" class="add_project_file  btn-custon-four btn  btn-success "><i class="fa fa-plus"></i> Ikan</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php else: ?>
                                <div class="project_images">
                                    <?php $no=1; ?>
                                    <?php $__currentLoopData = $detail; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $detail): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <!-- <div class="col-md-6   <?php if($no<>1): ?> abc <?php endif; ?>"> -->
                                    <div class="col-md-6 abc">
                                        <div class="panel panel-primary">
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                                            <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 120px; height: 160px;">
                                                                <img src="<?php echo e(base_url().$detail->gambar_ikan); ?>" alt="Smiley face">
                                                            </div>
                                                            <small> <span class="fileinput-filename"></span></small>
                                                            <div>
                                                                <span class="btn-file"> <input type="file" accept="image/*" name="gambar_ikan[]"> </span>
                                                                <input type="hidden" name="gambar_ikan_lama[]" value="<?php echo e($detail->gambar_ikan); ?>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <select name="variety_id[]" class="form-control" required>
                                                                <option value="">Pilih variety</option>
                                                                <?php $__currentLoopData = $variety; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $vv): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                <option <?php if($vv->id==$detail->variety_id): ?> selected <?php endif; ?> value="<?php echo e($vv->id); ?>"><?php echo e($vv->namavariety); ?></option>
                                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                            </select>
                                                        </div>
                                                        <div class="form-group">
                                                            <input type="number" name="ukuran[]" value="<?php echo e($detail->ukuran); ?>" placeholder="Ukuran" class="form-control">
                                                        </div>
                                                        <div class="form-group">
                                                            <select name="gender[]" class="form-control" required>
                                                                <option value="">Gender</option>
                                                                <?php for($g=1;$g<=2;$g++): ?> <option <?php if($detail->gender==$g): ?> selected <?php endif; ?> value="<?php echo e($g); ?>"><?php echo e($gender[$g]); ?></option>
                                                                    <?php endfor; ?>
                                                            </select>
                                                        </div>
                                                        <div class="form-group">
                                                            <select name="breeder[]" class="form-control" required>
                                                                <option value="">Breeder</option>
                                                                <?php for($b=1;$b<=2;$b++): ?> <option <?php if($detail->breeder==$b): ?> selected <?php endif; ?> value="<?php echo e($b); ?>"><?php echo e($breeder[$b]); ?></option>
                                                                    <?php endfor; ?>
                                                            </select>
                                                        </div>
                                                        <?php if($no==1): ?>
                                                        <button type="button" class="add_project_file  btn-custon-four btn  btn-success "><i class="fa fa-plus"></i> Ikan</button>
                                                        <?php else: ?>
                                                        <button type="button" class="remove_project_file btn  btn-custon-four btn-danger "><i class="fa fa-trash"></i></button>
                                                        <?php endif; ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php $no++; ?>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </div>
                                <?php endif; ?>
                                <div class="project_images"></div>
                            </div>

                            <input type="hidden" name="id" value="<?php echo $id; ?>" />
                            <div class="login-btn-inner">
                                <div class="login-horizental">

                                    <button class="btn btn-primary login-submit-cs" type="submit"><i class="fa fa-save"></i> Simpan</button>
                                    <?php echo anchor($_SERVER['HTTP_REFERER'],'<i class="fa fa-close"></i> Batal','class="btn btn-custon-four btn-warning"'); ?>

                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('css'); ?>
<link href="<?= base_url() ?>assets/js/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
<script src="<?= base_url() ?>assets/js/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
<script type="text/javascript">
    var rowNum = 0;
    ccc = 1;
    $('.add_project_file').click(function(e) {
        e.preventDefault();

        rowNum++;
        // Form Ikan <span id='ke'>1</span>

        if (ccc < 10) {
            ccc += 1;
            $('#vc').val(ccc);
            // $('#count').html('<button type="submit"  class="btn btn-sm green "><i class="fa fa-save"></i> Submit (' + ccc + ')</button>');
            $(".project_images").append('<div class="col-md-6 abc">' +
                '<div class="panel panel-primary">' +
                '<div class="panel-body">' +
                '<div class="row">' +
                '<div class="col-md-6">' +
                '<div class="fileinput fileinput-new" data-provides="fileinput">' +
                '<div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 120px; height: 160px;"> </div>' +
                '<small> <span class="fileinput-filename"></span></small>' +
                '<div>' +
                '<span class="btn-file"> <input type="file" required accept="image/*" name="gambar_ikan[]"> </span>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '<div class="col-md-6">' +
                '<div class="form-group">' +
                '<select name="variety_id[]" class="form-control" required>' +
                '<option value="">Pilih variety</option>' +
                '<?php $__currentLoopData = $variety; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $vv): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>' +
                '<option value="<?php echo e($vv->id); ?>"><?php echo e($vv->namavariety); ?></option>' +
                '<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>' +
                '</select>' +
                '</div>' +
                '<div class="form-group">' +
                '<input type="number" name="ukuran[]" placeholder="Ukuran" class="form-control">' +
                '</div>' +
                '<div class="form-group">' +
                '<select name="gender[]" class="form-control" required>' +
                '<option value="">Gender</option>' +
                '<?php for($g=1;$g<=2;$g++): ?> <option value="<?php echo e($g); ?>"><?php echo e($gender[$g]); ?></option>' +
                '<?php endfor; ?>' +
                '</select>' +
                '</div>' +
                '<div class="form-group">' +
                '<select name="breeder[]" class="form-control" required>' +
                '<option value="">Breeder</option>' +
                '<?php for($b=1;$b<=2;$b++): ?> <option value="<?php echo e($b); ?>"><?php echo e($breeder[$b]); ?></option>' +
                '<?php endfor; ?>' +
                '</select>' +
                '</div>' +
                '<button type="button" class="remove_project_file btn  btn-custon-four btn-danger "><i class="fa fa-trash"></i></button>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div>');
        }
    });



    // Remove parent of 'remove' link when link is clicked.


    $(document).ready(function() {

        $('.project_images').on('click', '.remove_project_file', function(e) {
            e.preventDefault();
            // alert('asj');
            $(this).parents(".abc").remove();
            ccc -= 1;
            $('#vc').val(ccc);
            // $('#count').html('<button type="submit" class="btn btn-sm green "><i class="fa fa-save"></i> Submit (' + ccc + ')</button>');

        });
    });
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH E:\xampp\htdocs\newkoishow\application\views/regisikan/Regisikan_form.blade.php ENDPATH**/ ?>