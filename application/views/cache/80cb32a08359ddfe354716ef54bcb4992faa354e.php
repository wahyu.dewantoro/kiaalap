<?php $__env->startSection('judul'); ?>
<h3>Kontes
</h3>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>

<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="sparkline13-list">
            <div class="sparkline13-graph">
                <div class="datatable-dashv1-list custom-datatable-overright">

                    <table class="table table-striped" style="margin-bottom: 10px">
                        <tr>

                            <th>Nama Kontes</th>
                            <th>Tempat</th>
                            <th>Kota</th>
                            <th>Daftar Online</th>
                            <th>Perolehan Poin </th>
                            <th>Most Handling</th>
                            <th>Most Entri</th>
                            <th></th>
                        </tr>
                        <?php $__currentLoopData = $kontes_data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $kontes): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>

                            <td><?php echo e($kontes->namakontes); ?></td>
                            <td><?php echo e($kontes->tempat); ?></td>
                            <td><?php echo e($kontes->kota); ?></td>
                            <td><?php echo e($ld[$kontes->daftar]); ?></td>
                            <td><?php echo e($ld[$kontes->poin]); ?></td>
                            <td><?php echo e($ld[$kontes->handling]); ?></td>
                            <td><?php echo e($ld[$kontes->entri]); ?></td>
                            <td style="text-align:center" width="100px">

                            <?php echo  anchor(site_url('kontes/update/' . $kontes->id), '<i class="fa fa-edit"></i>', 'class="btn btn-xs btn-success"') ?>

                            </td>
                        </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH E:\xampp\htdocs\newkoishow\application\views/kontes/Kontes_list.blade.php ENDPATH**/ ?>