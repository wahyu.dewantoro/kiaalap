<div class="mobile-menu-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="mobile-menu">
                    <nav id="dropdown">
                        <ul class="mobile-menu-nav">
                           <?php echo showMenuMobile(); ?>

                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div><?php /**PATH E:\xampp\htdocs\newkoishow\application\views/layouts/mobilemenu.blade.php ENDPATH**/ ?>