<?php $__env->startSection('judul'); ?>
<h3>Form Menu
</h3>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="sparkline9-list responsive-mg-b-30">
            <div class="sparkline9-graph">
                <div class="basic-login-form-ad">
                    <div class="basic-login-inner">
                        <form action="<?php echo e($action); ?>" method="post">
                            <?= csrfToken() ?>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="varchar">Nama Menu <?php echo form_error('nama_menu') ?></label>
                                        <input type="text" class="form-control" name="nama_menu" id="nama_menu" placeholder="Nama Menu" value="<?php echo $nama_menu; ?>" />
                                    </div>
                                    <div class="form-group">
                                        <label for="int">Parent <?php echo form_error('parent') ?></label>
                                        <select class="form-control" name="parent" id="parent">
                                            <option value="0">Is Parent</option>
                                            <?php $__currentLoopData = $lp; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $lp): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option <?php if($lp->id_inc==$parent): ?> selected <?php endif; ?> value="<?php echo e($lp->id_inc); ?>"><?php echo e($lp->nama_menu); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="varchar">Link Menu <?php echo form_error('link_menu') ?></label>
                                        <input type="text" class="form-control" name="link_menu" id="link_menu" placeholder="Link Menu" value="<?php echo $link_menu; ?>" />
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="int">Sort <?php echo form_error('sort') ?></label>
                                        <input type="text" class="form-control" name="sort" id="sort" placeholder="Sort" value="<?php echo $sort; ?>" />
                                    </div>
                                    <div class="form-group">
                                        <label for="varchar">Icon <?php echo form_error('icon') ?></label>
                                        <input type="text" class="form-control" name="icon" id="icon" placeholder="Icon" value="<?php echo $icon; ?>" />
                                    </div>
                                    <input type="hidden" name="id_inc" value="<?php echo $id_inc; ?>" />

                                </div>
                            </div>
                            <!--  -->
                            <div class="row">
                                <div class="col-md-offset-4 col-md-4">
                                    <div class="login-btn-inner">
                                        <div class="login-horizental">
                                            <button class="btn btn-sm btn-primary login-submit-cs" type="submit"><i class="fa fa-save"></i> Simpan</button>
                                            <?php echo anchor('menu','<i class="fa fa-close"></i> Batal','class="btn btn-custon-four btn-warning"'); ?>

                                        </div>
                                    </div>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH E:\xampp\htdocs\newkoishow\application\views/menu/Menu_form.blade.php ENDPATH**/ ?>