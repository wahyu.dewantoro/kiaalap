<div class="row">
    <div class="col-md-12">
        <h3><?php echo e($title); ?> </h3>
        <div class="row">
            <?php if(!empty($rank)): ?>
            <div class="col-md-6">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th  align="center" width="100px">No Ikan</th>
                            <th  align="center">Variety</th>
                            <th  align="center"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $__currentLoopData = $rank; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ra): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                            <td align="center"><?php echo e($ra->no_ikan); ?></td>
                            <td><?php echo e($ra->namavariety); ?> </td>
                            <td></td>
                        </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </tbody>
                </table>
            </div> 
            <?php endif; ?>
        </div>
    </div>
</div><?php /**PATH E:\xampp\htdocs\newkoishow\application\views/penjurian/dataFormBisChamp.blade.php ENDPATH**/ ?>