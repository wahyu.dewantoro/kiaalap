<?php $__env->startSection('judul'); ?>
<h3>Groups
</h3>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="sparkline9-list responsive-mg-b-30">
            <div class="sparkline9-graph">
                <div class="basic-login-form-ad">
                    
                            <div class="basic-login-inner">
                                <form action="<?php echo e($action); ?>" method="post">
                                    <?= csrfToken() ?>
                                    
	    <div class="form-group">
                                                <label for="varchar">Name <?php echo form_error('name') ?></label>
                                                <input type="text" class="form-control" name="name" id="name" placeholder="Name" value="<?php echo $name; ?>" />
                                            </div>
	    <div class="form-group">
                                                <label for="varchar">Description <?php echo form_error('description') ?></label>
                                                <input type="text" class="form-control" name="description" id="description" placeholder="Description" value="<?php echo $description; ?>" />
                                            </div>
	    <input type="hidden" name="id" value="<?php echo $id; ?>" />
                                    <div class="login-btn-inner">
                                    <div class="login-horizental">
                                    <button class="btn btn-sm btn-primary login-submit-cs" type="submit"><i class="fa fa-save"></i> Simpan</button>
                                    <?php echo anchor('test','<i class="fa fa-close"></i> Batal','class="btn btn-custon-four btn-warning"'); ?>

                                    </div>
                                    </div>
                                </form>
                            </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\kiaalap\application\views/test/Test_form.blade.php ENDPATH**/ ?>