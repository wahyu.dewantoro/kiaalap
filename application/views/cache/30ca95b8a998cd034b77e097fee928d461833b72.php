<?php $__env->startSection('judul'); ?>
<h3>Dashboard 
</h3>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="row">
    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
        <div class="analytics-sparkle-line reso-mg-b-30">
            <div class="analytics-content">
                <table class="table table-bordered">
                    <?php $total=0; ?>
                    <?php $__currentLoopData = $rfe; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $rfe): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                    <tr>
                        <td><?php echo e($rfe->ket); ?></td>
                        <td align="center"><?php echo e($rfe->jum); ?></td>
                    </tr>
                    <?php $total+=$rfe->jum; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <tr>
                        <td>Total</td>
                        <td align="center"><?php echo e($total); ?></td>
                    </tr>
                </table>

            </div>
        </div>
        <div class="analytics-sparkle-line reso-mg-b-30">
            <div class="analytics-content">
                 <?php echo anchor('peserta/dataikan','<i class="fa fa-print"></i> Data Ikan','class="btn btn-primary"'); ?><br><br>
                 <?php echo anchor('peserta/fotoikan','<i class="fa fa-print"></i> Foto Ikan','class="btn btn-info"'); ?> <br><br>
                 <?php echo anchor('peserta/fotoikanbesar','<i class="fa fa-print"></i> Ikan Besar','class="btn btn-success"'); ?><br><br>
                 <?php echo anchor('peserta/printdoorprize','<i class="fa fa-print"></i> Doorprize','class="btn btn-warning"'); ?>

            </div>
        </div>
    </div>
    <div class="col-lg-39 col-md-9 col-sm-12 col-xs-12">
        <div class="analytics-sparkle-line reso-mg-b-30">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>Rekap</th>
                        <?php $__currentLoopData = $ukuran; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ru): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <th><?php echo e($ru->namaukuran); ?></th>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </tr>
                </thead>
                <tbody>
                    <?php $__currentLoopData = $variety; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $rv): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr>
                        <td><?php echo e($rv->namavariety); ?> (<?php echo e($rv->kelas); ?>)</td>
                        <?php $__currentLoopData = $ukuran; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ru): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <td <?php if(ikanBySize($rv->id,$ru->ukuranMin,$ru->ukuranMax)==0): ?> bgcolor="gray" <?php endif; ?> align="center"><?php echo e(ikanBySize($rv->id,$ru->ukuranMin,$ru->ukuranMax)); ?></td>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH E:\xampp\htdocs\newkoishow\application\views/index.blade.php ENDPATH**/ ?>