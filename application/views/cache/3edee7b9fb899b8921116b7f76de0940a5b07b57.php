<?php $__env->startSection('judul'); ?>
<h3>Championref
</h3>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="row">
    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        <div class="sparkline9-list responsive-mg-b-30">
            <div class="sparkline9-graph">
                <div class="basic-login-form-ad">

                    <div class="basic-login-inner">
                        <form action="<?php echo e($action); ?>" method="post">
                            <?= csrfToken() ?>
                            <div class="form-group">
                                <label for="int">Ukuran </label>
                                <div class="row">
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" name="vmin" id="vmin" placeholder="Minimal" value="<?php echo $vmin; ?>" />
                                        <?php echo form_error('vmin') ?>
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" name="vmax" id="vmax" placeholder="Maximal" value="<?php echo $vmax; ?>" />
                                        <?php echo form_error('vmax') ?>
                                    </div>
                                </div>

                            </div>

                            <div class="form-group">
                                <label for="varchar">Chmpion </label>
                                <div class="row">
                                    <div class="col-md-6">
                                        <!-- <input type="text"placeholder="Id Juara" value="<?php echo $id_juara; ?>" /> -->
                                        <select class="form-control" name="id_juara" id="id_juara">
                                            <option value="">Pilih</option>
                                            <?php $__currentLoopData = $juara; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $j): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option <?php if($j->id==$id_juara): ?> selected <?php endif; ?> value="<?php echo e($j->id); ?>"><?php echo e($j->namajuara); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </select>
                                        <?php echo form_error('id_juara') ?>
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" name="alias" id="alias" placeholder="Alias" value="<?php echo $alias; ?>" />
                                        <?php echo form_error('alias') ?>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="int">Sort <?php echo form_error('urut') ?></label>
                                <input type="text" class="form-control" name="urut" id="urut" placeholder="Urut" value="<?php echo $urut; ?>" />
                            </div>
                            <div class="form-group">

                            </div>
                            <input type="hidden" name="id" value="<?php echo $id; ?>" />
                            <div class="login-btn-inner">
                                <div class="login-horizental">
                                    <button class="btn btn-sm btn-primary login-submit-cs" type="submit"><i class="fa fa-save"></i> Simpan</button>
                                    <?php echo anchor('refchamp','<i class="fa fa-close"></i> Batal','class="btn btn-custon-four btn-warning"'); ?>

                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH E:\xampp\htdocs\newkoishow\application\views/refchamp/Refchamp_form.blade.php ENDPATH**/ ?>