<?php $__env->startSection('judul'); ?>
<h3>Data Ikan <div class="pull-right"> </div>
</h3>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>

<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="sparkline13-list">
            <div class="sparkline13-graph">
                <div class="datatable-dashv1-list custom-datatable-overright table-responsive">
                    <table data-toolbar="#toolbarb" id="tableabc" data-toggle="table" data-pagination="true" data-search="true" data-resizable="true">
                        <thead>
                            <tr>
                                <th class="text-center">ID Ikan</th>
                                <th class="text-center">Variety</th>
                                <th class="text-center">Owner</th>
                                <th class="text-center">Handling</th>
                                <th class="text-center">Ikan</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $__currentLoopData = $ikan; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ikan): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr>
                                <td  align="center"><?php echo e($ikan->no_ikan); ?></td>
                                <td>
                                    <?php echo e($ikan->namavariety); ?> <br>
                                    <small class="text-muted"><?php echo e($gender[$ikan->gender]); ?>, <?php echo e($breeder[$ikan->breeder]); ?></small>
                                </td>
                                <td><?php echo e($ikan->namaowner); ?>

                                <br>
                                    <small class="text-muted"><?php echo e($ikan->kotaowner); ?></small>
                                </td>
                                <td><?php echo e($ikan->namahandling); ?><br>
                                    <small class="text-muted"><?php echo e($ikan->kotahandling); ?></small></td>
                                <td   align="center"> <img src="<?php echo e(base_url().$ikan->gambar_ikan); ?>" alt="..." class="img-thumbnail img-responsive"  >
                                
                                        </td>
                            </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                        </tbody>

                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('css'); ?>
<style>
    .img-responsive {
        width: auto;
        max-width: 40px;
        height: 60px;
        
    }
</style>
<link rel="stylesheet" href="<?php echo e(base_url('assets/')); ?>css/data-table/bootstrap-table.css">
<link rel="stylesheet" href="<?php echo e(base_url('assets/')); ?>css/data-table/bootstrap-editable.css">
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
<!-- data table JS
		============================================ -->
<script src="<?php echo e(base_url('assets/')); ?>js/data-table/bootstrap-table.js"></script>
<script src="<?php echo e(base_url('assets/')); ?>js/data-table/tableExport.js"></script>
<script src="<?php echo e(base_url('assets/')); ?>js/data-table/data-table-active.js"></script>
<script src="<?php echo e(base_url('assets/')); ?>js/data-table/bootstrap-table-editable.js"></script>
<script src="<?php echo e(base_url('assets/')); ?>js/data-table/bootstrap-editable.js"></script>
<script src="<?php echo e(base_url('assets/')); ?>js/data-table/bootstrap-table-resizable.js"></script>
<script src="<?php echo e(base_url('assets/')); ?>js/data-table/colResizable-1.5.source.js"></script>
<script src="<?php echo e(base_url('assets/')); ?>js/data-table/bootstrap-table-export.js"></script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH E:\xampp\htdocs\newkoishow\application\views/peserta/dataikan.blade.php ENDPATH**/ ?>