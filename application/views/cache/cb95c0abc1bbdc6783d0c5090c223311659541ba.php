<?php $__env->startSection('judul'); ?>
<h3>Form Pengguna
</h3>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="sparkline9-list responsive-mg-b-30">
            <div class="sparkline9-graph">
                <div class="basic-login-form-ad">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="basic-login-inner">
                                <div id="infoMessage"><?php echo $message; ?></div>
                                <?php echo form_open(uri_string()); ?>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Nama Pengguna </label>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <?php echo form_input($first_name); ?>
                                                </div>
                                                <div class="col-md-6">
                                                    <?php echo form_input($last_name); ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <?php echo lang('create_user_company_label', 'company'); ?>
                                            <?php echo form_input($company); ?>
                                        </div>
                                        <div class="form-group">
                                            <?php echo lang('create_user_phone_label', 'phone'); ?>
                                            <?php echo form_input($phone); ?>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <?php echo lang('create_user_password_label', 'password'); ?>
                                            <?php echo form_input($password); ?>
                                        </div>
                                        <div class="form-group">
                                            <?php echo lang('create_user_password_confirm_label', 'password_confirm'); ?>
                                            <?php echo form_input($password_confirm); ?>
                                        </div>
                                        <div class="form-group">


                                            <h3><?php echo lang('edit_user_groups_heading'); ?></h3>
                                            <?php foreach ($groups as $group) { ?>
                                            <label class="checkbox">
                                                <?php
                                                    $gID = $group['id'];
                                                    $checked = null;
                                                    $item = null;
                                                    foreach ($currentGroups as $grp) {
                                                        if ($gID == $grp->id) {
                                                            $checked = ' checked="checked"';
                                                            break;
                                                        }
                                                    }
                                                    ?>
                                                <input type="checkbox" name="groups[]" value="<?php echo $group['id']; ?>" <?php echo $checked; ?>>
                                                <?php echo htmlspecialchars($group['name'], ENT_QUOTES, 'UTF-8'); ?>
                                            </label>


                                            <?php }  ?>
                                        </div>
                                        <?php echo form_hidden('id', $user->id); ?>
                                        <?php echo form_hidden($csrf); ?>
                                        <div class="form-group">
                                            <button class="btn btn-sm btn-primary"><i class="fa fa-save"></i> Simpan </button>
                                        </div>
                                    </div>
                                </div>
                                <?php echo form_close(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH E:\xampp\htdocs\newkoishow\application\views/pengguna/edit_user.blade.php ENDPATH**/ ?>