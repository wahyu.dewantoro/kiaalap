<?php $__env->startSection('judul'); ?>
<h3>Poinmaster <div class="pull-right"> <?php echo anchor('poin/create','<i class="fa fa-plus"></i> Tambah','class="btn btn-custon-four btn-primary"'); ?> </div>
</h3>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>

<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="sparkline13-list">
            <div class="sparkline13-graph">
                <div class="datatable-dashv1-list custom-datatable-overright">
                <form action="<?php echo site_url('poin/index'); ?>" class="form-inline" method="get">
                <div class="input-group">
                    <input type="text" class="form-control" name="q" value="<?php echo $q; ?>">
                    <span class="input-group-btn">
                        <?php 
                            if ($q <> '')
                            {
                                ?>
                                <a href="<?php echo site_url('poin'); ?>" class="btn btn-default">Reset</a>
                                <?php
                            }
                        ?>
                      <button class="btn btn-xs btn-primary" type="submit">Search</button>
                    </span>
                </div>
            </form>
            <table class="table table-striped" style="margin-bottom: 10px">
            <tr>
                <th >No</th>
		<th>Ukuranmin</th>
		<th>Ukuranmax</th>
		<th>Juara Id</th>
		<th>Poin</th>
		<th></th>
            </tr><?php
            foreach ($poin_data as $poin)
            {
                ?>
                <tr>
			<td    width="80px"><?php echo ++$start ?></td>
			<td><?php echo $poin->ukuranmin ?></td>
			<td><?php echo $poin->ukuranmax ?></td>
			<td><?php echo $poin->juara_id ?></td>
			<td><?php echo $poin->poin ?></td>
			<td style="text-align:center" width="100px">
				<?php 
				echo anchor(site_url('poin/update/'.$poin->id),'<i class="fa fa-edit"></i>','class="btn btn-xs btn-success"'); 
				echo anchor(site_url('poin/delete/'.$poin->id),'<i class="fa fa-trash"></i>','class="btn btn-xs btn-danger" onclick="javasciprt: return confirm(\'Apakah anda yakin ?\')"'); 
				?>
			</td>
		</tr>
                <?php
            }
            ?>
        </table>
        <div class="row">
        <div class="col-md-6">
            <a href="#" class="btn btn-primary">Total Record : <?php echo $total_rows ?></a>
	    </div>
        <div class="col-md-6 text-right">
            <?php echo $pagination ?>
        </div>
    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH E:\xampp\htdocs\newkoishow\application\views/poin/Poin_list.blade.php ENDPATH**/ ?>