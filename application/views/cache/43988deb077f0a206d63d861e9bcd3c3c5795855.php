<?php $__env->startSection('judul'); ?>
<h3>Edit Ikan<div class="pull-right">

        <?php echo anchor($_SERVER['HTTP_REFERER'],'<i class="fa fa-angle-double-left"></i> kembali','class="btn btn-sm btn-primary"'); ?> </div>
</h3>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="row">
    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
        <div class="profile-info-inner">
            <div class="profile-details-hr">
                <div class="row">
                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-6">
                        <div class="address-hr">
                            <p><b>Handling</b><br /> <?php echo e($res->namahandling); ?></p>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-6">
                        <div class="address-hr tb-sm-res-d-n dps-tb-ntn">
                            <p><b>Kota</b><br /> <?php echo e($res->kotahandling); ?></p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-6">
                        <div class="address-hr">
                            <p><b>Owner</b><br /> <?php echo e($res->namaowner); ?></p>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-6">
                        <div class="address-hr tb-sm-res-d-n dps-tb-ntn">
                            <p><b>Kota</b><br /> <?php echo e($res->kotaowner); ?></p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="address-hr">
                            <p><b>Kode</b><br /> <u><?php echo e($res->uniq_code); ?></u></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
        <form action="<?php echo e($action); ?>" method="post" enctype="multipart/form-data">
            <?= csrfToken() ?>
            <input type="hidden" name="id" value="<?php echo e($res->id); ?>">
            <input type="hidden" name="id2" value="<?php echo e($res->uniq_code); ?>">
            <div class="panel panel-primary">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 120px; height: 160px;">
                                    <img src="<?php echo e(base_url().$res->gambar_ikan); ?>" alt="Smiley face">
                                </div>
                                <small> <span class="fileinput-filename"></span></small>
                                <div>
                                    <span class="btn-file"> <input type="file" accept="image/*" name="gambar_ikan"> </span>
                                    <input type="hidden" name="gambar_ikan_lama" value="<?php echo e($res->gambar_ikan); ?>">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <select name="variety_id" class="form-control" required>
                                    <option value="">Pilih variety</option>
                                    <?php $__currentLoopData = $variety; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $vv): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option <?php if($vv->id==$res->variety_id): ?> selected <?php endif; ?> value="<?php echo e($vv->id); ?>"><?php echo e($vv->namavariety); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <input type="number" name="ukuran" value="<?php echo e($res->ukuran); ?>" placeholder="Ukuran" class="form-control">
                            </div>
                            <div class="form-group">
                                <select name="gender" class="form-control" required>
                                    <option value="">Gender</option>
                                    <?php for($g=1;$g<=2;$g++): ?> <option <?php if($res->gender==$g): ?> selected <?php endif; ?> value="<?php echo e($g); ?>"><?php echo e($gender[$g]); ?></option>
                                        <?php endfor; ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <select name="breeder" class="form-control" required>
                                    <option value="">Breeder</option>
                                    <?php for($b=1;$b<=2;$b++): ?> <option <?php if($res->breeder==$b): ?> selected <?php endif; ?> value="<?php echo e($b); ?>"><?php echo e($breeder[$b]); ?></option>
                                        <?php endfor; ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <button class="btn btn-primary" type="submit"><i class="fa fa-save"></i> Simpan</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('css'); ?>
<link href="<?= base_url() ?>assets/js/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
<style>
    .img-responsive {
        width: auto;
        height: 230px;
    }
</style>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
<script src="<?= base_url() ?>assets/js/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH E:\xampp\htdocs\newkoishow\application\views/peserta/editikan.blade.php ENDPATH**/ ?>