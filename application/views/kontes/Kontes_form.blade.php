@extends('layouts.master')
@section('judul')
<h3>Kontes
</h3>
@endsection
@section('content')
<div class="row">
    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
        <div class="sparkline9-list responsive-mg-b-30">
            <div class="sparkline9-graph">
                <div class="basic-login-form-ad">

                    <div class="basic-login-inner">
                        <form action="{{ $action }}" method="post">
                            <?= csrfToken() ?>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="varchar">Nama Kontes <?php echo form_error('namakontes') ?></label>
                                        <input type="text" class="form-control" name="namakontes" id="namakontes" placeholder="Namakontes" value="<?php echo $namakontes; ?>" />
                                    </div>
                                    <div class="form-group">
                                        <label for="varchar">Tempat <?php echo form_error('tempat') ?></label>
                                        <input type="text" class="form-control" name="tempat" id="tempat" placeholder="Tempat" value="<?php echo $tempat; ?>" />
                                    </div>
                                    <div class="form-group">
                                        <label for="varchar">Kota <?php echo form_error('kota') ?></label>
                                        <input type="text" class="form-control" name="kota" id="kota" placeholder="Kota" value="<?php echo $kota; ?>" />
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="int">Daftar Online<?php echo form_error('daftar') ?></label>
                                        @php
                                        $ld=array('Tutup','Buka');
                                        @endphp
                                        <select class="form-control" name="daftar" id="daftar">
                                            @for($do=0;$do < 2; $do++) <option @if($do==$daftar ) @endif value='{{ $do }}'> {{ $ld[$do] }}</option>
                                                @endfor
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="int">Perolehan Poin <?php echo form_error('poin') ?></label>
                                        <select class="form-control" name="poin" id="poin">
                                            @for($dop=0;$dop < 2; $dop++) <option @if($dop==$poin ) @endif value='{{ $dop }}'> {{ $ld[$dop] }}</option>
                                                @endfor
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="int">Most Handling <?php echo form_error('handling') ?></label>
                                        <select class="form-control" name="handling" id="handling">
                                            @for($doh=0;$doh < 2; $doh++) <option @if($doh==$handling ) @endif value='{{ $doh }}'> {{ $ld[$doh] }}</option>
                                                @endfor
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="int">Most Entri <?php echo form_error('entri') ?></label>
                                        <select class="form-control" name="entri" id="entri">
                                            @for($doe=0;$doe < 2; $doe++) <option @if($doe==$entri ) @endif value='{{ $doe }}'> {{ $ld[$doe] }}</option>
                                                @endfor
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 col-md-offset-6">
                                    
                            <input type="hidden" name="id" value="<?php echo $id; ?>" />
                            <div class="login-btn-inner">
                                <div class="login-horizental">
                                    <button class="btn btn-sm btn-primary login-submit-cs" type="submit"><i class="fa fa-save"></i> Simpan</button>
                                    {!! anchor('kontes','<i class="fa fa-close"></i> Batal','class="btn btn-custon-four btn-warning"') !!}
                                </div>
                            </div>
                                </div>
                            </div>


                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection