@extends('layouts.master')
@section('judul')
<h3>Kontes
</h3>
@endsection
@section('content')

<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="sparkline13-list">
            <div class="sparkline13-graph">
                <div class="datatable-dashv1-list custom-datatable-overright">

                    <table class="table table-striped" style="margin-bottom: 10px">
                        <tr>

                            <th>Nama Kontes</th>
                            <th>Tempat</th>
                            <th>Kota</th>
                            <th>Daftar Online</th>
                            <th>Perolehan Poin </th>
                            <th>Most Handling</th>
                            <th>Most Entri</th>
                            <th></th>
                        </tr>
                        @foreach($kontes_data as $kontes)
                        <tr>

                            <td>{{ $kontes->namakontes }}</td>
                            <td>{{ $kontes->tempat }}</td>
                            <td>{{ $kontes->kota }}</td>
                            <td>{{ $ld[$kontes->daftar] }}</td>
                            <td>{{ $ld[$kontes->poin] }}</td>
                            <td>{{ $ld[$kontes->handling] }}</td>
                            <td>{{ $ld[$kontes->entri] }}</td>
                            <td style="text-align:center" width="100px">

                            @php echo  anchor(site_url('kontes/update/' . $kontes->id), '<i class="fa fa-edit"></i>', 'class="btn btn-xs btn-success"') @endphp

                            </td>
                        </tr>
                        @endforeach
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection