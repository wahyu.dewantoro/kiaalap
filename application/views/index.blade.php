@extends('layouts.master')
@section('judul')
<h3>Dashboard
</h3>
@endsection
@section('content')
@if($is_handling==false)
<div class="row">
    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
        <div class="analytics-sparkle-line reso-mg-b-30">
            <div class="analytics-content">
                <table class="table table-bordered">
                    @php $total=0; @endphp
                    @foreach($rfe as $rfe)
                    <tr>
                        <td>{{ $rfe->ket }}</td>
                        <td align="center">{{ $rfe->jum }}</td>
                    </tr>
                    @php $total+=$rfe->jum; @endphp
                    @endforeach
                    <tr>
                        <td>Total</td>
                        <td align="center">{{ $total }}</td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="analytics-sparkle-line reso-mg-b-30">
            <div class="analytics-content">
                <!-- peserta/dataikan -->
                {!! anchor('peserta/dataikan','<i class="fa fa-print"></i> Data Ikan','class="btn btn-primary" target="_blank"') !!}<br><br>
                {!! anchor('peserta/fotoikan','<i class="fa fa-print"></i> Foto Ikan','class="btn btn-info" target="_blank"') !!} <br><br>
                {!! anchor('peserta/fotoikanbesar','<i class="fa fa-print"></i> Ikan Besar','class="btn btn-success" target="_blank"') !!}<br><br>
                {!! anchor('peserta/printdoorprize','<i class="fa fa-print"></i> Doorprize','class="btn btn-warning" target="_blank"') !!}
            </div>
        </div>
    </div>
    <div class="col-lg-39 col-md-9 col-sm-12 col-xs-12">
        <div class="analytics-sparkle-line reso-mg-b-30">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>Rekap</th>
                        @foreach($ukuran as $ru)
                        <th>{{ $ru->namaukuran }}</th>
                        @endforeach
                    </tr>
                </thead>
                <tbody>
                    @foreach($variety as $rv)
                    <tr>
                        <td>{{ $rv->namavariety}} ({{ $rv->kelas }})</td>
                        @foreach($ukuran as $ru)
                        <td @if(ikanBySize($rv->id,$ru->ukuranMin,$ru->ukuranMax)==0) bgcolor="gray" @endif align="center">{{ ikanBySize($rv->id,$ru->ukuranMin,$ru->ukuranMax) }}</td>
                        @endforeach
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@else
<div class="row">
    <a href="{{ base_url('regisikan') }}">
        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
            <div class="analytics-sparkle-line reso-mg-b-30">
                <div class="analytics-content">
                    <h5>Cart</h5>
                    <h2><span class="counter">{{ $cart }}</span> <span class="tuition-fees">Data</span></h2>
                </div>
            </div>
        </div>
    </a>
    <a href="{{ base_url('listikan') }}">
        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
            <div class="analytics-sparkle-line reso-mg-b-30">
                <div class="analytics-content">
                    <h5>Ikan Terdaftar</h5>
                    <h2><span class="counter"><?= $ikan ?></span> <span class="tuition-fees">Ekor</span></h2>
                </div>
            </div>
        </div>
    </a>

    <a href="{{ base_url('tagihan') }}">
        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
            <div class="analytics-sparkle-line reso-mg-b-30">
                <div class="analytics-content">
                    <h5>Tagihan</h5>
                    <h2>Rp.<span class="counter">{{ rupiah($tagihan)}}</span> <span class="tuition-fees"></span></h2>

                </div>
            </div>
        </div>
    </a>
</div>
@endif

@endsection