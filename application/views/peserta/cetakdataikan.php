<html>

<head>
    <style>
        @page {
            margin: 5px;
        }

        body {
            margin: 5px;
            font-family: Tahoma, Geneva, sans-serif;
            font-size:11px;
        }

        .center {
            margin-left: auto;
            margin-right: auto;
        }

        .text_tengah {
            text-align: center;
        }

        .responsive {
            width: auto;
            max-width: 110px;
            height: 150px;
        }
    </style>
</head>

<body>

    <?php foreach ($ukuran as $ru) {
        $ca = "";
        foreach ($variety as $rv) {
            $ta = $ru->namaukuran . ', Kelas ' . $rv->kelas; ?>
            <h3 class="text_tengah"><?= $ta ?></h3>
            <?php $ikan = $this->db->query("SELECT ikandetail.id,gambar_ikan,ukuran,namavariety,breeder,gender
                        FROM ikandetail 
                        join variety on variety.id=ikandetail.variety_id
                        WHERE ( ukuran >=" . $ru->ukuranMin . " AND ukuran<= " . $ru->ukuranMax . ") AND kelas='" . $rv->kelas . "' order by variety.sort asc , ikandetail.id asc")->result(); ?>

            <table border='1' width="100%" cellspacing='0'>
                <thead>
                    <tr>
                        <th align="center">No</th>
                        <th align="center">variety</th>
                        <th align="center">ID</th>
                        <th align="center">Ukuran</th>
                        <th align="center">Keterangan</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $d=1; foreach ($ikan as $ri) { ?>
                        <tr>
                            <td align="center"><?= $d ?></td>
                            <td><?= $ri->namavariety ?></td>
                            <td align="center"><?= $ri->id ?></td>
                            <td align="center"><?= $ri->ukuran ?> CM</td>
                            <td><?= $gender[$ri->gender].', '.$breeder[$ri->breeder] ?></td>
                        </tr>
                    <?php $d++; } ?>
                </tbody>
            </table>
            
        <?php } ?>
        <div <?php if ($ca != $ta) { 
                        ?> style="page-break-before: always;" <?php } 
                                                                    ?>></div>
    <?php $ca = $ru->namaukuran;
    } ?>
</body>

</html>