@extends('layouts.master')
@section('judul')
<h3>Peserta <div class="pull-right"> </div>
</h3>
@endsection
@section('content')

<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="sparkline13-list">
            <div class="sparkline13-graph">
                <div class="datatable-dashv1-list custom-datatable-overright">
                    <form action="<?php echo site_url('peserta/index'); ?>" class="form-inline" method="get">
                        <div class="input-group">
                            <input type="text" class="form-control" name="q" value="<?php echo $q; ?>">
                            <span class="input-group-btn">
                                <?php
                                if ($q <> '') {
                                    ?>
                                    <a href="<?php echo site_url('peserta'); ?>" class="btn btn-default">Reset</a>
                                <?php
                                }
                                ?>
                                <button class="btn btn-xs btn-primary" type="submit">Search</button>
                            </span>
                        </div>
                    </form>
                    <table class="table table-striped" style="margin-bottom: 10px">
                        <tr>
                            <th>No</th>
                            <th>Kode</th>
                            <th>Handling</th>
                            <th>Owner</th>
                            <th>Ikan</th>
                            <th>Created By</th>
                            <th>Updated By</th>
                            <th></th>
                        </tr>
                        @foreach ($peserta_data as $peserta)
                        <tr>
                            <td width="80px">{{ ++$start }}</td>
                            <td><a href="{{ base_url('peserta/read/'.$peserta->uniq_code) }}">{{ $peserta->uniq_code }}</a>
                                @if($peserta->no_kwitansi<>'') <br> <small class="text-muted">No Kwitansi : {{ $peserta->no_kwitansi}} </small> @else <br> <small class="text-muted" style="color:red;">Belum bayar ! </small> @endif
                            </td>
                            <td>
                                {{ $peserta->namahandling }}<br>
                                <small class="text-muted">{{ $peserta->kotahandling }}</small>
                            </td>
                            <td>
                                {{ $peserta->namaowner }}<br>
                                <small class="text-muted">{{ $peserta->kotaowner }}</small>
                            </td>
                            <td>{{ $peserta->ikan }} ekor</td>
                            <td>{{ getNamaUser($peserta->created_by) }}<br> <small class="text-muted"> {{ $peserta->created_at }}</small> </td>
                            <td>{{ getNamaUser($peserta->updated_by) }}<br> <small class="text-muted"> {{ $peserta->updated_at }}</small> </td>
                            <td style="text-align:center" width="100px">
                                <?php
                                echo anchor(site_url('peserta/read/' . $peserta->uniq_code), '<i class="fa fa-eye"></i>', 'class="btn btn-xs btn-primary"');
                                echo anchor(site_url('peserta/update/' . $peserta->id), '<i class="fa fa-edit"></i>', 'class="btn btn-xs btn-success"');
                                echo anchor(site_url('peserta/delete/' . $peserta->id), '<i class="fa fa-trash"></i>', 'class="btn btn-xs btn-danger" onclick="javasciprt: return confirm(\'Apakah anda yakin ?\')"');
                                ?>
                            </td>
                        </tr>
                        @endforeach
                    </table>
                    <div class="row">
                        <div class="col-md-6">
                            <a href="#" class="btn btn-primary">Total Record : {{ $total_rows }}</a>
                        </div>
                        <div class="col-md-6 text-right">
                            <?php echo $pagination ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection