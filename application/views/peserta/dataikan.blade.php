@extends('layouts.master')
@section('judul')
<h3>Data Ikan <div class="pull-right"> </div>
</h3>
@endsection
@section('content')

<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="sparkline13-list">
            <div class="sparkline13-graph">
                <div class="datatable-dashv1-list custom-datatable-overright table-responsive">
                    <table data-toolbar="#toolbarb" id="tableabc" data-toggle="table" data-pagination="true" data-search="true" data-resizable="true">
                        <thead>
                            <tr>
                                <th class="text-center">ID</th>
                                <th class="text-center">Variety</th>
                                <th class="text-center">Owner</th>
                                @if($is_handling==false)<th class="text-center">Handling</th> @endif
                                <th class="text-center">Ikan</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($ikan as $ikan)
                            <tr>
                                <td  align="center">{{ $ikan->no_ikan }}</td>
                                <td>
                                    {{ $ikan->namavariety }} <br>
                                    <small class="text-muted"> {{ $ikan->ukuran }} cm, {{  $gender[$ikan->gender]}}, {{ $breeder[$ikan->breeder] }}</small>
                                </td>
                                <td>{{ $ikan->namaowner }}
                                <br>
                                    <small class="text-muted">{{$ikan->kotaowner }}</small>
                                </td>
                                @if($is_handling==false)<td>{{ $ikan->namahandling }}<br>
                                    <small class="text-muted">{{$ikan->kotahandling }}</small></td> @endif
                                <td   align="center"> <img src="{{ base_url().$ikan->gambar_ikan }}" alt="..." class="img-thumbnail img-responsive"  >
                                
                                        </td>
                            </tr>
                            @endforeach

                        </tbody>

                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('css')
<style>
    .img-responsive {
        width: auto;
        max-width: 40px;
        height: 60px;
        
    }
</style>
<link rel="stylesheet" href="{{ base_url('assets/') }}css/data-table/bootstrap-table.css">
<link rel="stylesheet" href="{{ base_url('assets/') }}css/data-table/bootstrap-editable.css">
@endsection
@section('script')
<!-- data table JS
		============================================ -->
<script src="{{ base_url('assets/') }}js/data-table/bootstrap-table.js"></script>
<script src="{{ base_url('assets/') }}js/data-table/tableExport.js"></script>
<script src="{{ base_url('assets/') }}js/data-table/data-table-active.js"></script>
<script src="{{ base_url('assets/') }}js/data-table/bootstrap-table-editable.js"></script>
<script src="{{ base_url('assets/') }}js/data-table/bootstrap-editable.js"></script>
<script src="{{ base_url('assets/') }}js/data-table/bootstrap-table-resizable.js"></script>
<script src="{{ base_url('assets/') }}js/data-table/colResizable-1.5.source.js"></script>
<script src="{{ base_url('assets/') }}js/data-table/bootstrap-table-export.js"></script>

@endsection