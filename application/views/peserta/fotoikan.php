<html>

<head>
    <style>
        @page {
            margin: 5px;
        }

        body {
            margin: 5px;
            font-family: Tahoma, Geneva, sans-serif;
            font-size:11px;
        }

        .center {
            margin-left: auto;
            margin-right: auto;
        }

        .text_tengah {
            text-align: center;
        }

        .responsive {
            width: auto;
            max-width: 110px;
            height: 150px;
        }
    </style>
</head>

<body>

    <?php foreach ($ukuran as $ru) {
        $ca = "";
        foreach ($variety as $rv) {
            $ta = $ru->namaukuran . ', Kelas ' . $rv->kelas; ?>
            <h3 class="text_tengah"><?= $ta ?></h3>
            <!-- data iwak -->
            <?php $ikan = $this->db->query("SELECT ikandetail.id no_ikan,gambar_ikan,ukuran,namavariety
                        FROM ikandetail 
                        join variety on variety.id=ikandetail.variety_id
                        WHERE ( ukuran >=" . $ru->ukuranMin . " AND ukuran<= " . $ru->ukuranMax . ") AND kelas='" . $rv->kelas . "' order by variety.sort asc , ikandetail.id asc")->result(); ?>
            <?php
                    $kolom  = 5;
                    $gambar = array_chunk($ikan, $kolom); ?>
            <table style="table-layout: fixed; width: 100%;">
                <?php foreach ($gambar as $chunk) { ?>
                    <tr>
                        <?php foreach ($chunk as $ikane) { ?>
                            <td>
                                <table>
                                    <tr>
                                        <td>
                                            <img class="responsive" src="<?php echo $ikane->gambar_ikan ?>">
                                        </td>
                                    </tr>
                                     <tr>
                                        <td>
                                            <b><?php  echo  $ikane->no_ikan ?></b>, <?php echo $ikane->namavariety.' - '.$ikane->ukuran ?> cm
                                        </td>
                                    </tr> 
                                </table>
                            </td>
                        <?php } ?>
                    </tr>
                <?php } ?>
            </table>

            <div <?php if ($ca != $ta) { ?> style="page-break-before: always;" <?php } ?>></div>
    <?php $ca = $ta;
        }
    } ?>
</body>

</html>