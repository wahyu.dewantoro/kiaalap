@extends('layouts.master')
@section('judul')
<h3>Form Peserta
</h3>
@endsection
@section('content')
<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="sparkline9-list responsive-mg-b-30">
            <div class="sparkline9-graph">
                <div class="basic-login-form-ad">

                    <div class="basic-login-inner">
                        <form action="{{ $action }}" method="post">
                            <?= csrfToken() ?>
                            <div class="form-group">
                                <label for="varchar">Kode <?php echo form_error('uniq_code') ?></label>
                                <div class="row">
                                    <div class="col-md-3">
                                        <input type="text" readonly class="form-control" name="uniq_code" id="uniq_code" placeholder="Uniq Code" value="<?php echo $uniq_code; ?>" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="varchar">Handling <?php echo form_error('namahandling') ?></label>
                                <div class="row">
                                    <div class="col-md-3">
                                        <input type="text" class="form-control" name="namahandling" id="namahandling" placeholder="Nama Handling" value="<?php echo $namahandling; ?>" />
                                    </div>
                                    <div class="col-md-2">
                                        <input type="text" class="form-control" name="kotahandling" id="kotahandling" placeholder="Kota Handling" value="<?php echo $kotahandling; ?>" />
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="varchar">Owner <?php echo form_error('namaowner') ?></label>
                                <div class="row">
                                    <div class="col-md-3">
                                        <input type="text" class="form-control" name="namaowner" id="namaowner" placeholder="Nama Owner" value="<?php echo $namaowner; ?>" />
                                    </div>
                                    <div class="col-md-2">
                                        <input type="text" class="form-control" name="kotaowner" id="kotaowner" placeholder="Kota Owner" value="<?php echo $kotaowner; ?>" />
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="id" value="<?php echo $id; ?>" />
                            <div class="login-btn-inner">
                                <div class="login-horizental">
                                    <button class="btn btn-sm btn-primary login-submit-cs" type="submit"><i class="fa fa-save"></i> Simpan</button>
                                    {!! anchor('peserta','<i class="fa fa-close"></i> Batal','class="btn btn-custon-four btn-warning"') !!}
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection