@extends('layouts.master')
@section('judul')
<h3>Penjurian Rank 1-5 <div class="pull-right"> </div>
</h3>
@endsection
@section('content')
<div class="row">
    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
        <div class="profile-info-inner">
            <div class="profile-details-hr">
                <form>
                    <div class="form-group">
                        <select id="noikan" class="form-control select2_demo_3" data-placeholder="Cari ikan">
                            <option value=""></option>
                            @foreach($noikan as $ni)
                            <option value="{{ $ni->no_ikan }}">{{ $ni->no_ikan.' - '.$ni->namavariety }} </option>
                            @endforeach
                        </select>
                    </div>
                </form>
                <hr>
                <form method="get" action="{{ base_url('penjurian/kategori') }}">
                    <div class="form-group">
                        <select onchange="this.form.submit()" name="ukuran" id="ukuran" class="select2_demo_3 form-control" required>
                            @foreach($ukuran as $ru)
                            <option @if($vu==$ru->ukuranMin.'_'.$ru->ukuranMax) selected @endif value="{{ $ru->ukuranMin.'_'.$ru->ukuranMax }}">{{ $ru->namaukuran }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">

                        <select onchange="this.form.submit()" name="variety" id="variety" class="form-control select2_demo_3" data-placeholder="Pilih variety" required>
                            <option value=""></option>
                            @foreach($variety as $rv)
                            <option @if($vv==$rv->id) selected @endif value="{{ $rv->id }}">{{ $rv->namavariety }}</option>
                            @endforeach
                        </select>
                    </div>
                </form>
                <hr>
                <div id="listjuara"></div>
            </div>
        </div>
    </div>
    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
        <div class="row">
            @foreach($ikan as $detail)
            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-6">
                <div data-id="{{ $detail->id }}" class="student-inner-std res-mg-b-30 ikan">
                    <div class="student-img">
                        <img class="img-responsive" src="{{ base_url($detail->gambar_ikan) }}" alt="" />
                    </div>
                    <div class="student-dtl">
                        <p class="dp-ag"><b>ID</b>: {{ $detail->id }}<br> <b>Size</b>: {{ $detail->ukuran }} cm<br>
                            <!-- {!-- $detail->namahandling --!} ({{ $detail->namaowner }}) -->
                        </p>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">Data Ikan</h4>
            </div>
            <div class="modal-body">
            </div>
             
        </div>
    </div>
</div>
@endsection
@section('css')
<!-- select2 CSS
		============================================ -->
<link rel="stylesheet" href="{{ base_url().'assets/' }}css/select2/select2.min.css">
<style>
    .img-responsive {
        width: auto;
        height: 150px;
    }
</style>
@endsection
@section('script')
<!-- select2 JS
		============================================ -->
<script src="{{ base_url().'assets/' }}js/select2/select2.full.min.js"></script>
<script src="{{ base_url().'assets/' }}js/select2/select2-active.js"></script>
<script>
    $(document).on('change', '#noikan', function(e) {
        // alert();
        e.preventDefault();
        $(".modal-body").html('');
        $("#myModal").modal('show');
        $.ajax({
            url: "<?php echo base_url('penjurian/cariikan'); ?>",
            type: 'get',
            data: {id:$('#noikan').val()},
            success: function(data) {
                $(".modal-body").html(data);
            }
        });
    });


    $(function() {
        loadjuara();

        $(document).on('click', '.ikan', function(e) {
            e.preventDefault();
            var ikan = $(this).attr('data-id');
            $.get('<?php echo base_url() . "penjurian/choserank" ?>', {
                    id: $(this).attr('data-id'),
                    ukuran: $('#ukuran').val(),
                    variety: $('#variety').val()
                },
                function(html) {
                    // $('#listjuara').html(html);
                    loadjuara();
                }
            );
        });

        function loadjuara() {
            $.get('<?php echo base_url() . "penjurian/listrank" ?>', {
                    ukuran: $('#ukuran').val(),
                    variety: $('#variety').val()
                },
                function(html) {
                    $('#listjuara').html(html);
                }
            );
        }
    });
</script>
@endsection