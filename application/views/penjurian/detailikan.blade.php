<div class="row">
    <div class="col-md-4">
        <img class="img-responsive" src="{{ base_url($gambar_ikan) }}" alt="" />
    </div>
    <div class="col-md-8">
        <table class="table">
            <tr>
                <td width="20%">No Ikan</td>
                <td> {{ $no_ikan }} 
                    {!! anchor('penjurian/editikan/'.$no_ikan,'<i class="fa fa-edit"></i> Edit','class="btn btn-xs btn-success"') !!}
                </td>
            </tr>
            <tr>
                <td width="20%">Variety</td>
                <td> {{ $namavariety }} <br>
                <small class="text-muted"> {{ $gd[$gender].', '.$br[$breeder] }} </small>
            </td>
            </tr>
            <tr>
                <td width="20%">Ukuran</td>
                <td> {{ $ukuran }} cm</td>
            </tr>
            <tr>
                <td width="20%">Handling</td>
                <td> {{ $namahandling }}<br> <small class="text-muted"> {{ $kotahandling }}</small> </td>
            </tr>
            <tr>
                <td width="20%">Owner</td>
                <td> {{ $namaowner }}<br> <small class="text-muted"> {{ $kotahandling }}</small> </td>
            </tr>
        </table>
    </div>
</div>
<style>
    .img-responsive {
        width: auto;
        height: 150px;
    }
</style>