<div class="row">
    <div class="col-md-12">
        <h3>{{ $title }} </h3>
        <div class="row">
            @if(!empty($rank))
            <div class="col-md-6">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th  align="center" width="100px">No Ikan</th>
                            <th  align="center">Variety</th>
                            <th  align="center"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($rank as $ra)
                        <tr>
                            <td align="center">{{ $ra->no_ikan }}</td>
                            <td>{{ $ra->namavariety}} </td>
                            <td></td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div> 
            @endif
        </div>
    </div>
</div>