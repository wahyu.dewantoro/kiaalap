@extends('layouts.master')
@section('judul')
<h3>Penjurian Bis & Champion<div class="pull-right"> </div>
</h3>
@endsection
@section('content')
<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="profile-info-inner">
            <div class="profile-details-hr">
                <input type="hidden" id="vc" name="vc" value="{{ $vc }}">
                <input type="hidden" id="vk" name="vk" value="{{ $vk }}">

                <div class="row">
                    <div class="col-md-10 col-md-offset-2">
                        @foreach($kelas as $kelas)
                        <button data-kelas="{{ $kelas->kelas }}" @if($vk==$kelas->kelas) class="kelas btn btn-primary" @else class="kelas btn btn-primary" @endif >Kelas <br> {{ $kelas->kelas}}</button>
                        @endforeach
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        @foreach($champ as $champ)
                        <br>
                        <button data-champ='{{ $champ->id }}' @if($vc==$champ->id) class=" champ btn btn-warning" @else class="champ btn btn-warning" @endif >@php $dd=explode(' ',$champ->alias); @endphp {{ $dd[0] }} <br>{{$dd[1] }} </button>
                        <br>
                        @endforeach
                    </div>
                    <div class="col-md-10">
                        <br>
                        <div id="hasildata"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
@endsection
@section('css')
<style>
    .img-responsive {
        width: auto;
        height: 150px;
    }
</style>
@endsection
@section('script')
<script>
    getData($('#vk').val(), $('#vc').val());

    $(document).on('click', '.kelas,.champ', function(e) {
        e.preventDefault();
        var _kelas = $(this).attr('data-kelas');
        var _champ = $(this).attr('data-champ');

        if (_kelas == null) {
            _kelas = $('#vk').val();
        }

        if (_champ == null) {
            _champ = $('#vc').val();
        }
        $('#vc').val(_champ);
        $('#vk').val(_kelas);
        getData(_kelas, _champ);
    });

    function getData(_kelas, _champ) {
        var _url = '<?= base_url() ?>' + 'penjurian/getdataformbis';
        $.get(_url, {
            'kelas': _kelas,
            'champ': _champ
        }, function(res) {
            $('#hasildata').html(res);
        });
    }
</script>
@endsection