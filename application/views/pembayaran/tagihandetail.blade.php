@extends('layouts.master')
@section('judul')
<h3>Detail Tagihan <div class="pull-right">

        {!! anchor('pembayaran','<i class="fa fa-angle-double-left"></i> kembali','class="btn btn-sm btn-primary"') !!} </div>
</h3>
@endsection
@section('content')
<div class="row">
    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
        <div class="profile-info-inner">
            <div class="profile-details-hr">
                <div class="row">
                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-6">
                        <div class="address-hr">
                            <p><b>Handling</b><br /> {{ $header->namahandling }}</p>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-6">
                        <div class="address-hr tb-sm-res-d-n dps-tb-ntn">
                            <p><b>Kota</b><br /> {{ $header->kotahandling}}</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-6">
                        <div class="address-hr">
                            <p><b>Owner</b><br /> {{ $header->namaowner }}</p>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-6">
                        <div class="address-hr tb-sm-res-d-n dps-tb-ntn">
                            <p><b>Kota</b><br /> {{ $header->kotaowner }}</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="address-hr">
                            <p><b>Kode</b><br /> <u>{{ $header->uniq_code }}</u></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
        <div class="row">
            @foreach($detail as $detail)
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                <div class="student-inner-std res-mg-b-30">
                    <div class="student-img">
                        <img class="img-responsive" src="{{ base_url($detail->gambar_ikan) }}" alt="" />
                    </div>
                    <div class="student-dtl">
                        <h5>{{ $detail->namavariety}}</h5>
                        <b>Size</b>: {{ $detail->ukuran }} cm</p>
                        <p class="dp">{{ $gender[$detail->gender] }}, {{ $breeder[$detail->breeder]}}
                            <br> Rp. {{rupiah($detail->tarif)}}
                            <br>
                            {!! anchor('pembayaran/editikan/'.$detail->id,'<i class="fa fa-edit"></i>','class="btn btn-sm btn-success"') !!}
                            {!! anchor('pembayaran/hapusikan/'.$detail->id,'<i class="fa fa-trash"></i>',' onclick="javasciprt: return confirm(\'Apakah anda yakin ?\')" class="btn btn-sm btn-danger" ') !!}
                        </p>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>
@endsection
@section('css')
<style>
    .img-responsive {
        width: auto;
        height: 230px;
    }
</style>
@endsection