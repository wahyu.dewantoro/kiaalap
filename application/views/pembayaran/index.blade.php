@extends('layouts.master')
@section('judul')
<h3>Pembayaran <div class="pull-right"> </div>
</h3>
@endsection
@section('content')

<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="product-payment-inner-st res-mg-t-30 analysis-progrebar-ctn">
            <ul id="myTabedu1" class="tab-review-design">
                <li @if($tab=='satu' ) class="active" @endif><a href="#description">Tagihan</a></li>
                <li @if($tab=='dua' ) class="active" @endif><a href="#reviews">Pembayaran</a></li>
            </ul>
            <div id="myTabContent" class="tab-content custom-product-edit st-prf-pro">
                <div class="product-tab-list tab-pane fade @if($tab=='satu') active in @endif" id="description">
                    <div class="row">
                        <div class="col-md-12">
                            <div id="toolbar">
                                <button class="btn btn-sm btn-success" id="generate_kwitansi"><i class="fa fa-print"></i> kwitansi</button>
                            </div>
                            <table data-toolbar="#toolbar" data-checkbox-header="false" data-id-field="kode" data-select-item-name="kode" id="tableabc" data-toggle="table" data-pagination="true" data-search="true" data-resizable="true">
                                <thead>
                                    <tr>
                                        <th data-checkbox="true"></th>
                                        <th data-field="kode">Kode</th>
                                        <th>Handling</th>
                                        <th>Owner</th>
                                        <th>Ikan</th>
                                        <th>Tagihan</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php $noa=1; @endphp
                                    @foreach($tagihan as $rn)
                                    <tr>
                                        <td></td>
                                        <td>{!! $rn->uniq_code !!}</td>
                                        <td>{{ $rn->namahandling }}<br> <small class='text-muted'>{{ $rn->kotahandling }}</small></td>
                                        <td>{{ $rn->namaowner }}<br> <small class='text-muted'>{{ $rn->kotaowner }}</small></td>
                                        <td class="text-center">{{ $rn->ikan }} ekor</td>
                                        <td class="text-right">{{ rupiah($rn->tagihan) }}</td>
                                        <td class="text-center">{!! anchor('pembayaran/detailTagihan?q='.urlencode($rn->uniq_code),'<i class="fa fa-binoculars"></i> Detail','class="btn btn-xs btn-primary"')  !!}</td>
                                    </tr>
                                    @php $noa++; @endphp
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="product-tab-list tab-pane fade @if($tab=='dua') active in @endif" id="reviews">
                    <div class="row">
                        <div class="col-md-12">
                            <div id="toolbarb">
                                <button class="btn btn-sm btn-success" id="pelunasan_masal"><i class="fa fa-check"></i> Pelunasan</button>
                            </div>
                            <table data-toolbar="#toolbarb" data-checkbox-header="false" data-id-field="id" data-select-item-name="id" id="tableabc" data-toggle="table" data-pagination="true" data-search="true" data-resizable="true">
                                <thead>
                                    <tr>
                                        <th data-checkbox="true"></th>
                                        <th data-field="id">No Kwitansi</th>
                                        <th>Tanggal</th>
                                        <th>Jumlah</th>
                                        <th>Lunas</th>
                                        <th>Keterangan</th>
                                        <th>Created By</th>
                                        <th>Cetak</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php $nob=1; @endphp
                                    @foreach($pembayaran as $rm)
                                    <tr>
                                        <td></td>
                                        <td class="text-center">{{ $rm->id }}</td>
                                        <td class="text-center">{{ date_indo($rm->tgl_kwitansi) }}</td>
                                        <td class="text-right">{{ rupiah($rm->jumlah) }}</td>
                                        <td class="text-center">@if($rm->lunas=='1') Lunas @else Belum @endif</td>
                                        <td>{{ $rm->keterangan }}</td>
                                        <td>{{ getNamaUser($rm->created_by) }}<br>
                                            <small class="text-muted">{{ $rm->created_at }}</small>
                                        </td>
                                        <td class="text-center">
                                            <a href="{{ base_url('pembayaran/print/'.$rm->id) }}" class="btn btn-xs btn-primary"><i class="fa fa-print"></i></a>
                                            <a href="{{ base_url('pembayaran/printlabel/'.$rm->id) }}" class="btn btn-xs btn-success"><i class="fa fa-file-text"></i></a>
                                        </td>
                                    </tr>
                                    @php $nob++; @endphp
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection
@section('css')
<link rel="stylesheet" href="{{ base_url('assets/') }}css/data-table/bootstrap-table.css">
<link rel="stylesheet" href="{{ base_url('assets/') }}css/data-table/bootstrap-editable.css">
@endsection
@section('script')
<!-- data table JS
		============================================ -->
<script src="{{ base_url('assets/') }}js/data-table/bootstrap-table.js"></script>
<script src="{{ base_url('assets/') }}js/data-table/tableExport.js"></script>
<script src="{{ base_url('assets/') }}js/data-table/data-table-active.js"></script>
<script src="{{ base_url('assets/') }}js/data-table/bootstrap-table-editable.js"></script>
<script src="{{ base_url('assets/') }}js/data-table/bootstrap-editable.js"></script>
<script src="{{ base_url('assets/') }}js/data-table/bootstrap-table-resizable.js"></script>
<script src="{{ base_url('assets/') }}js/data-table/colResizable-1.5.source.js"></script>
<script src="{{ base_url('assets/') }}js/data-table/bootstrap-table-export.js"></script>
<script>
    // pelunasan
    // 
    $(document).on('click', '#pelunasan_masal', function(e) {
        e.preventDefault();
        var val = [];
        $(':checkbox:checked').each(function(i) {
            val[i] = $(this).val();
        });

        $.get("{{ base_url('pembayaran/pelunasan') }}", {
                kwitansi: val
            },
            function(data) {
                window.location.replace("{{ base_url('pembayaran?tab=dua') }}");
            });
    });

    $(document).on('click', '#generate_kwitansi', function(e) {
        e.preventDefault();
        var val = [];
        $(':checkbox:checked').each(function(i) {
            val[i] = $(this).val();
        });
        valu = val.join();
        $.get("{{ base_url('pembayaran/generatekwitansi') }}", {
                uniq: val
            },
            function(data) {
                // alert(data);
                if (data == 0) {
                    location.reload(true);
                } else {
                    window.location.replace("{{ base_url('pembayaran/print/') }}" + data);
                }
            });
    });
</script>

@endsection