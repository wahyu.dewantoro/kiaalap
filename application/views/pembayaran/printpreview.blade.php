@extends('layouts.master')
@section('judul')
<h3>Print Preview <div class="pull-right"> {!! anchor($_SERVER['HTTP_REFERER'],'<i class="fa fa-angle-double-left"></i> kembali','class="btn btn-sm btn-success"') !!}</div>
</h3>
@endsection
@section('content')

<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div id="tutorial-pdf-responsive" class="tutorial-pdf-responsive custom2">
            <div class="custom2">
                <iframe src="{{ $url }}"></iframe>
            </div>
        </div>
    </div>
     
</div>
@endsection
@section('css')
<style>
    .custom1 {
        margin-left: auto !important;
        margin-right: auto !important;
        margin: 20px;
        padding: 10px;
    }

    .custom2 {
        position: relative;
        height: 0;
        overflow: hidden;
        padding-bottom: 90%;
    }

    .custom2 iframe {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        padding: 0;
        margin: 0;
    }

    .tutorial-pdf-responsive {
        max-width: 100%;
        max-height: auto;
        overflow: hidden;
    }
</style>
@endsection