<html>

<head>
    <style>
        @page {
            margin: 10px;
        }

        body {
            margin: 10px;
        }

        .center {
            margin-left: auto;
            margin-right: auto;
        }

        .responsive {
            width: auto;
            max-width: 110px;
            height: 150px;
        }
    </style>
    <title>Kartu Ikan</title>
</head>

<body>
    <?php foreach ($ikan as $ikan) { ?>
        <table align="center" style="table-layout: fixed; width: 227px; border: 1px dashed ;">
            <tr>
                <td align=center colspan="2">
                    <span style="font-size:70px;text-align:center"><?= $ikan->no_ikan ?></span><br>
                </td>
            </tr>
            <tr>
                <td>
                    <img class="responsive" src="<?= $ikan->gambar_ikan ?>">
                </td>
                <td style="vertical-align:top;padding: 10px;">
                    <span style="font-size:14px">
                        <?= $ikan->namavariety ?> <br> <?= $ikan->ukuran ?> cm<br>
                        <?= $gender[$ikan->gender] . ', ' . $breeder[$ikan->breeder] ?>
                    </span>
                </td>
            </tr>
        </table>
        <br>
    <?php } ?>
    <script type="text/javascript">
        try {
            this.print();
        } catch (e) {
            window.onload = window.print;
        }
    </script>
</body>

</html>