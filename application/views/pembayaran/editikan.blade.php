@extends('layouts.master')
@section('judul')
<h3>Detail Tagihan <div class="pull-right">

        {!! anchor('pembayaran','<i class="fa fa-angle-double-left"></i> kembali','class="btn btn-sm btn-primary"') !!} </div>
</h3>
@endsection
@section('content')
<div class="row">
    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
        <div class="profile-info-inner">
            <div class="profile-details-hr">
                <div class="row">
                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-6">
                        <div class="address-hr">
                            <p><b>Handling</b><br /> {{ $res->namahandling }}</p>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-6">
                        <div class="address-hr tb-sm-res-d-n dps-tb-ntn">
                            <p><b>Kota</b><br /> {{ $res->kotahandling}}</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-6">
                        <div class="address-hr">
                            <p><b>Owner</b><br /> {{ $res->namaowner }}</p>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-6">
                        <div class="address-hr tb-sm-res-d-n dps-tb-ntn">
                            <p><b>Kota</b><br /> {{ $res->kotaowner }}</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="address-hr">
                            <p><b>Kode</b><br /> <u>{{ $res->uniq_code }}</u></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
        <form action="{{ $action }}" method="post" enctype="multipart/form-data">
            <?= csrfToken() ?>
            <input type="hidden" name="id" value="{{  $res->id }}">
            <input type="hidden" name="id2" value="{{  $res->uniq_code }}">
            <div class="panel panel-primary">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 120px; height: 160px;">
                                    <img src="{{ base_url().$res->gambar_ikan }}" alt="Smiley face">
                                </div>
                                <small> <span class="fileinput-filename"></span></small>
                                <div>
                                    <span class="btn-file"> <input type="file" accept="image/*" name="gambar_ikan"> </span>
                                    <input type="hidden" name="gambar_ikan_lama" value="{{ $res->gambar_ikan }}">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <select name="variety_id" class="form-control" required>
                                    <option value="">Pilih variety</option>
                                    @foreach($variety as $vv)
                                    <option @if($vv->id==$res->variety_id) selected @endif value="{{$vv->id}}">{{ $vv->namavariety }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <input type="number" name="ukuran" value="{{ $res->ukuran }}" placeholder="Ukuran" class="form-control">
                            </div>
                            <div class="form-group">
                                <select name="gender" class="form-control" required>
                                    <option value="">Gender</option>
                                    @for($g=1;$g<=2;$g++) <option @if($res->gender==$g) selected @endif value="{{ $g }}">{{ $gender[$g] }}</option>
                                        @endfor
                                </select>
                            </div>
                            <div class="form-group">
                                <select name="breeder" class="form-control" required>
                                    <option value="">Breeder</option>
                                    @for($b=1;$b<=2;$b++) <option @if($res->breeder==$b) selected @endif value="{{ $b }}">{{ $breeder[$b] }}</option>
                                        @endfor
                                </select>
                            </div>
                            <div class="form-group">
                                <button class="btn btn-primary" type="submit"><i class="fa fa-save"></i> Simpan</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
@section('css')
<link href="<?= base_url() ?>assets/js/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
<style>
    .img-responsive {
        width: auto;
        height: 230px;
    }
</style>
@endsection
@section('script')
<script src="<?= base_url() ?>assets/js/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
@endsection