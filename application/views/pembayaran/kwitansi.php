<html>

<head>
    <style>
        @page {
            margin: 5px;
        }

        body {
            margin: 5px;
        }

        .center {
            margin-left: auto;
            margin-right: auto;
        }

        .responsive {
            width: auto;
            max-width:110px;
            height: 150px;
        }
    </style>
    <title>Kwitansi <?= $global->id ?> </title>
</head>

<body>
    <?php for ($a = 0; $a < 2; $a++) { ?>
        <table style="table-layout: fixed; width: 100%;  <?php if ($a == 1) { ?> page-break-before: always; <?php } ?>">
            <tr style="vertical-align:top;">
                <td>
                    <b><?= strtoupper($kontes->namakontes) ?></b><br>
                    <small><?= $kontes->tempat ?><br>
                        <?= $kontes->kota ?></small>
                </td>
                <td>
                    <b>INVOICE</b>
                    <table style="table-layout: fixed; width: 100%; font-size:12px">
                        <tr style="vertical-align:top;">
                            <td width="15%">Nomer</td>
                            <td>: <?= $global->id ?></td>
                        </tr>
                        <tr style="vertical-align:top;">
                            <td>Tanggal</td>
                            <td>: <?= date_indo($global->tgl_kwitansi) ?></td>
                        </tr>
                        <tr style="vertical-align:top;">
                            <td>Untuk</td>
                            <td>: <?= $global->keterangan ?></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <hr>
        <table style="table-layout: fixed; width: 100%; font-size:12px; border-style: dashed;  color: black; padding:5px">
            <thead style="border-bottom:thin solid gray;">
                <tr>
                    <th style="text-align:center" width='12%'>No</th>
                    <th style="text-align:center">Size</th>
                    <th style="text-align:center">Qty</th>
                    <th style="text-align:center">Harga</th>
                    <th style="text-align:center">Jumlah</th>
                </tr>
            </thead>
            <tbody style="border-bottom:thin solid gray;">
                <?php $no = 1;
                    foreach ($resume as $rn) { ?>
                    <tr>
                        <td style="text-align:center"><?= $no ?></td>
                        <td style="text-align:center"><?= $rn->size ?></td>
                        <td style="text-align:center"><?= $rn->jum ?></td>
                        <td style="text-align:right"><?= rupiah($rn->harga) ?></td>
                        <td style="text-align:right"><?= rupiah($rn->total) ?></td>
                    </tr>
                <?php $no++;
                    } ?>
            </tbody>
            <tfoot>
                <tr>
                    <td style="text-align:right" colspan="4"><b>Total</b> </td>
                    <td style="text-align:right"><?= rupiah($global->jumlah) ?></td>
                </tr>
                <tr>
                    <td style="text-align:right" colspan="5"><b><?= ucwords(terbilang($global->jumlah)) ?> Rupiah</b> </td>
                </tr>
            </tfoot>
        </table><br>
        <table style="table-layout: fixed; width: 100%; font-size:14px; padding:10px;">
            <tr valign="top">
                <td style="text-align:justify">
                    <p>Untuk pembayaran bisa di lakukan dengan transfer ke <?= $kontes->rekening ?>, dan melakukan konfirmasi ke sekretariat dengan membawa bukti transfer.</p>
                </td>
                <td style="text-align:center">
                    <!-- <b>Plastik</b> -->
                    <table style="table-layout: fixed; width: 100%; border-style: dashed;  color: gray; font-size:12px; padding:10px;">
                        <tr valign="top">
                            <th style="text-align:center"  colspan='2'>Plastik</th>
                        </tr>
                        <tr>
                            <th style="text-align:center" >Ukuran</th>
                            <th style="text-align:center" >Jumlah</th>
                        </tr>
                        <?php foreach ($plastik as $pl) { ?>
                            <tr>
                                <td style="text-align:center"><?= $pl->plastik ?></td>
                                <td style="text-align:center"><?= $pl->jumlah ?></td>
                            </tr>
                        <?php } ?>
                    </table>
                </td>
                <td style="text-align:center">
                    <?= $kontes->kota ?>, <?= date_indo($global->tgl_kwitansi) ?><br>
                    Panitia<br><br><br>Bendahara
                </td>
            </tr>
        </table>
    <?php } ?>

    <!-- detail invoice -->
    <table style="table-layout: fixed; width: 100%;   page-break-before: always;  ">
        <tr style="vertical-align:top;">
            <td>
                <b><?= strtoupper($kontes->namakontes) ?></b><br>
                <small><?= $kontes->tempat ?><br>
                    <?= $kontes->kota ?></small>
            </td>
            <td>
                <b>DETAIL INVOICE</b>
                <table style="table-layout: fixed; width: 100%; font-size:12px">
                    <tr style="vertical-align:top;">
                        <td width="15%">Nomer</td>
                        <td>: <?= $global->id ?></td>
                    </tr>
                    <tr style="vertical-align:top;">
                        <td>Tanggal</td>
                        <td>: <?= date_indo($global->tgl_kwitansi) ?></td>
                    </tr>

                </table>
            </td>
        </tr>
    </table>
    <hr>
    <?php
    $temp = "";
    foreach ($ikan as $ri) { ?>
        <span style="font-size:12px;">
            <?php if ($ri->uniq_code != $temp) { ?>
                <b>#<?= $ri->uniq_code ?></b>, <b>Owner</b>: <?= $ri->namaowner . ' (' . $ri->kotaowner . ')' ?>, <b>Handling</b>:<?= $ri->namahandling . ' (' . $ri->kotahandling . ')' ?> <br>
                <table style="table-layout: fixed; width: 100%; font-size:12px; border-style: dashed;  color: black; ">
                    <thead style="border-bottom:thin solid gray;">
                        <tr>
                            <th style="text-align:center" width='12%'>No</th>
                            <th style="text-align:center">ID</th>
                            <th style="text-align:center">Variety</th>
                            <th style="text-align:center">Size</th>
                            <th style="text-align:center">Harga</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $nn = 1;
                                foreach ($ikan as $rii) {
                                    if ($rii->uniq_code == $ri->uniq_code) { ?>
                                <tr>
                                    <td style="text-align:center" width='12%'><?= $nn ?></td>
                                    <td style="text-align:center"><?= $rii->no_ikan ?></td>
                                    <td style="text-align:center"><?= $rii->namavariety ?></td>
                                    <td style="text-align:center"><?= $rii->ukuran ?> cm</td>
                                    <td style="text-align:right"><?= rupiah($rii->biaya) ?></td>
                                </tr>
                        <?php $nn++;
                                    }
                                } ?>
                    </tbody>
                </table><br>
            <?php } ?>
        </span>
    <?php $temp = $ri->uniq_code;
    } ?>
    <table style="table-layout: fixed; width: 100%;   page-break-before: always;  ">
        <tr style="vertical-align:top;">
            <td>
                <b><?= strtoupper($kontes->namakontes) ?></b><br>
                <small><?= $kontes->tempat ?><br>
                    <?= $kontes->kota ?></small>
            </td>
            <td></td>
            <td>
                <b>DAFTAR IKAN</b>
                <table style="table-layout: fixed; width: 100%; font-size:12px">
                    <tr style="vertical-align:top;">
                        <td width="50%">Nomer Kwitansi</td>
                        <td>: <?= $global->id ?></td>
                    </tr>
                    <tr style="vertical-align:top;">
                        <td>Tanggal</td>
                        <td>: <?= date_indo($global->tgl_kwitansi) ?></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <hr>

    <?php
    $kolom  = 5;
    $gambar = array_chunk($ikan, $kolom);
    echo '<table style="table-layout: fixed; width: 100%;">';
    foreach ($gambar as $chunk) {
        echo '<tr >';
        foreach ($chunk as $ikane) {
            echo '<td valign="top" width="150px">'; ?>
            <table style=" font-size:12px">
                <tr>
                    <td valign="top"><img class="responsive" src="<?php echo $ikane->gambar_ikan ?>"></td>
                </tr>
                <tr>
                    <td>ID : <b><?php echo  $ikane->no_ikan ?></b></td>
                </tr>
                <tr>
                    <td valign="top"><?php echo $ikane->namavariety ?> / <?php echo $ikane->ukuran ?> cm </td>
                </tr>
                <tr>
                    <td valign="top"><?php echo $gender[$ikane->gender] ?> / <?php echo $breeder[$ikane->breeder] ?> </td>
                </tr>
            </table>
    <?php echo '</td>';
        }
        echo '</tr>';
    }
    echo '</table>';

    ?>
 <script type="text/javascript">
    try {
      this.print();
    } catch (e) {
      window.onload = window.print;
    }
  </script>
</body>

</html>