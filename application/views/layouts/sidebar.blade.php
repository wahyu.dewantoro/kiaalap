<div class="left-sidebar-pro">
    <nav id="sidebar" class="">
        <div class="sidebar-header">
            <a href="{{ base_url() }}"><img class="main-logo" src="{{ base_url() }}assets/img/logo/logo.png" alt="" /></a>
            <strong><a href="{{ base_url() }}"><img src="{{ base_url() }}assets/img/logo/logosn.png" alt="" /></a></strong>
        </div>
        <div class="left-custom-menu-adp-wrap comment-scrollbar">
            <nav class="sidebar-nav left-sidebar-menu-pro">
                <ul class="metismenu" id="menu1">
                    {!! showMenu() !!}
                </ul>
            </nav>
        </div>
    </nav>
</div>