@extends('layouts.master')
@section('judul')
<h3>Groups <div class="pull-right"> {!! anchor('pengguna/tambah','<i class="fa fa-plus"></i> Tambah','class="btn btn-custon-four btn-primary"') !!} </div>
</h3>
@endsection
@section('content')

<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="sparkline13-list">

            <div class="sparkline13-graph">
                <div class="datatable-dashv1-list custom-datatable-overright">
                    <table class="table table-striped table-border">
                        <thead>
                            <tr>
                                <th width='10px'>No</th>
                                <th>Nama</th>
                                <th>Email</th>
                                <th>Company</th>
                                <th>Phone</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($data as $data)
                            <tr>
                                <td>{{ ++$start }}</td>
                                <td>{{ $data->first_name }} {{ $data->last_name }}</td>
                                <td>{{ $data->email }}</td>
                                <td>{{ $data->company }}</td>
                                <td>{{ $data->phone }}</td>

                                <td align="center">
                                    <?= anchor('pengguna/edit/' . $data->id, '<i class="fa fa-edit"></i>', 'class="btn btn-xs btn-success"') ?>
                                    <?= anchor('pengguna/hapus/' . $data->id, '<i class="fa fa-trash"></i>', 'class="btn btn-xs btn-danger" onclick="return confirm(\'Apakah anda yakin?\')"') ?>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                            <button class="btn  btn-space btn-secondary" disabled>Total Record : {{ $total_rows }}</button>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                            <div class="pull-right"><?= $pagination ?></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection