@extends('layouts.master')

@section('content')

<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="product-payment-inner-st res-mg-t-30 analysis-progrebar-ctn">
            <ul id="myTabedu1" class="tab-review-design">
                <li @if($tab=='satu' ) class="active" @endif><a href="#description">Tagihan</a></li>
                
            </ul>
            <div id="myTabContent" class="tab-content custom-product-edit st-prf-pro">
                <div class="product-tab-list tab-pane fade @if($tab=='satu') active in @endif" id="description">
                    <div class="row">
                        <div class="col-md-12">
                            
                            <table data-toolbar="#toolbar" data-checkbox-header="false" data-id-field="kode" data-select-item-name="kode" id="tableabc" data-toggle="table" data-pagination="true" data-search="true" data-resizable="true">
                                <thead>
                                    <tr>
                                        
                                        <th data-field="kode">Kode</th>
                                        <th>Owner</th>
                                        <th>Ikan</th>
                                        <th>Tagihan</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    @php $noa=1; @endphp
                                    @foreach($tagihan as $rn)
                                    <tr>
                                        <td> {!! anchor('tagihan/detailTagihan?q='.urlencode($rn->uniq_code),$rn->uniq_code)  !!} </td>
                                        <td>{{ $rn->namaowner }}<br> <small class='text-muted'>{{ $rn->kotaowner }}</small></td>
                                        <td class="text-center">{{ $rn->ikan }} ekor</td>
                                        <td class="text-right">{{ rupiah($rn->tagihan) }}</td>
                                        
                                    </tr>
                                    @php $noa++; @endphp
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
             
            </div>
        </div>
    </div>
</div>
@endsection
@section('css')
<link rel="stylesheet" href="{{ base_url('assets/') }}css/data-table/bootstrap-table.css">
<link rel="stylesheet" href="{{ base_url('assets/') }}css/data-table/bootstrap-editable.css">
@endsection
@section('script')
<!-- data table JS
		============================================ -->
<script src="{{ base_url('assets/') }}js/data-table/bootstrap-table.js"></script>
<script src="{{ base_url('assets/') }}js/data-table/tableExport.js"></script>
<script src="{{ base_url('assets/') }}js/data-table/data-table-active.js"></script>
<script src="{{ base_url('assets/') }}js/data-table/bootstrap-table-editable.js"></script>
<script src="{{ base_url('assets/') }}js/data-table/bootstrap-editable.js"></script>
<script src="{{ base_url('assets/') }}js/data-table/bootstrap-table-resizable.js"></script>
<script src="{{ base_url('assets/') }}js/data-table/colResizable-1.5.source.js"></script>
<script src="{{ base_url('assets/') }}js/data-table/bootstrap-table-export.js"></script>
<script>
  
</script>

@endsection