<?php

use Jenssegers\Blade\Blade;

if (!function_exists('view')) {
    function view($view, $data = [])
    {
        $path = APPPATH . 'views';
        $blade = new Blade($path, $path . '/cache');
        echo $blade->make($view, $data);
    }
}


if (!function_exists('getUser')) {
    function getUser()
    {
        $CI = get_instance();
        $CI->load->library(array('ion_auth'));
        return $user = $CI->ion_auth->user()->row();
    }
}

if (!function_exists('showMenu')) {
    function showMenu()
    {
        $CI = get_instance();
        $CI->load->database();
        $CI->load->library(array('ion_auth'));
        $user = getUser();
        $user_groups = $CI->ion_auth->get_users_groups($user->id)->result();
        $group_id = '';
        foreach ($user_groups as $rg) {
            $group_id .= $rg->id . ',';
        }
        $group_id = substr($group_id, 0, -1);
        $qmenu0 = $CI->db->query("SELECT DISTINCT a.*
                                    FROM ms_menu a
                                    JOIN ms_privilege b ON a.id_inc=b.ms_menu_id
                                    WHERE group_id IN ($group_id)
                                    AND STATUS='1'  
                                    AND parent='0' 
                                    ORDER BY sort ASC")->result_array();
        $varmenu = '';
        foreach ($qmenu0 as $row0) {
            $parent  = $row0['id_inc'];
            $qmenu1 = $CI->db->query("SELECT distinct a.*
                                        FROM ms_menu a
                                        JOIN ms_privilege b ON a.id_inc=b.ms_menu_id
                                        WHERE group_id in ($group_id)
                                        AND STATUS='1'  
                                        AND parent='$parent' 
                                        ORDER BY sort ASC");
            $cekmenu = $qmenu1->num_rows();
            $dmenu1  = $qmenu1->result_array();
            if ($cekmenu > 0) {
                $varmenu .= "<li >
                      <a  href='" . $row0['link_menu'] . "'  class='has-arrow' > <span class='" . $row0['icon'] . "' aria-hidden='true'></span>  <span class='mini-click-non'>" . ucwords($row0['nama_menu']) . "</span> <span class='menu-arrow'></span></a>";
                $varmenu .= "<ul class='submenu-angle' aria-expanded='true'>";
                foreach ($dmenu1 as $row1) {
                    $qwa = explode('/', $row1['link_menu']);
                    $dda = strtolower($CI->uri->segment(1)) == strtolower($qwa[0]) ? 'active' : '';
                    $varmenu .= "<li class='" . $dda . "'>" . anchor(strtolower($row1['link_menu']), "<span class='mini-sub-pro'>" . ucwords($row1['nama_menu'] . "</span>")) . "</li>";
                }
                $varmenu .= "</ul>
                      </li>";
            } else {
                $qaw = explode('/', $row0['link_menu']);
                $dd = strtolower($CI->uri->segment(1)) == strtolower($qaw[0]) ? 'active' : '';
                $varmenu .= "<li class='" . $dd . "'>" . anchor(strtolower($row0['link_menu']), "<span class='" . $row0['icon'] . "'></span> <span class='mini-click-non'>" . ucwords($row0['nama_menu']) . '</span>', 'class="nav-link "') . "</li>";
            }
        }
        echo $varmenu;
    }
}

if (!function_exists('showMenuMobile')) {
    function showMenuMobile()
    {
        $CI = get_instance();
        $CI->load->database();
        $CI->load->library(array('ion_auth'));
        $user = getUser();
        $user_groups = $CI->ion_auth->get_users_groups($user->id)->result();
        $group_id = '';
        foreach ($user_groups as $rg) {
            $group_id .= $rg->id . ',';
        }
        $group_id = substr($group_id, 0, -1);
        $qmenu0 = $CI->db->query("SELECT DISTINCT a.*
                                    FROM ms_menu a
                                    JOIN ms_privilege b ON a.id_inc=b.ms_menu_id
                                    WHERE group_id IN ($group_id)
                                    AND STATUS='1'  
                                    AND parent='0' 
                                    ORDER BY sort ASC")->result_array();

        $varmenu = '';
        $nomer = 1;
        foreach ($qmenu0 as $row0) {
            $parent  = $row0['id_inc'];
            $qmenu1 = $CI->db->query("SELECT distinct a.*
            FROM ms_menu a
            JOIN ms_privilege b ON a.id_inc=b.ms_menu_id
            WHERE group_id in ($group_id)
            AND STATUS='1'  
            AND parent='$parent' 
            ORDER BY sort ASC");
            $cekmenu = $qmenu1->num_rows();
            $dmenu1  = $qmenu1->result_array();
            if ($cekmenu > 0) {
                $varmenu .= "<li>
                      <a data-toggle='collapse' data-target='#menu" . $nomer . "' href='#' > <span class='" . $row0['icon'] . "' aria-hidden='true'></span>  <span class='mini-click-non'>" . ucwords($row0['nama_menu']) . "</span> <span class='admin-project-icon edu-icon edu-down-arrow'></span></a>";
                $varmenu .= "<ul  id='menu" . $nomer . "' class='collapse dropdown-header-top'>";
                foreach ($dmenu1 as $row1) {
                    $qwa = explode('/', $row1['link_menu']);
                    $dda = strtolower($CI->uri->segment(1)) == strtolower($qwa[0]) ? 'active' : '';
                    $varmenu .= "<li class='" . $dda . "'>" . anchor(strtolower($row1['link_menu']), "<span class='mini-sub-pro'>" . ucwords($row1['nama_menu'] . "</span>")) . "</li>";
                }
                $varmenu .= "</ul>
                      </li>";
            } else {
                $qaw = explode('/', $row0['link_menu']);
                $dd = strtolower($CI->uri->segment(1)) == strtolower($qaw[0]) ? 'active' : '';
                $varmenu .= "<li class='" . $dd . "'>" . anchor(strtolower($row0['link_menu']), "<span class='" . $row0['icon'] . "'></span> <span class='mini-click-non'>" . ucwords($row0['nama_menu']) . '</span>', 'class="nav-link "') . "</li>";
            }
            $nomer++;
        }
        echo $varmenu;
    }
}

if (!function_exists('csrfToken')) {
    function csrfToken()
    {
        $CI = get_instance();
        return '<input type="hidden" name="' . $CI->security->get_csrf_token_name() . '" value="' . $CI->security->get_csrf_hash() . '" style="display: none">';
    }
}

if (!function_exists('rupiah')) {
    function rupiah($angka)
    {
        if (!empty($angka)) {
            return number_format($angka, 0, '', '.');
        } else {
            return 0;
        }
    }
}

if (!function_exists('dd')) {
    function dd($data)
    {
        echo "<pre>";
        print_r($data);
        echo "</pre>";
    }
}


if (!function_exists('getNamaUser')) {
    function getNamaUser($id)
    {
        if ($id <> '') {
            $CI = get_instance();
            $rw = $CI->db->query("SELECT CONCAT(first_name,' ',last_name)  nama FROM users WHERE id=$id")->row();
            if ($rw) {
                return $rw->nama;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }
}

if (!function_exists('ikanBySize')) {
    function ikanBySize($id, $min, $max)
    {
        if ($id <> '' || $min <> '' || $max <> '') {
            $CI = get_instance();
            $rw = $CI->db->query("
            SELECT COUNT(1) jum
            FROM ikandetail 
            WHERE deleted_at is null and deleted_by is null and variety_id=$id AND ukuran>=$min AND ukuran<=$max")->row();
            if ($rw) {
                return $rw->jum;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }
}

if (!function_exists('getKontes')) {
    function getKontes()
    {
        $CI = get_instance();
        return $CI->db->query("SELECT * FROM kontes")->row();
    }
}



if (!function_exists('gender')) {
    function gender()
    {
        return array(1 => 'Female', 'Male');
    }
}

if (!function_exists('breeder')) {
    function breeder()
    {
        return array(1 => 'Lokal', 'Impor');
    }
}



//Long date indo Format
if (!function_exists('longdate_indo')) {
    function longdate_indo($tanggal)
    {
        $ubah = gmdate($tanggal, time() + 60 * 60 * 8);
        $pecah = explode("-", $ubah);
        $tgl = $pecah[2];
        $bln = $pecah[1];
        $thn = $pecah[0];
        $bulan = bulan($pecah[1]);

        $nama = date("l", mktime(0, 0, 0, $bln, $tgl, $thn));
        $nama_hari = "";
        if ($nama == "Sunday") {
            $nama_hari = "Minggu";
        } else if ($nama == "Monday") {
            $nama_hari = "Senin";
        } else if ($nama == "Tuesday") {
            $nama_hari = "Selasa";
        } else if ($nama == "Wednesday") {
            $nama_hari = "Rabu";
        } else if ($nama == "Thursday") {
            $nama_hari = "Kamis";
        } else if ($nama == "Friday") {
            $nama_hari = "Jumat";
        } else if ($nama == "Saturday") {
            $nama_hari = "Sabtu";
        }
        return $nama_hari . ',' . $tgl . ' ' . $bulan . ' ' . $thn;
    }
}

if (!function_exists('bulan')) {
    function bulan($bln)
    {
        switch ($bln) {
            case 1:
                return "Januari";
                break;
            case 2:
                return "Februari";
                break;
            case 3:
                return "Maret";
                break;
            case 4:
                return "April";
                break;
            case 5:
                return "Mei";
                break;
            case 6:
                return "Juni";
                break;
            case 7:
                return "Juli";
                break;
            case 8:
                return "Agustus";
                break;
            case 9:
                return "September";
                break;
            case 10:
                return "Oktober";
                break;
            case 11:
                return "November";
                break;
            case 12:
                return "Desember";
                break;
        }
    }
}

if (!function_exists('date_indo')) {
    function date_indo($tgl)
    {
        if (!empty($tgl)) {
            $ubah    = gmdate($tgl, time() + 60 * 60 * 8);
            $pecah   = explode("-", $ubah);
            $tanggal = $pecah[2];
            $bulan   = bulan($pecah[1]);
            $tahun   = $pecah[0];
            return $tanggal . ' ' . $bulan . ' ' . $tahun;
        } else {
            return null;
        }
    }
}


if (!function_exists('terbilang')) {
    function terbilang($x)
    {
        $angka = ["", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas"];
        if ($x < 12)
            return " " . $angka[$x];
        elseif ($x < 20)
            return terbilang($x - 10) . " belas";
        elseif ($x < 100)
            return terbilang($x / 10) . " puluh" . terbilang($x % 10);
        elseif ($x < 200)
            return " seratus" . terbilang($x - 100);
        elseif ($x < 1000)
            return terbilang($x / 100) . " ratus" . terbilang($x % 100);
        elseif ($x < 2000)
            return " seribu" . terbilang($x - 1000);
        elseif ($x < 1000000)
            return terbilang($x / 1000) . " ribu" . terbilang($x % 1000);
        elseif ($x < 1000000000)
            return terbilang($x / 1000000) . " juta" . terbilang($x % 1000000);
    }
}


if (!function_exists('kodeurut')) {
    function kodeurut()
    {
        $ci = get_instance();
        $row = $ci->db->query("SELECT CAST(MAX(digits(uniq_code)) AS INT) lastid
                                    FROM (
                                            SELECT uniq_code
                                            FROM cartheader 
                                            UNION 
                                            SELECT uniq_code FROM ikanheader
                                            ) qw ")->row();

        if ($row) {
            $kode = (int) $row->lastid;

            $kode = $kode + 1;
            $auto_kode = "A" . str_pad($kode, 4, "0",  STR_PAD_LEFT);
        } else {
            $auto_kode = "A0001";
        }

        return $auto_kode;
    }
}
