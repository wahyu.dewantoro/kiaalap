<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Penjurian extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model(array('Mpenjurian', 'Mukuran', 'Mvariety', 'Mpeserta'));
        $this->load->library('form_validation');
    }

    public function kategori()
    {
        $vu = $this->input->get('ukuran', true);
        $vv = $this->input->get('variety', true);

        $ikan = [];
        if ($vv <> '' && $vu <> '') {
            $ikan = $this->Mpenjurian->getListIkan($vu, $vv);
        }

        $data = array(
            'ukuran' => $this->Mukuran->get()->result(),
            'variety' => $this->Mvariety->get()->result(),
            'vu' => $vu,
            'vv' => $vv,
            'ikan' => $ikan,
            'breeder' => breeder(),
            'gender' => gender(),
            'noikan' => $this->Mpenjurian->getListNoIkan()
        );
        return view('penjurian.kategori', $data);
    }

    function choserank()
    {
        // dd($this->input->get());
        $id = $this->input->get('id', true);
        $ukuran = $this->input->get('ukuran', true);
        $variety = $this->input->get('variety', true);

        $a = explode('_', $ukuran);

        $min = $a[0];
        $max = $a[1];
        // // cek slot juara
        $cc = $this->db->query("SELECT MIN(id)  juara_id,aliasjuara FROM (
                                    SELECT id,aliasjuara
                                    FROM juara WHERE id IN (1,2,3,4,5)
                                ) asd WHERE id 
                                NOT IN ( 
                                    SELECT juara_id
                                    FROM juara_kontes a
                                    JOIN ikandetail b ON a.ikandetail_id=b.id
                                WHERE variety_id=$variety AND ukuran BETWEEN '$min' AND '$max')")->row();

        // cek ikan 
        $rr = $this->db->query("SELECT aliasjuara
                                FROM juara_kontes  a
                                JOIN juara c ON c.id=a.juara_id
                                WHERE ikandetail_id=$id")->row();

        // exit;
        if ($cc->juara_id != '') {
            // cek id ikan s
            if (count($rr) == 0) {
                $this->db->set('juara_id', $cc->juara_id);
                $this->db->set('ikandetail_id', $id);
                $this->db->insert('juara_kontes');
            }
        }
    }

    function listrank()
    {
        $ukuran = $this->input->get('ukuran', true);
        $variety = $this->input->get('variety', true);
        $a = explode('_', $ukuran);
        $min = $a[0];
        $max = $a[1];

        $data = $this->db->query("SELECT b.id,c.aliasjuara
        FROM juara_kontes a
        JOIN ikandetail b ON a.ikandetail_id=b.id
        JOIN juara c ON c.id=a.juara_id
        WHERE variety_id=$variety AND ukuran BETWEEN '$min' AND '$max'")->result();

        $html = "";

        if ($data) {
            foreach ($data as $rk) {
                $html .= '<b>' . $rk->aliasjuara . '</b>: ' . $rk->id . ', ';
            }

            $html = substr($html, 0, -2) . ' <a href="' . base_url('penjurian/resetrank?ukuran=' . $ukuran . '&variety=' . $variety) . '" class="btn btn-sm btn-danger" id="reset">Reset</a>';
        }
        echo $html;
    }

    function resetrank()
    {
        $ukuran = $this->input->get('ukuran', true);
        $variety = $this->input->get('variety', true);
        $a = explode('_', $ukuran);
        $min = $a[0];
        $max = $a[1];

        $this->db->query("DELETE a FROM juara_kontes a
                                            JOIN ikandetail b ON a.ikandetail_id=b.id
                                            JOIN juara c ON c.id=a.juara_id
                                            WHERE variety_id=$variety AND ukuran BETWEEN '$min' AND '$max'");
        redirect($_SERVER['HTTP_REFERER']);
    }

    function bischampion()
    {
        $vk = 'A';
        $vc = '8';
        $kelas = $this->db->query("SELECT DISTINCT kelas FROM variety")->result();
        $champ = $this->db->query("SELECT * FROM championref WHERE id_juara=7 order by urut asc")->result();
        return view('penjurian.bischamp', ['kelas' => $kelas, 'champ' => $champ, 'vk' => $vk, 'vc' => $vc]);
    }

    function cariikan()
    {
        $id = $this->input->get('id', true);
        $this->db->where('ikandetail.id', $id);
        $rk = $this->Mpeserta->getListIkan();
        // dd($rk);
        if ($rk) {
            foreach ($rk as $rk) {
                $data['no_ikan'] = $rk->no_ikan;
                $data['namavariety'] = $rk->namavariety;
                $data['ukuran'] = $rk->ukuran;
                $data['gender'] = $rk->gender;
                $data['breeder'] = $rk->breeder;
                $data['gambar_ikan'] = $rk->gambar_ikan;
                $data['namahandling'] = $rk->namahandling;
                $data['kotahandling'] = $rk->kotahandling;
                $data['namaowner'] = $rk->namaowner;
                $data['kotaowner'] = $rk->kotaowner;
                $data['br'] = breeder();
                $data['gd'] = gender();
            }

            // dd($data);
            return view('penjurian.detailikan', $data);
        } else {
            echo "Data kosong";
        }
    }

    function editikan($id)
    {
        $this->load->model('Mvariety');
        $this->db->where('id', $id);
        $this->db->where('deleted_by is null', '', false);
        $cek = $this->db->get('ikandetail')->row();
        if ($cek) {
            $data['res'] = $cek;
            $data['gender'] = gender();
            $data['breeder'] = breeder();
            $data['action'] = base_url() . 'penjurian/prosesedit';
            $data['variety'] = $this->Mvariety->get()->result();
            return view('peserta.editikan', $data);
        } else {
            redirect($_SERVER['HTTP_REFERER']);
        }
    }

    function prosesedit()
    {
        if (empty($_FILES['gambar_ikan']['name'])) {
            $gambar = $this->input->post('gambar_ikan_lama');
        } else {
            @unlink($this->input->post('gambar_ikan_lama'));

            $_FILES['gbr_ikan']['name']     = $_FILES['gambar_ikan']['name'];
            $_FILES['gbr_ikan']['type']     = $_FILES['gambar_ikan']['type'];
            $_FILES['gbr_ikan']['tmp_name'] = $_FILES['gambar_ikan']['tmp_name'];
            $_FILES['gbr_ikan']['error']     = $_FILES['gambar_ikan']['error'];
            $_FILES['gbr_ikan']['size']     = $_FILES['gambar_ikan']['size'];

            // proses upload gambar 
            $config['upload_path']   = 'ikan/';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['encrypt_name']  = TRUE;
            $this->load->library('upload', $config);
            if ($this->upload->do_upload('gbr_ikan')) {
                $gbr                      = $this->upload->data();
                $config['image_library']  = 'gd2';
                $config['source_image']   = $config['upload_path'] . $gbr['file_name'];
                $config['create_thumb']   = FALSE;
                $config['maintain_ratio'] = TRUE;
                $config['quality']        = '70%';
                $config['new_image']      = 'ikan/' . $gbr['file_name'];
                $this->load->library('image_lib', $config);
                $this->image_lib->resize();
                $gambar = $config['new_image'];
            }
        }

        $detail['variety_id'] = $this->input->post('variety_id', true);
        $detail['ukuran'] = $this->input->post('ukuran', true);
        $detail['gender'] = $this->input->post('gender', true);
        $detail['breeder'] = $this->input->post('breeder', true);
        $detail['gambar_ikan'] = $gambar;
        $detail['updated_by'] = getUser()->user_id;
        $detail['updated_at'] = date('Y-m-d H:i:s');
        $this->db->where('id', $this->input->post('id'));
        $this->db->update('ikandetail', $detail);

        $kodeu = $this->input->post('id2');
        $this->db->query("UPDATE kwitansiheader
                            JOIN (
                            SELECT a.uniq_code,kwitansi_id,SUM(biaya(ukuran)) jumlah FROM ikandetail a
                            JOIN kwitansidetail b ON a.`uniq_code`=b.uniq_code
                            WHERE  a.uniq_code ='$kodeu' AND deleted_by IS NULL
                            GROUP BY a.uniq_code,kwitansi_id
                            ) b ON b.kwitansi_id=kwitansiheader.`id`
                            SET 
                            kwitansiheader.jumlah=b.jumlah");
        redirect('penjurian/kategori');
    }

    function getdataformbis()
    {
        $vk = $this->input->get('kelas');
        $vc = $this->input->get('champ');

        $cham = $this->db->query("SELECT  * FROM championref WHERE id=$vc")->row();
        
        $rank = $this->db->query("SELECT a.id,a.id no_ikan,namavariety,ukuran
        FROM juara_kontes a
        JOIN ikandetail b ON a.ikandetail_id=b.id
        JOIN variety c ON c.id=b.variety_id
        WHERE ukuran >='" . $cham->vmin . "' AND ukuran<='" . $cham->vmax . "' AND kelas='$vk' AND juara_id='1'")->result();

        $rankb='';
        $data = array(
            'title' => 'BIS Kelas ' . $this->input->get('kelas') . ' / ' . $cham->alias,
            'rank'=>$rank,
            'rankb'=>$rankb
        );
        return view('penjurian/dataFormBisChamp', $data);
    }
}
