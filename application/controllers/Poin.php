<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Poin extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model(array('Mpoin', 'Mjuara'));
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));

        if ($q <> '') {
            $config['base_url'] = base_url() . 'poin?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'poin?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'poin';
            $config['first_url'] = base_url() . 'poin';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Mpoin->totalRows($q);
        $poin = $this->Mpoin->getLimitData($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'poin_data' => $poin,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );

        return view('poin/Poin_list', $data);
    }



    public function create()
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('poin/create_action'),
            'id' => set_value('id'),
            'ukuranmin' => set_value('ukuranmin'),
            'ukuranmax' => set_value('ukuranmax'),
            'juara_id' => set_value('juara_id'),
            'poin' => set_value('poin'),
            'juara' => $this->Mjuara->get()->result()
        );
        return view('poin/Poin_form', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
                'ukuranmin' => $this->input->post('ukuranmin', TRUE),
                'ukuranmax' => $this->input->post('ukuranmax', TRUE),
                'juara_id' => $this->input->post('juara_id', TRUE),
                'poin' => $this->input->post('poin', TRUE),
            );

            $this->Mpoin->insertData($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('poin'));
        }
    }

    public function update($id)
    {
        $row = $this->Mpoin->first('poinmaster.id', $id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('poin/update_action'),
                'id' => set_value('id', $row->id),
                'ukuranmin' => set_value('ukuranmin', $row->ukuranmin),
                'ukuranmax' => set_value('ukuranmax', $row->ukuranmax),
                'juara_id' => set_value('juara_id', $row->juara_id),
                'poin' => set_value('poin', $row->poin),
                'juara' => $this->Mjuara->get()->result()
            );
            return view('poin/Poin_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('poin'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
                'ukuranmin' => $this->input->post('ukuranmin', TRUE),
                'ukuranmax' => $this->input->post('ukuranmax', TRUE),
                'juara_id' => $this->input->post('juara_id', TRUE),
                'poin' => $this->input->post('poin', TRUE),
            );

            $this->Mpoin->updateData($data, $this->input->post('id', TRUE));
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('poin'));
        }
    }

    public function delete($id)
    {
        $row = $this->Mpoin->first('id', $id);

        if ($row) {
            $this->Mpoin->deleteData($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('poin'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('poin'));
        }
    }

    public function _rules()
    {
        $this->form_validation->set_rules('ukuranmin', 'ukuranmin', 'trim|required');
        $this->form_validation->set_rules('ukuranmax', 'ukuranmax', 'trim|required');
        $this->form_validation->set_rules('juara_id', 'juara id', 'trim|required');
        $this->form_validation->set_rules('poin', 'poin', 'trim|required');

        $this->form_validation->set_rules('id', 'id', 'trim');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }
}

/* End of file Poin.php */
/* Location: ./application/controllers/Poin.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2019-08-28 02:51:11 */
/* http://harviacode.com */
