<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Peserta extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Mpeserta');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));

        if ($q <> '') {
            $config['base_url'] = base_url() . 'peserta?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'peserta?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'peserta';
            $config['first_url'] = base_url() . 'peserta';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Mpeserta->totalRows($q);
        $peserta = $this->Mpeserta->getLimitData($config['per_page'], $start, $q);
        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'peserta_data' => $peserta,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );

        return view('peserta/Peserta_list', $data);
    }



    public function update($id)
    {
        $row = $this->Mpeserta->first('id', $id);
        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('peserta/update_action'),
                'id' => set_value('id', $row->id),
                'uniq_code' => set_value('uniq_code', $row->uniq_code),
                'namahandling' => set_value('namahandling', $row->namahandling),
                'kotahandling' => set_value('kotahandling', $row->kotahandling),
                'namaowner' => set_value('namaowner', $row->namaowner),
                'kotaowner' => set_value('kotaowner', $row->kotaowner),
            );
            return view('peserta/Peserta_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('peserta'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {

            $this->db->trans_start();
            $data = array(
                'namahandling' => $this->input->post('namahandling', TRUE),
                'kotahandling' => $this->input->post('kotahandling', TRUE),
                'namaowner' => $this->input->post('namaowner', TRUE),
                'kotaowner' => $this->input->post('kotaowner', TRUE),
                'updated_at' => date('Y-m-d H:i:s'),
                'updated_by' =>  getUser()->user_id
            );
            $this->Mpeserta->updateData($data, $this->input->post('id', TRUE));
            $this->db->where('uniq_code', $this->input->post('uniq_code', TRUE));
            $this->db->update('ikandetail', $data);
            $this->db->trans_complete();

            if ($this->db->trans_status() === FALSE) {
                $this->session->set_flashdata('message', 'Update Record failed');
            } else {
                $this->session->set_flashdata('message', 'Update Record Success');
            }
            redirect(site_url('peserta'));
        }
    }

    public function delete($id)
    {
        $row = $this->Mpeserta->first('id', $id);

        if ($row) {
            $data = array('deleted_by' => getUser()->user_id, 'deleted_at' => date('Y-m-d H:i:s'));
            $this->Mpeserta->updateData($data, $id);

            $this->db->where("uniq_code in (select uniq_code from ikanheader where id='$id') and deleted_by is null");
            $this->db->update('ikandetail', $data);

            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('peserta'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('peserta'));
        }
    }

    public function _rules()
    {
        $this->form_validation->set_rules('namahandling', 'Nama handaling', 'trim|required');
        $this->form_validation->set_rules('kotahandling', 'Kota handling', 'trim|required');
        $this->form_validation->set_rules('namaowner', 'Nama Owner', 'trim|required');
        $this->form_validation->set_rules('kotaowner', 'Kota Owner', 'trim|required');
        $this->form_validation->set_rules('id', 'id', 'trim');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function read($id)
    {
        $row = $this->Mpeserta->first('uniq_code', $id);
        if ($row) {
            $cb = $this->db->query("SELECT COUNT(1) res FROM kwitansidetail WHERE uniq_code='$id'")->row();

            $detail = $this->Mpeserta->getDetailPeserta($row->uniq_code);
            $data = array(
                'header' => $row,
                'detail' => $detail,
                'gender' => gender(),
                'breeder' => breeder(),
                'cb' => $cb->res
            );
            return view('peserta/read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('peserta'));
        }
    }

    function printdoorprize()
    {
        $data = array(
            'url' => base_url('peserta/doorprize/')
        );
        return view('pembayaran.printpreview', $data);
    }

    public function doorprize()
    {
        $res = $this->Mpeserta->getDoorprize();
        $data = array('data' => $res);
        $this->load->library('pdf');
        $this->pdf->setPaper('A4', 'potrait');;
        $this->pdf->filename = "doorprize.pdf";
        $this->pdf->load_view('peserta/doorprize_view', $data);
    }

    function editikan($id)
    {
        $this->load->model('Mvariety');
        $this->db->where('id', $id);
        $this->db->where('deleted_by is null', '', false);
        $cek = $this->db->get('ikandetail')->row();

        if ($cek) {
            $data['res'] = $cek;
            $data['gender'] = gender();
            $data['breeder'] = breeder();
            $data['action'] = base_url() . 'peserta/prosesedit';
            $data['variety'] = $this->Mvariety->get()->result();
            return view('peserta.editikan', $data);
        } else {
            redirect($_SERVER['HTTP_REFERER']);
        }
    }

    function prosesedit()
    {
        if (empty($_FILES['gambar_ikan']['name'])) {
            $gambar = $this->input->post('gambar_ikan_lama');
        } else {
            @unlink($this->input->post('gambar_ikan_lama'));

            $_FILES['gbr_ikan']['name']     = $_FILES['gambar_ikan']['name'];
            $_FILES['gbr_ikan']['type']     = $_FILES['gambar_ikan']['type'];
            $_FILES['gbr_ikan']['tmp_name'] = $_FILES['gambar_ikan']['tmp_name'];
            $_FILES['gbr_ikan']['error']     = $_FILES['gambar_ikan']['error'];
            $_FILES['gbr_ikan']['size']     = $_FILES['gambar_ikan']['size'];

            // proses upload gambar 
            $config['upload_path']   = 'ikan/';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['encrypt_name']  = TRUE;
            $this->load->library('upload', $config);
            if ($this->upload->do_upload('gbr_ikan')) {
                $gbr                      = $this->upload->data();
                $config['image_library']  = 'gd2';
                $config['source_image']   = $config['upload_path'] . $gbr['file_name'];
                $config['create_thumb']   = FALSE;
                $config['maintain_ratio'] = TRUE;
                $config['quality']        = '70%';
                $config['new_image']      = 'ikan/' . $gbr['file_name'];
                $this->load->library('image_lib', $config);
                $this->image_lib->resize();
                $gambar = $config['new_image'];
            }
        }

        $detail['variety_id'] = $this->input->post('variety_id', true);
        $detail['ukuran'] = $this->input->post('ukuran', true);
        $detail['gender'] = $this->input->post('gender', true);
        $detail['breeder'] = $this->input->post('breeder', true);
        $detail['gambar_ikan'] = $gambar;
        $detail['updated_by'] = getUser()->user_id;
        $detail['updated_at'] = date('Y-m-d H:i:s');
        $this->db->where('id', $this->input->post('id'));
        $this->db->update('ikandetail', $detail);

        $kodeu = $this->input->post('id2');
        $this->db->query("UPDATE kwitansiheader
        JOIN (
        SELECT a.uniq_code,kwitansi_id,SUM(biaya(ukuran)) jumlah FROM ikandetail a
        JOIN kwitansidetail b ON a.`uniq_code`=b.uniq_code
        WHERE  a.uniq_code ='$kodeu' AND deleted_by IS NULL
        GROUP BY a.uniq_code,kwitansi_id
        ) b ON b.kwitansi_id=kwitansiheader.`id`
        SET 
        kwitansiheader.jumlah=b.jumlah");
        redirect('peserta/read/' . $kodeu);
    }

    function hapusikan($id)
    {
        $this->db->where('id', $id);
        $this->db->where('deleted_by is null', '', false);
        $cek = $this->db->get('ikandetail')->row();
        if ($cek) {
            $data = array('deleted_by' => getUser()->user_id, 'deleted_at' => date('Y-m-d H:i:s'));
            $this->db->where('id', $id);
            $this->db->where('deleted_by is null', '', false);
            $this->db->update('ikandetail', $data);
            $kodeu = $cek->uniq_code;
            $this->db->query("UPDATE kwitansiheader
                                JOIN (
                                SELECT a.uniq_code,kwitansi_id,SUM(biaya(ukuran)) jumlah FROM ikandetail a
                                JOIN kwitansidetail b ON a.`uniq_code`=b.uniq_code
                                WHERE  a.uniq_code ='$kodeu' AND deleted_by IS NULL
                                GROUP BY a.uniq_code,kwitansi_id
                                ) b ON b.kwitansi_id=kwitansiheader.`id`
                                SET 
                                kwitansiheader.jumlah=b.jumlah");
        }
        redirect($_SERVER['HTTP_REFERER']);
    }

    function label($id)
    {
        $data = array(
            'url' => base_url('peserta/getLabel/' . $id)
        );
        return view('pembayaran.printpreview', $data);
    }


    function getLabel($id)
    {
        $ikan = $this->db->query("SELECT  ikanheader.id,ikandetail.uniq_code,ikandetail.namahandling,ikandetail.kotahandling,ikandetail.namaowner,ikandetail.kotaowner,ikandetail.id no_ikan,namavariety,ukuran,ukuranalias(ukuran) kategori,biaya(ukuran) biaya,gambar_ikan,gender,breeder
                                FROM ikandetail 
                                JOIN ikanheader ON ikandetail.uniq_code=ikanheader.uniq_code
                                JOIN variety ON variety.id=ikandetail.`variety_id`
                                WHERE ikanheader.uniq_code='$id' and ikandetail.deleted_by is null
                                ORDER BY ikanheader.id ASC,uniq_code ASC,ikandetail.id ASC")->result();
        $res = array('ikan' => $ikan, 'breeder' => breeder(), 'gender' => gender());
        $this->load->library('pdf');
        $this->pdf->setPaper('A4', 'potrait');;
        $this->pdf->filename = "laporan-petanikode.pdf";
        $this->pdf->load_view('pembayaran/kartuikan', $res);
    }


    function ikan()
    {
        $user = getUser();
        $user_groups = $this->ion_auth->get_users_groups($user->id)->result();
        $group_id = [];
        foreach ($user_groups as $rg) {
            $group_id[] = $rg->id;
        }
        $handling = false;
        if (in_array('5', $group_id)) {
            $handling = true;
        }

        $ikan = $this->Mpeserta->getListIkan();
        return view('peserta.dataikan', ['ikan' => $ikan, 'breeder' => breeder(), 'gender' => gender(),'is_handling'=>$handling]);
    }

    function fotoikan()
    {
        
        $variety = $this->db->query("SELECT DISTINCT kelas FROM variety ORDER BY sort ASC")->result();
        $ukuran=$this->db->query("SELECT * FROM ukuran  ORDER BY id ASC")->result();
        $data=array(
            'variety'=>$variety,
            'ukuran'=>$ukuran
        );
        $this->load->library('pdf');
        $this->pdf->setPaper('A4', 'potrait');;
        $this->pdf->filename = "dataikan.pdf";
        $this->pdf->load_view('peserta/fotoikan', $data);
    }

    function dataikan(){
        $variety = $this->db->query("SELECT DISTINCT kelas FROM variety ORDER BY sort ASC")->result();
        $ukuran=$this->db->query("SELECT * FROM ukuran  ORDER BY id ASC")->result();
        $data=array(
            'variety'=>$variety,
            'ukuran'=>$ukuran,
            'gender'=>gender(),
            'breeder'=>breeder()
        );
        $this->load->library('pdf');
        $this->pdf->setPaper('A4', 'potrait');;
        $this->pdf->filename = "dataikan.pdf";
        $this->pdf->load_view('peserta/cetakdataikan', $data);
    }
}

/* End of file Peserta.php */
/* Location: ./application/controllers/Peserta.php */
