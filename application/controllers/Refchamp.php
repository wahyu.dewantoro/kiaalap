<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Refchamp extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model(array('Mchamp', 'Mjuara'));
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));

        if ($q <> '') {
            $config['base_url'] = base_url() . 'refchamp?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'refchamp?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'refchamp';
            $config['first_url'] = base_url() . 'refchamp';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Mchamp->totalRows($q);
        $refchamp = $this->Mchamp->getLimitData($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'refchamp_data' => $refchamp,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );

        return view('refchamp/Refchamp_list', $data);
    }



    public function create()
    {
        $this->db->order_by('id', 'asc');
        $this->db->where("id IN (7,8)", '', false);
        $juara = $this->Mjuara->get()->result();
        $data = array(
            'button' => 'Create',
            'action' => site_url('refchamp/create_action'),
            'id' => set_value('id'),
            'vmin' => set_value('vmin'),
            'vmax' => set_value('vmax'),
            'alias' => set_value('alias'),
            'urut' => set_value('urut'),
            'id_juara' => set_value('id_juara'),
            'juara' => $juara
        );

        return view('refchamp/Refchamp_form', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
                'vmin' => $this->input->post('vmin', TRUE),
                'vmax' => $this->input->post('vmax', TRUE),
                'alias' => $this->input->post('alias', TRUE),
                'urut' => $this->input->post('urut', TRUE),
                'id_juara' => $this->input->post('id_juara', TRUE),
            );

            $this->Mchamp->insertData($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('refchamp'));
        }
    }

    public function update($id)
    {
        $row = $this->Mchamp->first('id', $id);
        if ($row) {

            $this->db->order_by('id', 'asc');
            $this->db->where("id IN (7,8)", '', false);
            $juara = $this->Mjuara->get()->result();
            $data = array(
                'button' => 'Update',
                'action' => site_url('refchamp/update_action'),
                'id' => set_value('id', $row->id),
                'vmin' => set_value('vmin', $row->vmin),
                'vmax' => set_value('vmax', $row->vmax),
                'alias' => set_value('alias', $row->alias),
                'urut' => set_value('urut', $row->urut),
                'id_juara' => set_value('id_juara', $row->id_juara),
                'juara'=>$juara
            );
            return view('refchamp/Refchamp_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('refchamp'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
                'vmin' => $this->input->post('vmin', TRUE),
                'vmax' => $this->input->post('vmax', TRUE),
                'alias' => $this->input->post('alias', TRUE),
                'urut' => $this->input->post('urut', TRUE),
                'id_juara' => $this->input->post('id_juara', TRUE),
            );

            $this->Mchamp->updateData($data, $this->input->post('id', TRUE));
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('refchamp'));
        }
    }

    public function delete($id)
    {
        $row = $this->Mchamp->first('id', $id);

        if ($row) {
            $this->Mchamp->deleteData($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('refchamp'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('refchamp'));
        }
    }

    public function _rules()
    {
        $this->form_validation->set_rules('vmin', 'vmin', 'trim|required');
        $this->form_validation->set_rules('vmax', 'vmax', 'trim|required');
        $this->form_validation->set_rules('alias', 'alias', 'trim|required');
        $this->form_validation->set_rules('urut', 'urut', 'trim|required');
        $this->form_validation->set_rules('id_juara', 'id juara', 'trim|required');

        $this->form_validation->set_rules('id', 'id', 'trim');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }
}

/* End of file Refchamp.php */
/* Location: ./application/controllers/Refchamp.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2019-09-22 08:38:13 */
/* http://harviacode.com */
