<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Pembayaran extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Mpembayaran');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $tab = $this->input->get('tab') <> '' ? $this->input->get('tab') : 'satu';
        $data = array(
            'tagihan' => $this->Mpembayaran->getTagihan(),
            'pembayaran' => $this->Mpembayaran->getPembayaran(),
            'tab' => $tab
        );
        return view('pembayaran/index', $data);
    }

    public function generatekwitansi()
    {
        $uniq = $this->input->get('uniq', true);
        $var = "";
        foreach ($uniq as $uniq) {
            $var .= "'" . $uniq . "',";
        }
        $var = substr($var, 0, -1);
        // proses 
        $res = $this->Mpembayaran->prosesKwitansi($var);
        echo $res;
    }

    function print($id)
    {
        $data = array(
            'url' => base_url('pembayaran/getkwitansi/' . $id),
        );
        return view('pembayaran.printpreview', $data);
    }

    function printlabel($id)
    {
        $data = array(
            'url' => base_url('pembayaran/getLabel/' . $id)
        );
        return view('pembayaran.printpreview', $data);
    }

    function getkwitansi($id)
    {
        $data = $this->Mpembayaran->getKwitansi($id);

        $this->load->library('pdf');
        $this->pdf->setPaper('A4', 'potrait');
        $this->pdf->filename = "laporan-petanikode.pdf";
        $this->pdf->load_view('pembayaran/kwitansi', $data);
    }

    function getLabel($id)
    {
        
        $data = $this->Mpembayaran->getKwitansi($id);
        $res = array('ikan' => $data['ikan'], 'breeder' => $data['breeder'], 'gender' => $data['gender']);
        $this->load->library('pdf');
        $this->pdf->setPaper('A4', 'potrait');;
        $this->pdf->filename = "label.pdf";
        $this->pdf->load_view('pembayaran/kartuikan', $res);
    }

    function pelunasan()
    {
        $kw = $this->input->get('kwitansi', true);
        $var = "";
        foreach ($kw as $kw) {
            $var .= "'" . $kw . "',";
        }
        $var = substr($var, 0, -1);
        $res = $this->Mpembayaran->prosesPelunasan($var);
        echo $res;
    }

    function detailTagihan()
    {
        $kode = urldecode($this->input->get('q'));
        $this->db->where('uniq_code', $kode);
        $row = $this->db->get('ikanheader')->row();
        if ($row) {
            // detail
            // SELECT  FROM ikandetail  WHERE 
            $this->db->where("uniq_code='$kode' AND deleted_at IS NULL", '', false);
            $this->db->select('ikandetail.*,biaya(ukuran) tarif,namavariety', false);
            $this->db->join('variety', 'variety.id=ikandetail.variety_id');
            $detail = $this->db->get('ikandetail')->result();

            $data['header'] = $row;
            $data['detail'] = $detail;
            $data['gender'] = gender();
            $data['breeder'] = breeder();
            return view('pembayaran.tagihandetail', $data);
        } else {
            redirect('pembayaran');
        }
    }

    function hapusikan($id)
    {
        $this->db->where('id', $id);
        $this->db->where('deleted_by is null', '', false);
        $cek = $this->db->get('ikandetail')->row();
        if ($cek) {
            $data = array('deleted_by' => getUser()->user_id, 'deleted_at' => date('Y-m-d H:i:s'));
            $this->db->where('id', $id);
            $this->db->where('deleted_by is null', '', false);
            $this->db->update('ikandetail',$data);
        } 
            redirect($_SERVER['HTTP_REFERER']);
        
    }

    function editikan($id){
        $this->load->model('Mvariety');
        $this->db->where('id', $id);
        $this->db->where('deleted_by is null', '', false);
        $cek = $this->db->get('ikandetail')->row();

        if ($cek) {
            $data['res']=$cek;
            $data['gender'] = gender();
            $data['breeder'] = breeder();
            $data['action']=base_url().'pembayaran/prosesedit';
            $data['variety']= $this->Mvariety->get()->result();
            return view('peserta.editikan', $data);
        }else{
            redirect($_SERVER['HTTP_REFERER']);
        }
    }

    function prosesedit(){
        if (empty($_FILES['gambar_ikan']['name'])) {
            $gambar = $this->input->post('gambar_ikan_lama');
        } else {
            @unlink($this->input->post('gambar_ikan_lama'));

            $_FILES['gbr_ikan']['name']     = $_FILES['gambar_ikan']['name'];
            $_FILES['gbr_ikan']['type']     = $_FILES['gambar_ikan']['type'];
            $_FILES['gbr_ikan']['tmp_name'] = $_FILES['gambar_ikan']['tmp_name'];
            $_FILES['gbr_ikan']['error']     = $_FILES['gambar_ikan']['error'];
            $_FILES['gbr_ikan']['size']     = $_FILES['gambar_ikan']['size'];

            // proses upload gambar 
            $config['upload_path']   = 'ikan/';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['encrypt_name']  = TRUE;
            $this->load->library('upload', $config);
            if ($this->upload->do_upload('gbr_ikan')) {
                $gbr                      = $this->upload->data();
                $config['image_library']  = 'gd2';
                $config['source_image']   = $config['upload_path'] . $gbr['file_name'];
                $config['create_thumb']   = FALSE;
                $config['maintain_ratio'] = TRUE;
                $config['quality']        = '70%';
                $config['new_image']      = 'ikan/' . $gbr['file_name'];
                $this->load->library('image_lib', $config);
                $this->image_lib->resize();
                $gambar = $config['new_image'];
            }
        }
        
        $detail['variety_id'] = $this->input->post('variety_id', true);
        $detail['ukuran'] = $this->input->post('ukuran', true);
        $detail['gender'] = $this->input->post('gender', true);
        $detail['breeder'] = $this->input->post('breeder', true);
        $detail['gambar_ikan'] = $gambar;
        $detail['updated_by'] = getUser()->user_id;
        $detail['updated_at'] = date('Y-m-d H:i:s');
        $this->db->where('id',$this->input->post('id'));
        $this->db->update('ikandetail', $detail);

        redirect('pembayaran/detailTagihan?q='.$this->input->post('id2'));
    }
}

/* End of file Peserta.php */
/* Location: ./application/controllers/Peserta.php */
