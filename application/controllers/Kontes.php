<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Kontes extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Mkontes');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));

        if ($q <> '') {
            $config['base_url'] = base_url() . 'kontes?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'kontes?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'kontes';
            $config['first_url'] = base_url() . 'kontes';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Mkontes->totalRows($q);
        $kontes = $this->Mkontes->getLimitData($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'kontes_data' => $kontes,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
            'ld' => array('Tutup', 'Buka')
        );

        return view('kontes/Kontes_list', $data);
    }



    public function create()
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('kontes/create_action'),
            'id' => set_value('id'),
            'namakontes' => set_value('namakontes'),
            'tempat' => set_value('tempat'),
            'kota' => set_value('kota'),
            'daftar' => set_value('daftar'),
            'poin' => set_value('poin'),
            'handling' => set_value('handling'),
            'entri' => set_value('entri'),
        );

        return view('kontes/Kontes_form', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
                'namakontes' => $this->input->post('namakontes', TRUE),
                'tempat' => $this->input->post('tempat', TRUE),
                'kota' => $this->input->post('kota', TRUE),
                'daftar' => $this->input->post('daftar', TRUE),
                'poin' => $this->input->post('poin', TRUE),
                'handling' => $this->input->post('handling', TRUE),
                'entri' => $this->input->post('entri', TRUE),
            );

            $this->Mkontes->insertData($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('kontes'));
        }
    }

    public function update($id)
    {
        $row = $this->Mkontes->first('id', $id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('kontes/update_action'),
                'id' => set_value('id', $row->id),
                'namakontes' => set_value('namakontes', $row->namakontes),
                'tempat' => set_value('tempat', $row->tempat),
                'kota' => set_value('kota', $row->kota),
                'daftar' => set_value('daftar', $row->daftar),
                'poin' => set_value('poin', $row->poin),
                'handling' => set_value('handling', $row->handling),
                'entri' => set_value('entri', $row->entri),
            );
            return view('kontes/Kontes_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('kontes'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
                'namakontes' => $this->input->post('namakontes', TRUE),
                'tempat' => $this->input->post('tempat', TRUE),
                'kota' => $this->input->post('kota', TRUE),
                'daftar' => $this->input->post('daftar', TRUE),
                'poin' => $this->input->post('poin', TRUE),
                'handling' => $this->input->post('handling', TRUE),
                'entri' => $this->input->post('entri', TRUE),
            );

            $this->Mkontes->updateData($data, $this->input->post('id', TRUE));
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('kontes'));
        }
    }

    public function delete($id)
    {
        $row = $this->Mkontes->first('id', $id);

        if ($row) {
            $this->Mkontes->deleteData($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('kontes'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('kontes'));
        }
    }

    public function _rules()
    {
        $this->form_validation->set_rules('namakontes', 'namakontes', 'trim|required');
        $this->form_validation->set_rules('tempat', 'tempat', 'trim|required');
        $this->form_validation->set_rules('kota', 'kota', 'trim|required');
        $this->form_validation->set_rules('daftar', 'daftar', 'trim|required');
        $this->form_validation->set_rules('poin', 'poin', 'trim|required');
        $this->form_validation->set_rules('handling', 'handling', 'trim|required');
        $this->form_validation->set_rules('entri', 'entri', 'trim|required');

        $this->form_validation->set_rules('id', 'id', 'trim');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }
}

/* End of file Kontes.php */
/* Location: ./application/controllers/Kontes.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2019-08-27 17:00:34 */
/* http://harviacode.com */
