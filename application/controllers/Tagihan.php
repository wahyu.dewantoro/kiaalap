<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Tagihan extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Mpembayaran');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $tab = $this->input->get('tab') <> '' ? $this->input->get('tab') : 'satu';
        $data = array(
            'tagihan' => $this->Mpembayaran->getTagihanOnline(),
            'tab' => $tab
        );
        return view('tagihan/index', $data);
    }

    function detailTagihan()
    {
        $kode = urldecode($this->input->get('q'));
        $this->db->where('uniq_code', $kode);
        $row = $this->db->get('ikanheader')->row();
        if ($row) {
            // detail
            // SELECT  FROM ikandetail  WHERE 
            $this->db->where("uniq_code='$kode' AND deleted_at IS NULL", '', false);
            $this->db->select('ikandetail.*,biaya(ukuran) tarif,namavariety', false);
            $this->db->join('variety', 'variety.id=ikandetail.variety_id');
            $detail = $this->db->get('ikandetail')->result();

            $data['header'] = $row;
            $data['detail'] = $detail;
            $data['gender'] = gender();
            $data['breeder'] = breeder();
            return view('tagihan.tagihandetail', $data);
        } else {
            redirect('tagihan');
        }
    }
 
}

/* End of file Peserta.php */
/* Location: ./application/controllers/Peserta.php */
