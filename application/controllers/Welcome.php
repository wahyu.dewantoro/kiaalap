<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Welcome extends CI_Controller
{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->model('Mvariety');
		$this->load->model('Mukuran');

		// $user = $this->ion_auth->user()->row();
		// $user_groups = $this->ion_auth->get_users_groups($user->id)->result();
		$user = getUser();
		$user_groups = $this->ion_auth->get_users_groups($user->id)->result();
		$group_id = [];
		foreach ($user_groups as $rg) {
			$group_id[] = $rg->id;
		}
		$handling = false;
		if (in_array('5', $group_id)) {
			// $this->db->where('created_by', $user->id);
			$handling = true;
		}


		if ($handling == false) {
			$data = array(
				'rfe' => $this->db->query("SELECT 'Cart' ket,COUNT(1)  jum
				FROM cartdetail
				UNION
				SELECT CASE WHEN lunas=1 THEN 'Terbayar' ELSE 'Belum Terbayar' END ket,COUNT(1) jum 
				FROM ikandetail
				JOIN ikanheader ON ikandetail.`uniq_code`=ikanheader.`uniq_code`
				where ikandetail.deleted_at is null and ikandetail.deleted_by is null
				GROUP BY lunas")->result(),
				'variety' => $this->Mvariety->get()->result(),
				'ukuran' => $this->Mukuran->get()->result(),
				'is_handling' => $handling
			);
		} else {
			$data = array(
				'cart' => $this->db->query("SELECT COUNT(1) cart FROM cartheader WHERE created_by=" . $user->id)->row()->cart,
				'ikan' => $this->db->query("SELECT COUNT(1) ikan FROM ikandetail WHERE deleted_by IS NULL AND created_by=" . $user->id)->row()->ikan,
				'tagihan' => $this->db->query("SELECT SUM(biaya(ukuran)) tagihan
				FROM ikanheader 
											JOIN ikandetail ON ikanheader.uniq_code=ikandetail.`uniq_code`
											WHERE  ikanheader.created_by ='" . $user->id . "' AND no_kwitansi IS NULL AND ikandetail.deleted_at IS NULL AND ikandetail.deleted_by IS NULL")->row()->tagihan,
				'is_handling' => $handling
			);
		}


		return view('index', $data);
	}
}
