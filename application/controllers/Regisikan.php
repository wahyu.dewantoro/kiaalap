<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Regisikan extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Mregis');
        $this->load->model('Mvariety');
        $this->load->library(array('form_validation', 'ion_auth'));
    }

    public function index()
    {
        $start = intval($this->input->get('start'));
        $user = getUser();
        $user_groups = $this->ion_auth->get_users_groups($user->id)->result();
        $group_id = [];
        foreach ($user_groups as $rg) {
            $group_id[] = $rg->id;
        }
        $handling = false;
        if (in_array('5', $group_id)) {
            $this->db->where('created_by', $user->id);
            $handling = true;
        }
        $regisikan = $this->Mregis->getData();
        $data = array(
            'regisikan_data' => $regisikan,
            'start' => $start,
            'is_handling' => $handling
        );
        return view('regisikan/Regisikan_list', $data);
    }



    public function create()
    {
        $gender = array(1 => 'Female', 'Male');
        $breeder = array(1 => 'Lokal', 'Impor');
        $this->db->order_by('sort', 'asc');
        $variety = $this->Mvariety->get()->result();

        $user = getUser();
        $user_groups = $this->ion_auth->get_users_groups($user->id)->result();
        $group_id = [];
        foreach ($user_groups as $rg) {
            $group_id[] = $rg->id;
        }

        $namahandling = '';
        $kotahandling = '';
        $telphandling = '';
        $handling = false;
        if (in_array('5', $group_id)) {
            $namahandling = $user->first_name . ' ' . $user->last_name;
            $kotahandling = $user->kota;
            $telphandling = $user->phone;
            $handling = true;
        }
        $data = array(
            'button' => 'Create',
            'action' => site_url('regisikan/create_action'),
            'id' => set_value('id'),
            'namahandling' => set_value('namahandling', $namahandling),
            'kotahandling' => set_value('kotahandling', $kotahandling),
            'telphandling' => set_value('telphandling', $telphandling),
            'namaowner' => set_value('namaowner'),
            'kotaowner' => set_value('kotaowner'),
            'variety' => $variety,
            'gender' => $gender,
            'breeder' => $breeder,
            'detail' => [],
            'is_handling' => $handling
        );

        return view('regisikan/Regisikan_form', $data);
    }

    public function create_action()
    {
        // dd($this->input->post());
        $this->db->trans_start();
        // data header
        $header['namahandling'] = $this->input->post('namahandling', true);
        $header['kotahandling'] = $this->input->post('kotahandling', true);
        $header['telphandling'] = $this->input->post('telphandling', true);
        $header['namaowner'] = $this->input->post('namaowner', true);
        $header['kotaowner'] = $this->input->post('kotaowner', true);
        $this->db->set('uniq_code', kodeurut());
        $header['created_by'] = getUser()->user_id;
        $header['created_at'] = date('Y-m-d H:i:s');
        $this->Mregis->insertData($header);

        // get last id
        $this->db->where('created_by', $header['created_by']);
        $this->db->where('created_at', $header['created_at']);
        $this->db->select('MAX(id) id', false);
        $lastid = $this->db->get('cartheader')->row()->id;

        // update uniq_code

        // $this->db->where('id', $lastid);
        // $this->db->update('cartheader');

        // NEW.uniq_code = getkode(NEW.id)

        for ($i = 0; $i < count($this->input->post('variety_id')); $i++) {
            $_FILES['gbr_ikan']['name']     = $_FILES['gambar_ikan']['name'][$i];
            $_FILES['gbr_ikan']['type']     = $_FILES['gambar_ikan']['type'][$i];
            $_FILES['gbr_ikan']['tmp_name'] = $_FILES['gambar_ikan']['tmp_name'][$i];
            $_FILES['gbr_ikan']['error']     = $_FILES['gambar_ikan']['error'][$i];
            $_FILES['gbr_ikan']['size']     = $_FILES['gambar_ikan']['size'][$i];

            // proses upload gambar 
            $config['upload_path']   = 'ikan/';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['encrypt_name']  = TRUE;
            $this->load->library('upload', $config);
            if ($this->upload->do_upload('gbr_ikan')) {
                $gbr                      = $this->upload->data();
                $config['image_library']  = 'gd2';
                $config['source_image']   = $config['upload_path'] . $gbr['file_name'];
                $config['create_thumb']   = FALSE;
                $config['maintain_ratio'] = TRUE;
                $config['quality']        = '70%';
                $config['new_image']      = 'ikan/' . $gbr['file_name'];
                $this->load->library('image_lib', $config);
                $this->image_lib->resize();
                $gambar = $config['new_image'];

                $detail['cartheader_id'] = $lastid;
                $detail['variety_id'] = $this->input->post('variety_id', true)[$i];
                $detail['ukuran'] = $this->input->post('ukuran', true)[$i];
                $detail['gender'] = $this->input->post('gender', true)[$i];
                $detail['breeder'] = $this->input->post('breeder', true)[$i];
                $detail['gambar_ikan'] = $gambar;
                $this->db->insert('cartdetail', $detail);
            }
        }
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            $this->session->set_flashdata('message', 'Gagal simpan');
        } else {
            $this->session->set_flashdata('message', 'Berhasil di simpan');
        }
        redirect(site_url('regisikan'));
    }

    public function update($id)
    {
        $row = $this->Mregis->first('id', $id);
        if ($row) {
            $gender = array(1 => 'Female', 'Male');
            $breeder = array(1 => 'Lokal', 'Impor');
            $this->db->order_by('sort', 'asc');
            $variety = $this->Mvariety->get()->result();

            $this->db->where('cartheader_id', $id);
            $detail = $this->db->get('cartdetail')->result();

            $user = getUser();
            $user_groups = $this->ion_auth->get_users_groups($user->id)->result();
            $group_id = [];
            foreach ($user_groups as $rg) {
                $group_id[] = $rg->id;
            }

            // $namahandling = '';
            // $kotahandling = '';
            // $telphandling = '';
            $handling = false;
            if (in_array('5', $group_id)) {
                // $namahandling = $user->first_name . ' ' . $user->last_name;
                // $kotahandling = $user->kota;
                // $telphandling = $user->phone;
                $handling = true;
            }

            $data = array(
                'button' => 'Update',
                'action' => site_url('regisikan/update_action'),
                'id' => set_value('id', $row->id),
                'uniq_code' => set_value('uniq_code', $row->uniq_code),
                'namahandling' => set_value('namahandling', $row->namahandling),
                'kotahandling' => set_value('kotahandling', $row->kotahandling),
                'telphandling' => set_value('telphandling', $row->telphandling),
                'namaowner' => set_value('namaowner', $row->namaowner),
                'kotaowner' => set_value('kotaowner', $row->kotaowner),
                'variety' => $variety,
                'gender' => $gender,
                'breeder' => $breeder,
                'detail' => $detail,
                'is_handling'=>$handling
            );
            return view('regisikan/Regisikan_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('regisikan'));
        }
    }

    public function update_action()
    {
        // dd($this->input->post());
        $this->db->trans_start();
        // data header
        $header['namahandling'] = $this->input->post('namahandling', true);
        $header['kotahandling'] = $this->input->post('kotahandling', true);
        $header['telphandling'] = $this->input->post('telphandling', true);
        $header['namaowner'] = $this->input->post('namaowner', true);
        $header['kotaowner'] = $this->input->post('kotaowner', true);
        $header['updated_by'] = getUser()->user_id;
        $header['updated_at'] = date('Y-m-d H:i:s');
        $this->Mregis->updateData($header, $this->input->post('id'));

        // get last id
        $lastid = $this->input->post('id');
        // delete ikan lama
        $this->db->where('cartheader_id', $lastid);
        $this->db->delete('cartdetail');

        for ($i = 0; $i < count($this->input->post('variety_id')); $i++) {
            if (empty($_FILES['gambar_ikan']['name'][$i])) {
                $gambar = $this->input->post('gambar_ikan_lama')[$i];
            } else {
                $_FILES['gbr_ikan']['name']     = $_FILES['gambar_ikan']['name'][$i];
                $_FILES['gbr_ikan']['type']     = $_FILES['gambar_ikan']['type'][$i];
                $_FILES['gbr_ikan']['tmp_name'] = $_FILES['gambar_ikan']['tmp_name'][$i];
                $_FILES['gbr_ikan']['error']     = $_FILES['gambar_ikan']['error'][$i];
                $_FILES['gbr_ikan']['size']     = $_FILES['gambar_ikan']['size'][$i];

                // proses upload gambar 
                $config['upload_path']   = 'ikan/';
                $config['allowed_types'] = 'jpg|png|jpeg';
                $config['encrypt_name']  = TRUE;
                $this->load->library('upload', $config);
                if ($this->upload->do_upload('gbr_ikan')) {
                    $gbr                      = $this->upload->data();
                    $config['image_library']  = 'gd2';
                    $config['source_image']   = $config['upload_path'] . $gbr['file_name'];
                    $config['create_thumb']   = FALSE;
                    $config['maintain_ratio'] = TRUE;
                    $config['quality']        = '70%';
                    $config['new_image']      = 'ikan/' . $gbr['file_name'];
                    $this->load->library('image_lib', $config);
                    $this->image_lib->resize();
                    $gambar = $config['new_image'];
                }
            }
            $detail['cartheader_id'] = $lastid;
            $detail['variety_id'] = $this->input->post('variety_id', true)[$i];
            $detail['ukuran'] = $this->input->post('ukuran', true)[$i];
            $detail['gender'] = $this->input->post('gender', true)[$i];
            $detail['breeder'] = $this->input->post('breeder', true)[$i];
            $detail['gambar_ikan'] = $gambar;
            $this->db->insert('cartdetail', $detail);
        }
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            $this->session->set_flashdata('message', 'Gagal simpan');
        } else {
            $this->session->set_flashdata('message', 'Berhasil di simpan');
        }
        redirect(site_url('regisikan'));
    }

    public function delete($id)
    {
        $row = $this->Mregis->first('id', $id);

        if ($row) {
            $this->Mregis->deleteData($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('regisikan'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('regisikan'));
        }
    }

    public function checkikan($id)
    {
        $row = $this->Mregis->first('id', $id);
        if ($row) {
            $dd = $this->Mregis->checkoutIkan($id, getUser()->user_id);
            if ($dd == 1) {
                $this->session->set_flashdata('message', 'checkout berhasil');
            } else {
                $this->session->set_flashdata('message', 'checkout gagal');
            }
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
        }
        redirect(site_url('regisikan'));
    }

    function detail($id)
    {
        $row = $this->Mregis->first('id', $id);
        if ($row) {
            $data['header'] = $row;
            $data['detail'] = $this->Mregis->getDetailHeader($id);
            $data['gender'] = gender();
            $data['breeder'] = breeder();
            return view('regisikan/view_detail', $data);
        } else {
            redirect('regisikan');
        }
    }
}

/* End of file Regisikan.php */
/* Location: ./application/controllers/Regisikan.php */
