<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Group extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Mgroup');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q     = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));

        if ($q <> '') {
            $config['base_url']  = base_url() . 'group?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'group?q=' . urlencode($q);
        } else {
            $config['base_url']  = base_url() . 'group';
            $config['first_url'] = base_url() . 'group';
        }
        $config['per_page']          = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows']        = $this->Mgroup->totalRows($q);
        $data                   = $this->Mgroup->getLimitData($config['per_page'], $start, $q);
        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'data' => $data,
            'q'                   => $q,
            'pagination'          => $this->pagination->create_links(),
            'total_rows'          => $config['total_rows'],
            'start'               => $start
        );

        return view('group/index', $data);
    }

    public function tambah()
    {
        $data = array(
            'action'    => site_url('group/prosesSimpan'),
            'id'    => set_value('id'),
            'name' => set_value('name'),
            'description' => set_value('description'),

        );
        return view('group/form', $data);
    }

    public function prosesSimpan()
    {
        $this->_rules();
        if ($this->form_validation->run() == FALSE) {
            $this->tambah();
        } else {
            $data = $this->input->post();
            $this->Mgroup->insertData($data);
            redirect('group');
        }
    }

    public function edit($id)
    {
        $cek = $this->Mgroup->first('id', $id);
        if ($cek) {
            $data = array(
                'action'    => site_url('group/prosesUpdate'),
                'id'    => set_value('id', $cek->id),
                'name' => set_value('name', $cek->name),
                'description' => set_value('description', $cek->description),
            );
            return view('group/form', $data);
        } else {
            redirect('group');
        }
    }

    public function prosesUpdate()
    {
        $this->_rules();
        if ($this->form_validation->run() == FALSE) {
            $this->edit($this->input->post('id'));
        } else {
            $data = $this->input->post();
            $this->Mgroup->updateData($data, $this->input->post('id'));
            redirect('group');
        }
    }

    public function hapus($id)
    {
        $cek = $this->Mgroup->first('id', $id);
        if ($cek) {
            $this->Mgroup->deleteData($id);
        }
        redirect('group');
    }

    public function _rules()
    {
        $this->form_validation->set_rules('name', 'nama menu', 'trim|required');
        $this->form_validation->set_rules('id', 'id', 'trim');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function setting($id)
    {
        // $row = $this->Mrole->get_by_id($id);
        // first('id',$id);
        $this->db->where('id',$id);
        $row= $this->Mgroup->first('id',$id);
        // $row =$role->row();
        if ($row) {
            
            $data = array(
                'id_inc'    => $row->id,
                'menu'      => $this->Mgroup->GetMenu($id),
                'title' => 'Setting Privilege : ' . $row->name
            );
            return view('group/view_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('role'));
        }
    }

    function prosessettingrole()
    {
        
        $kode_group = $this->input->post('kode_group');
        $roles      = $this->input->post('role');


        if (!empty($this->input->post('create'))) {
            $create = $this->input->post('create');
            // create
            $idc = '';
            while ($aa = current($create)) {
                if ($aa == '1') {
                    $idc .= key($create) . ',';
                }
                next($create);
            }
            $idc = substr($idc, 0, -1);
        } else {
            // $create=array();
            $idc = null;
        }


        if (!empty($this->input->post('update'))) {
            $update = $this->input->post('update');
            // update
            $idu = '';
            while ($ab = current($update)) {
                if ($ab == '1') {
                    $idu .= key($update) . ',';
                }
                next($update);
            }
            $idu = substr($idu, 0, -1);
        } else {
            $idu = null;
            // $update=array();
        }

        if (!empty($this->input->post('delete'))) {
            $delete     = $this->input->post('delete');
            // delete
            $idd = '';
            while ($ac = current($delete)) {
                if ($ac == '1') {
                    $idd .= key($delete) . ',';
                }
                next($delete);
            }
            $idd = substr($idd, 0, -1);
        } else {
            // $delete=array();
            $idd = null;
        }

        $data['kode_role'] = $kode_group;
        $data['read'] = implode(',', $roles);
        $data['create'] = $idc;
        $data['update'] = $idu;
        $data['delete'] = $idd;

        $role = $this->Mgroup->prosesrole($data);

        // echo $role;
        // if ($role) {
        //     set_flashdata('success', 'Berhasil, Data telah di simpan.');
        // } else {
        //     set_flashdata('warning', 'Berhasil, Data gagal di simpan.');
        // }
        redirect('group');
    }

}
