<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Check_session
{
    public function __construct()
    {
        $this->CI = &get_instance();
        $this->CI->load->library(array('session', 'ion_auth'));
    }

    public function validate()
    {
        $exp = array('auth', 'landing', 'report');
        $ctrl = $this->CI->uri->segment(1);
        if (!in_array(strtolower($ctrl), $exp)) {
            if (!$this->CI->ion_auth->logged_in()) {
                redirect('landing', 'refresh');
            }
        }
    }
}
