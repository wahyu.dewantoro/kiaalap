<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mgroup extends CI_Model
{

    public $table = 'groups';
    public $id = 'id';
    public $order = 'DESC';

    // function __construct()
    // {
    //     parent::__construct();
    // }

    // insert 
    function insertData($data)
    {
        return  $this->db->insert($this->table, $data);
    }


    function updateData($data, $id)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    function deleteData($id)
    {
        $this->db->where('id', $id);
        $this->db->delete($this->table);
    }

    // get all
    function get()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table);
    }

    function first($colom, $var)
    {
        $this->db->where($colom, $var);
        return $this->get()->row();
    }

    function queryDasar($q = null)
    {
        $this->db->like('id', $q);
        $this->db->or_like('name', $q);
        $this->db->or_like('description', $q);
        return $this->db->from($this->table, false);
    }

    function totalRows($q = null)
    {
        $this->queryDasar($q);
        return $this->db->count_all_results();
    }

    function getLimitData($limit, $start = 0, $q = NULL)
    {
        $this->queryDasar($q);
        $this->db->limit($limit, $start);
        return $this->db->get()->result();
    }


    function getMenu($id)
    {
        $cm = $this->db->query('SELECT id_inc FROM ms_menu')->result_array();
        foreach ($cm as $rm) {
            $kode_menu = $rm['id_inc'];
            $sql = "SELECT ms_menu_id FROM ms_privilege WHERE group_id='$id' AND ms_menu_id='$kode_menu'";
            $qcm = $this->db->query($sql)->num_rows();
            if ($qcm == null) {
                $data = array('ms_menu_id' => $kode_menu, 'group_id' => $id);
                $this->db->insert('ms_privilege', $data);
            }
        }

        $qr = $this->db->query("SELECT * FROM (
            SELECT b.id_inc kode_role,nama_menu,CASE parent WHEN 0 THEN '#'  ELSE NULL END AS parent,STATUS,a.sort sort,b.is_created created,b.is_updated updated,b.is_deleted deleted
            FROM ms_menu a , ms_privilege b WHERE a.id_inc=b.ms_menu_id 
            AND parent=0 AND group_id='$id' 
            UNION 
                SELECT c.id_inc kode_role,a.nama_menu,b.nama_menu parent,STATUS,a.sort ,c.is_created created,c.is_updated updated,c.is_deleted deleted
                FROM ms_menu a,(SELECT id_inc,nama_menu FROM ms_menu) b, ms_privilege c WHERE a.parent=b.id_inc AND a.id_inc=c.ms_menu_id AND group_id='$id' ) cb ORDER BY parent ASC,cb.sort ASC
                ")->result_array();
        return $qr;
    }

    function prosesrole($data = array())
    {
        // print_r($data);
        $this->db->trans_start();
        // set null all

        $this->db->set('status', null);
        $this->db->set('is_created', null);
        $this->db->set('is_updated', null);
        $this->db->set('is_deleted', null);
        $this->db->where('group_id', $data['kode_role']);
        $this->db->update('ms_privilege');

        

        if (!empty($data['read'])) {
            $idrr = explode(',', $data['read']);
            $this->db->set('status', 1);
            $this->db->where_in('id_inc', $idrr);
            $this->db->update('ms_privilege');
            // echo $this->db->last_query().'<br>';
        }


        if (!empty($data['create'])) {
            $idr = explode(',', $data['create']);
            $this->db->set('is_created', 1);
            $this->db->where_in('id_inc', $idr);
            $this->db->update('ms_privilege');
        }

        if (!empty($data['update'])) {
            $idu = explode(',', $data['update']);
            $this->db->set('is_updated', 1);
            $this->db->where_in('id_inc', $idu);
            $this->db->update('ms_privilege');
        }

        if (!empty($data['delete'])) {
            $idd = explode(',', $data['delete']);
            $this->db->set('is_deleted', 1);
            $this->db->where_in('id_inc', $idd);
            $this->db->update('ms_privilege');
        }


        $this->db->trans_complete();

        return $this->db->trans_status();
    }
}
