<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mpengguna extends CI_Model
{

    public $table = 'users';
    public $id = 'id';
    public $order = 'DESC';

    // function __construct()
    // {
    //     parent::__construct();
    // }

    // insert 
    function insertData($data)
    {
        return  $this->db->insert($this->table, $data);
    }


    function updateData($data, $id)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    function deleteData($id)
    {
        $this->db->where('id', $id);
        $this->db->delete($this->table);
    }

    // get all
    function get()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table);
    }

    function first($colom, $var)
    {
        $this->db->where($colom, $var);
        return $this->get()->row();
    }

    function queryDasar($q = null)
    {
        $this->db->like('id', $q);
        $this->db->or_like('first_name', $q);
        $this->db->or_like('last_name', $q);
        $this->db->or_like('email', $q);
        $this->db->or_like('company', $q);
        $this->db->or_like('phone', $q);
        return $this->db->from($this->table, false);
    }

    function totalRows($q = null)
    {
        $this->queryDasar($q);
        return $this->db->count_all_results();
    }

    function getLimitData($limit, $start = 0, $q = NULL)
    {
        $this->queryDasar($q);
        $this->db->limit($limit, $start);
        return $this->db->get()->result();
    }
}
