<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mregis extends CI_Model
{

    public $table = 'cartheader';
    public $id = 'id';
    public $order = 'DESC';

    // get all
    function get()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table);
    }

    // get data by
    function first($colom, $var)
    {
        $this->db->where($colom, $var);
        return $this->get()->row();
    }


    function queryDasar($q = null)
    {
        $this->db->where("(".$this->table.".id like '%".$q."%' or ".
        $this->table.".uniq_code like '%".$q."%' or ".
        $this->table.".namahandling like '%".$q."%' or ".
        $this->table.".kotahandling like '%".$q."%' or ".
        $this->table.".telphandling like '%".$q."%' or ".
        $this->table.".namaowner like '%".$q."%' or ".
        $this->table.".kotaowner like '%".$q."%' or ".
        $this->table.".created_by like '%".$q."%' or ".
        $this->table.".created_at like '%".$q."%' or ".
        $this->table.".updated_by like '%".$q."%' or ".
        $this->table.".updated_at like '%".$q."%')",'',false);
        $this->db->select($this->table . '.*');
        return $this->db->from($this->table);
    }
    // get total rows
    function totalRows($q = null)
    {
        $this->queryDasar($q);
        $this->withCreated();
        $this->countikan();
        $this->db->group_by($this->table . '.id');
        return $this->db->count_all_results();
    }

    //lmiit data
    function getLimitData($limit, $start = 0, $q = NULL)
    {
        $this->queryDasar($q);
        $this->withCreated();
        $this->countikan();
        $this->db->group_by($this->table . '.id');
        $this->db->limit($limit, $start);
        return $this->db->get()->result();
    }

    // insert data
    function insertData($data)
    {
        $this->db->insert($this->table, $data);
    }


    // update data
    function updateData($data, $id)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    // delete data
    function deleteData($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }

    function withCreated()
    {
        $this->db->select("CONCAT(first_name,' ',last_name) entri");
        return $this->db->join("users", $this->table . '.created_by=users.id');
    }

    function countikan()
    {
        $this->db->select("count(1) ikan");
        return $this->db->join("cartdetail", $this->table . '.id=cartdetail.cartheader_id');
    }


    function checkoutIkan($id, $user)
    {
        // insert ikan header
        $this->db->trans_start();
        $this->db->query("INSERT INTO ikanheader (
                uniq_code,
                namahandling,
                kotahandling,
                namaowner,
                kotaowner,
                created_at,
                created_by
              ) 
              SELECT uniq_code,
                namahandling,
                kotahandling,
                namaowner,
                kotaowner,
                NOW(),created_by
                 FROM cartheader WHERE id=" . $id);
        // ikan detail
        $this->db->query(" INSERT INTO ikandetail (
                    uniq_code,
                    namahandling,
                    kotahandling,
                    namaowner,
                    kotaowner,
                    variety_id,
                    ukuran,
                    gender,
                    breeder,
                    gambar_ikan,
                    created_at,
                    created_by
                  )
                    SELECT uniq_code,
                    namahandling,
                    kotahandling,
                    namaowner,
                    kotaowner,
                    variety_id,
                    ukuran,
                    gender,
                    breeder,
                    gambar_ikan,
                    NOW(),created_by
                     FROM cartdetail  a
                     JOIN cartheader b ON a.cartheader_id=b.id
                     WHERE cartheader_id=" . $id);
            // delete cart
            $this->db->where('id',$id);
            $this->db->delete('cartheader');
    
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE) {
            return false;
        } else {
            return true;
        }
    }

    function getData($q=null){
        $this->queryDasar($q);
        $this->withCreated();
        $this->countikan();
        $this->db->group_by($this->table . '.id');
        $this->db->order_by('id','desc');
        return $this->db->get()->result();
    }

    function getDetailHeader($id){
        return $this->db->query("SELECT a.* ,namavariety
        FROM cartdetail a
        JOIN variety b ON a.variety_id=b.id
        WHERE cartheader_id='$id'")->result();
    }
    
}

/* End of file Mregis.php */
/* Location: ./application/models/Mregis.php */
