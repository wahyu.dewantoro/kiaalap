<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mpenjurian extends CI_Model
{

    public function getListIkan($vu,$vv){
        $a=explode('_',$vu);
        $min=$a[0];
        $max=$a[1];
        return $this->db->query("SELECT ikandetail.* ,namavariety
                    FROM ikandetail 
                    join variety on ikandetail.variety_id=variety.id
                    WHERE ukuran>=$min AND ukuran<=$max 
                    AND variety_id=$vv AND deleted_at IS NULL 
                    AND deleted_by IS NULL")->result();
    }

    public function getListNoIkan(){
        $this->db->select("ikandetail.id no_ikan,namavariety",false);
        $this->db->from('ikandetail');
        $this->db->join('variety','variety.id=ikandetail.`variety_id`');
        $this->db->order_by('no_ikan','asc');
        return $this->db->get()->result();
    }

}
 