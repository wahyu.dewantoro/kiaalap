<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mjuara extends CI_Model
{

    public $table = 'juara';
    public $id = 'id';
    public $order = 'DESC';

    // get all
    function get()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table);
    }

    // get data by
    function first($colom, $var)
    {
        $this->db->where($colom, $var);
        return $this->get()->row();
    }

    function queryDasar($q = null)
    {
        $this->db->like($this->table.'.id', $q);
        $this->db->or_like($this->table.'.namajuara', $q);
        $this->db->or_like($this->table.'.aliasjuara', $q);
        $this->db->or_like($this->table.'.id_up', $q);
        $this->db->select($this->table.'.*');
        return $this->db->from($this->table);
    }
    // get total rows
    function totalRows($q = null)
    {
        $this->queryDasar($q);
        $this->withUp();
        return $this->db->count_all_results();
    }

    //lmiit data
    function getLimitData($limit, $start = 0, $q = NULL)
    {
        $this->queryDasar($q);
        $this->withUp();
        $this->db->limit($limit, $start);
        return $this->db->get()->result();
    }

    // insert data
    function insertData($data)
    {
        $this->db->insert($this->table, $data);
    }


    // update data
    function updateData($data, $id)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    // delete data
    function deleteData($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }

    function withUp(){
        $this->db->select("ifnull(p.namajuara,'-') nama_up");
        return $this->db->join("juara p", 'p.id ='.$this->table.'.id_up', 'left');
    }

}

/* End of file Mjuara.php */
/* Location: ./application/models/Mjuara.php */
