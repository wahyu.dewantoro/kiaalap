<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mvariety extends CI_Model
{

    public $table = 'variety';
    public $id = 'id';
    public $order = 'DESC';

    // get all
    function get()
    {
        $this->db->order_by('sort', 'asc');
        return $this->db->get($this->table);
    }

    // get data by
    function first($colom,$var){
        $this->db->where($colom,$var);
        return $this->get()->row();
    }
    

     function queryDasar($q=null)
    {
        $this->db->like('id', $q);
	$this->db->or_like('namavariety', $q);
	$this->db->or_like('kelas', $q);
	$this->db->or_like('sort', $q);
	 return $this->db->from($this->table);

     } 
    // get total rows
    function totalRows($q=null){
        $this->queryDasar($q);
        return $this->db->count_all_results();
    }

    //lmiit data
    function getLimitData($limit, $start = 0, $q = NULL){
        $this->queryDasar($q);
        $this->db->limit($limit, $start);
        return $this->db->get()->result();
     }

    // insert data
    function insertData($data)
    {
        $this->db->insert($this->table, $data);
    }
    

    // update data
    function updateData( $data,$id)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    // delete data
    function deleteData($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }

}

/* End of file Mvariety.php */
/* Location: ./application/models/Mvariety.php */