<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mpeserta extends CI_Model
{

    public $table = 'ikanheader';
    public $id = 'id';
    public $order = 'DESC';

    // get all
    function get()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table);
    }

    // get data by
    function first($colom, $var)
    {
        $this->db->where($colom, $var);
        return $this->get()->row();
    }


    function queryDasar($q = null)
    {
        $this->db->where("(" . $this->table . ".id like '%" . $q . "%' or " . $this->table . ".uniq_code like '%" . $q . "%' or " . $this->table . ".namahandling like '%" . $q . "%' or " . $this->table . ".kotahandling  like '%" . $q . "%' or " . $this->table . ".namaowner like '%" . $q . "%' or " . $this->table . ".kotaowner like '%" . $q . "%' or " . $this->table . ".no_kwitansi like  '%" . $q . "%' )", '', false);
        $this->db->select($this->table . '.*');
        return $this->db->from($this->table);
    }
    // get total rows
    function totalRows($q = null)
    {
        $this->db->where($this->table . '.deleted_at is null', '', false);
        $this->queryDasar($q);
        $this->countikan();
        $this->db->group_by($this->table . '.uniq_code');
        return $this->db->count_all_results();
    }

    //lmiit data
    function getLimitData($limit, $start = 0, $q = NULL)
    {
        $this->queryDasar($q);
        $this->countikan();
        $this->db->group_by($this->table . '.uniq_code');
        $this->db->where($this->table . '.deleted_at is null', '', false);
        $this->db->limit($limit, $start);
        $this->db->order_by('no_kwitansi','desc');
        return $this->db->get()->result();
    }

    // insert data
    function insertData($data)
    {
        $this->db->insert($this->table, $data);
    }


    // update data
    function updateData($data, $id)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    // delete data
    function deleteData($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }

    function countikan()
    {
        $this->db->select("count(1) ikan");
        return $this->db->join("(select * from ikandetail where deleted_at is null) ikandetail", $this->table . '.uniq_code=ikandetail.uniq_code');
    }

    function getDetailPeserta($kode)
    {
        $this->db->select('ikandetail.*,namavariety', false);
        $this->db->join('variety', 'variety.id=ikandetail.variety_id');
        $this->db->from('ikandetail');
        $this->db->where('deleted_at is null','',false);
        $this->db->where('uniq_code', $kode);
        return $this->db->get()->result();
    }

    function getDoorprize(){
        return $this->db->query("select id no,concat(namahandling,' (',kotahandling,')') handling, concat(namaowner,' (',kotaowner,')') owner from ikandetail where deleted_at is null and deleted_by is null
                                order by id")->result();
    }

    function getListIkan(){
        $user = getUser();
        $user_groups = $this->ion_auth->get_users_groups($user->id)->result();
        $group_id = [];
        foreach ($user_groups as $rg) {
            $group_id[] = $rg->id;
        }
        $handling = false;
        if (in_array('5', $group_id)) {
            $this->db->where('ikandetail.created_by', $user->id);
            $handling = true;
        }
        $this->db->select('ikandetail.id no_ikan,namavariety,ukuran,gender,breeder,gambar_ikan,namahandling,kotahandling,namaowner,kotaowner',false);
        $this->db->where('deleted_by is null and deleted_at is null',null,false);
        $this->db->join('variety','variety.`id`=ikandetail.`variety_id`');
        $this->db->from('ikandetail');
        $this->db->order_by('ikandetail.id','asc');
        return $this->db->get()->result();

    }
}

/* End of file Mpeserta.php */
/* Location: ./application/models/Mpeserta.php */
