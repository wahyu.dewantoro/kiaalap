<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mmenu extends CI_Model
{

    public $table = 'ms_menu';
    public $id = 'id_inc';
    public $order = 'DESC';

    // get all
    function get($kolom = null, $val = null)
    {
        if ($kolom <> '') {
            $this->db->where($kolom, $val);
        }
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table);
    }

    // get data by
    function first($colom, $var)
    {
        $this->db->where($colom, $var);
        return $this->get()->row();
    }


    function queryDasar($q = null)
    {
        $this->db->like( $this->table.'.id_inc', $q);
        $this->db->or_like( $this->table.'.nama_menu', $q);
        $this->db->or_like( $this->table.'.link_menu', $q);
        $this->db->or_like( $this->table.'.parent', $q);
        $this->db->or_like( $this->table.'.sort', $q);
        $this->db->or_like( $this->table.'.icon', $q);
        $this->db->select($this->table.'.*');
        return $this->db->from($this->table);
    }
    // get total rows
    function totalRows($q = null)
    {
        $this->withParent();
        $this->queryDasar($q);
        return $this->db->count_all_results();
    }

    //lmiit data
    function getLimitData($limit, $start = 0, $q = NULL)
    {
        $this->withParent();
        $this->queryDasar($q);
        $this->db->limit($limit, $start);
        return $this->db->get()->result();
    }
 

    // insert data
    function insertData($data)
    {
        $this->db->insert($this->table, $data);
    }


    // update data
    function updateData($data, $id)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    // delete data
    function deleteData($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }

    function withParent(){
        $this->db->select("ifnull(p.nama_menu,'Is Parent') nama_parent");
        return $this->db->join("(SELECT * FROM ms_menu WHERE parent=0) p", 'p.id_inc ='.$this->table.'.parent', 'left');
    }

}

/* End of file Mmenu.php */
/* Location: ./application/models/Mmenu.php */
