<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mpoin extends CI_Model
{

    public $table = 'poinmaster';
    public $id = 'id';
    public $order = 'DESC';

    // get all
    function get()
    {
       
        $this->db->order_by($this->table.'.'.$this->id, $this->order);
        $this->db->join('juara','juara.id='.$this->table.'.juara_id');
        $this->db->select($this->table.'.*,namajuara',false);
        return $this->db->get($this->table);
    }

    // get data by
    function first($colom, $var)
    {
        $this->db->where($colom, $var);
        return $this->get()->row();
    }


    function queryDasar($q = null)
    {
        $this->db->like($this->table.'.id', $q);
        $this->db->or_like('ukuranmin', $q);
        $this->db->or_like('ukuranmax', $q);
        $this->db->or_like('juara_id', $q);
        $this->db->or_like('namajuara', $q);
        $this->db->or_like('poin', $q);
        $this->db->join('juara','juara.id='.$this->table.'.juara_id');
        $this->db->select($this->table.'.*,namajuara',false);
        return $this->db->from($this->table);
    }
    // get total rows
    function totalRows($q = null)
    {
        $this->queryDasar($q);
        return $this->db->count_all_results();
    }

    //lmiit data
    function getLimitData($limit, $start = 0, $q = NULL)
    {
        $this->queryDasar($q);
        $this->db->limit($limit, $start);
        return $this->db->get()->result();
    }

    // insert data
    function insertData($data)
    {
        $this->db->insert($this->table, $data);
    }


    // update data
    function updateData($data, $id)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    // delete data
    function deleteData($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }
}

/* End of file Mpoin.php */
/* Location: ./application/models/Mpoin.php */
