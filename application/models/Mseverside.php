<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mseverside extends CI_Model
{

    public $table = 'variety';
    public $id = 'id';
    public $order = 'DESC';

    // datatables
    function json() {
        $this->datatables->select('id,namavariety,kelas,sort');
        $this->datatables->from('variety');
        //add this line for join
        //$this->datatables->join('table2', 'variety.field = table2.field');
        $this->datatables->add_column('action', anchor(site_url('serverside/read/$1'),'Read')." | ".anchor(site_url('serverside/update/$1'),'Update')." | ".anchor(site_url('serverside/delete/$1'),'Delete','onclick="javasciprt: return confirm(\'Are You Sure ?\')"'), 'id');
        return $this->datatables->generate();
    }

    // get all
    function get()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table);
    }

    // get data by
    function first($colom,$var){
        $this->db->where($colom,$var);
        return $this->get()->row();
    }
    

     function queryDasar($q=null)
    {
        $this->db->like('id', $q);
	$this->db->or_like('namavariety', $q);
	$this->db->or_like('kelas', $q);
	$this->db->or_like('sort', $q);
	 return $this->db->from($this->table);

     } 
    // get total rows
    function totalRows($q=null){
        $this->queryDasar($q);
        return $this->db->count_all_results();
    }

    //lmiit data
    function getLimitData($limit, $start = 0, $q = NULL){
        $this->queryDasar($q);
        $this->db->limit($limit, $start);
        return $this->db->get()->result();
     }

    // insert data
    function insertData($data)
    {
        $this->db->insert($this->table, $data);
    }
    

    // update data
    function updateData( $data,$id)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    // delete data
    function deleteData($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }

}

/* End of file Mseverside.php */
/* Location: ./application/models/Mseverside.php */