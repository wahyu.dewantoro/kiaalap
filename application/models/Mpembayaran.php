<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mpembayaran extends CI_Model
{


    function getTagihan()
    {
        return $this->db->query("SELECT ikanheader.id,ikanheader.uniq_code,ikanheader.namahandling,ikanheader.kotahandling,ikanheader.namaowner,ikanheader.kotaowner,COUNT(ikandetail.id) ikan,SUM(biaya(ukuran)) tagihan
                                FROM ikanheader 
                                JOIN ikandetail ON ikanheader.uniq_code=ikandetail.`uniq_code`
                                WHERE no_kwitansi IS NULL and ikandetail.deleted_at is null and ikandetail.deleted_by is null
                                GROUP BY ikanheader.id,ikanheader.uniq_code,ikanheader.namahandling,ikanheader.kotahandling,ikanheader.namaowner,ikanheader.kotaowner")->result();
    }

    function getTagihanOnline()
    {
        $user = getUser()->user_id;
        return $this->db->query("SELECT ikanheader.id,ikanheader.uniq_code,ikanheader.namahandling,ikanheader.kotahandling,ikanheader.namaowner,ikanheader.kotaowner,COUNT(ikandetail.id) ikan,SUM(biaya(ukuran)) tagihan,lunas
                                    FROM ikanheader 
                                    JOIN ikandetail ON ikanheader.uniq_code=ikandetail.`uniq_code`
                                    WHERE  ikanheader.created_by ='$user' AND no_kwitansi IS NULL AND ikandetail.deleted_at IS NULL AND ikandetail.deleted_by IS NULL
                                    GROUP BY ikanheader.id,ikanheader.uniq_code,ikanheader.namahandling,ikanheader.kotahandling,ikanheader.namaowner,ikanheader.kotaowner,lunas")->result();
    }

    function getPembayaran()
    {
        return $this->db->query("SELECT kwitansiheader.id,tgl_kwitansi,jumlah,lunas,GROUP_CONCAT(uniq_code,' - ',namahandling,' (',kotahandling,')' SEPARATOR ', ') keterangan,created_by,created_at
                                FROM kwitansiheader 
                                JOIN kwitansidetail ON kwitansidetail.`kwitansi_id`=kwitansiheader.`id`
                                
                                GROUP BY kwitansiheader.id,tgl_kwitansi,jumlah,lunas,created_by,created_at")->result();
    }

    

    function prosesKwitansi($var)
    {
        // cek
        $sql = "SELECT GROUP_CONCAT(\"'\",uniq_code,\"'\") var FROM ikanheader WHERE no_kwitansi IS NULL AND uniq_code in (" . $var . ")";
        $cek = $this->db->query($sql)->row();
        if (!empty($cek->var)) {
            $this->db->trans_start();
            // ambil jumlah total
            $total = $this->db->query("SELECT SUM(biaya(ukuran)) jumlah FROM ikandetail 
                                        WHERE  uniq_code IN (" . $cek->var . ") and deleted_by is null")->row()->jumlah;

            $hk = array(
                'tgl_kwitansi' => date('Y-m-d'),
                'jumlah' => $total,
                'created_at' => date('Y-m-d H:i:s'),
                'created_by' => getUser()->user_id
            );
            $this->db->insert('kwitansiheader', $hk);

            // get last_id
            $this->db->where($hk);
            $lastid = $this->db->get('kwitansiheader')->row()->id;

            // insert kwitansi detail
            $this->db->query("INSERT INTO kwitansidetail (kwitansi_id,ikanheader_id,uniq_code,namahandling,kotahandling,namaowner,kotaowner) 
                            SELECT'" . $lastid . "' kwitansi_id,ikanheader.id,ikanheader.uniq_code,ikanheader.namahandling,ikanheader.kotahandling,ikanheader.namaowner,ikanheader.kotaowner
                            FROM ikanheader 
                            WHERE uniq_code IN (" . $cek->var . ")");

            // update no_kwitansi di ikanheader
            $this->db->query("update ikanheader set no_kwitansi='" . $lastid . "' where uniq_code in (" . $cek->var . ")");

            $this->db->trans_complete();
            if ($this->db->trans_status() === FALSE) {
                return  0;
            } else {
                return $lastid;
            }
        } else {
            return 0;
        }
    }

    function getKwitansi($id)
    {
        $kontes = $this->db->query("SELECT * FROM kontes")->row();
        $global = $this->db->query("SELECT kwitansiheader.id,tgl_kwitansi,jumlah,lunas,GROUP_CONCAT(uniq_code,' - ',namahandling,' (',kotahandling,')' SEPARATOR ', ') keterangan
                                    FROM kwitansiheader 
                                    JOIN kwitansidetail ON kwitansidetail.`kwitansi_id`=kwitansiheader.`id`
                                    WHERE kwitansiheader.id=$id
                                    GROUP BY kwitansiheader.id,tgl_kwitansi,jumlah,lunas")->row();
        $resume = $this->db->query("SELECT ukuranalias(ukuran) size,COUNT(1) jum,SUM(biaya(ukuran)) total,ROUND(SUM(biaya(ukuran)) / COUNT(1),0)  harga
                                    FROM ikandetail 
                                    JOIN ikanheader ON ikandetail.uniq_code=ikanheader.uniq_code
                                    WHERE no_kwitansi=$id and ikandetail.deleted_by is null
                                    GROUP BY  size
                                    ORDER BY ukuran ASC")->result();
        $ikan = $this->db->query("SELECT  ikanheader.id,ikandetail.uniq_code,ikandetail.namahandling,ikandetail.kotahandling,ikandetail.namaowner,ikandetail.kotaowner,ikandetail.id no_ikan,namavariety,ukuran,ukuranalias(ukuran) kategori,biaya(ukuran) biaya,gambar_ikan,gender,breeder
                                FROM ikandetail 
                                JOIN ikanheader ON ikandetail.uniq_code=ikanheader.uniq_code
                                JOIN variety ON variety.id=ikandetail.`variety_id`
                                WHERE no_kwitansi=$id and ikandetail.deleted_by is null
                                ORDER BY ikanheader.id ASC,uniq_code ASC,ikandetail.id ASC")->result();

        $plastik = $this->db->query("SELECT getplastik(ukuran) plastik,COUNT(1) jumlah
                                    FROM ikandetail
                                    JOIN ikanheader ON ikandetail.uniq_code=ikanheader.uniq_code
                                    WHERE no_kwitansi=$id  and ikandetail.deleted_by is null
                                    GROUP BY plastik
                                    ORDER BY ukuran ASC")->result();
        return $data = array(
            'kontes' => $kontes,
            'global' => $global,
            'resume' => $resume,
            'ikan' => $ikan,
            'plastik' => $plastik,
            'breeder' => breeder(),
            'gender' => gender()
        );
    }

    function prosesPelunasan($var)
    {
        $this->db->trans_start();
        $this->db->query("UPDATE kwitansiheader SET lunas=1 WHERE id IN (" . $var . ")");
        $this->db->query("UPDATE ikanheader SET lunas=1 WHERE no_kwitansi IN (" . $var . ")");
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE) {
            return false;
        } else {
            return true;
        }
    }
}

/* End of file Mpeserta.php */
/* Location: ./application/models/Mpeserta.php */
